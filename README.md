# yailfc - Yet Another Image Library for C

Aims to become a simple but bloody fast library for image-processing in C. Aims to be optimized for several platforms like x86 and ARM, also for several threads. The library will not use dynamic memory managment, therefore it should be ideal for a low level application. 

But that also means, that you needs to care for memory, care for the data and maybe copy it before an operation if you need the original data. Requires knowledge of the C language. If you are not sure, what ''fopen(...)'' means, you should probably stay away. 

## Using yailfc

Using yailfc is quite simple. You can use the sources, compile them and link them against your program. You could also build shared objects from the sources (if you can figure out yourself how to do that), and link your program against that shared library, although I don't see the reason why you would prefer that over static linking.

The image structure is created with the ''createImage''-call. You need to allocate the payload-memory region yourself, so you either want to use malloc / new or in bare metal programs reserve the array yourself. The library will not reserve any memory for you, not even for resizing, copying or rotating an image. You have to do that on your own! The library also does not check whether the memory region is reserved, so make sure the region is big enough.

## Color models

yailfc theoretically supports pretty much every color model you can think of. 

(VISION). It should become possible to call a generic function, for example ''swap'', which in turn decides based in the color model which subroutine is called. This could be done by using an array of function-pointers or a struct of said pointers. I'm not sure how I'll do that, as both procedures have pros and cons. Act suprised when it finally arrives.

## Software renderer

I'm currently working on a simple software renderer and a basic scene within that. At the time of writing this paragraph a central projection works with viewports, i.e. one can have split screens with no problems whatsoever. Also: textures do work for triangles but not for rectangles at the moment, either having stretched textures (rectangle-renderer) or textures split in half (based on the triangle-rendering). 

### What is to come
- some basic matrix operations like rotation, scaling and translation
- even more matrix operations later on after projection (maybe have them set in the context, not sure about that)
- lighting ... like lights... and stuff.
- blending, for water, glass
- filters after the rasterization for under water

# Usage of the demo

There are four basic game-states:
1. Prompt-mode
2. Settings-mode
3. Ingame
4. Ingame settings

## Prompt-mode

This mode receives its input from a `getchar()`-call, so be sure to use the terminal for input. As such, some compromises hat to be done. Textinput works normally, BACKSPACE is replaces with `<`. ENTER will send the command. The following commands are defined:
- `uname` - prints information about the Operating System kernel into prompt
- `echo` - prints the next parameters to prompt
- `help` - prints very helpful helptext to prompt
- `control`, `settings` - opens the settings-dialoge, see Settings-mode
- `run` - runs the demo
- `exit` - exits the prompt
- `clear` - clears the prompt

There are no other commands defined, no need looking for it. Especially, the cake is a lie.

## Settings-mode and Ingame Settings

These modes show the settings window, but differ in the mode they return into on closing the window. The window and controls can only be used with the keyboard, as before, they get their input from `getchar()`. You can navigate the window by using the `w` and `s`-key and toggle a checkbutton by pressing the SPACEBAR. The input of a textbox can be deleted character by character using the `<`-key. Make sure to disable Motion Blur. The currently focused control is identified with a green rectangle next to it. Close with window with `q`. 

## Ingame

You can move using the w,a,s,d-keys, and look using the i,j,k,l-keys.

These functions can be triggered in game by a keystroke:
- `t` - take screenshots (linux only, disabled on Raspberry Pi due to 'missing' file-system support)
- `b` - drop on y-axis
- `g` - rise on y-axis
- `f` - toggle orthogonal projection
- `c` - disable map constaints
- `m` - open settings window
- `1` - disable light source 1 (ambient only)
- `2` - disable light source 2 (ambient and diffuse)
- `3` - disable light source 3 (ambient, diffuse and specular (moves with the camera))
- `4` - disable light source 4 (fire-light)

## Known Issues

- Light source 3 (specular) seems to not move with the camera at all. 
- Orthogonal projection breaks depthtest (idea: divide by w after projection)
- alpha rendering creates artefacts on the light-beam of the watch tower (probably due to missing clipping functions)
- fire-plane gets messed up when too close (probably due to missing clipping-function)
- transparent textures mess up the Depth-buffer (should be rendered after every other object) - this really messes up the fog


# Attribution

I got all of the textures from [OpemGameArt](https://opengameart.org/). The monospaced font is from the [BakingPi Tutorial](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/os/downloads.html#fonts).
