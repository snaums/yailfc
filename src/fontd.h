/**
 * \file fontd.h
 * \author Stefan Naumann
 * \brief font description for using a monospaced font
 **/

#ifndef PIOS_MS_FONT
#define PIOS_MS_FONT

#include <stdint.h>

/**
 * \brief how wide and tall is the font?
 **/
struct _font_info
{
    int x, ///< width
        y; ///< height
};

/// describing the builtin monospace font
extern struct _font_info ms_font_info;

/// array defining the font itself
extern uint8_t ms_font[];
/// length of the array describing the font
extern unsigned int ms_font_len;

#endif
