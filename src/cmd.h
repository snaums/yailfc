/**
 * \file cmd.h
 * \author Stefan Naumann
 * \brief contains the subprograms which can be invoked in the command-prompt
 **/

#ifndef YIL_CMD
#define YIL_CMD

#include "console.h" 

/**
 * \brief state of the demo
 **/
enum _state
{
    CONSOLE,            ///< console prompt 
    SETTINGS,           ///< settings window opened (with console in the background)
    RENDERER,           ///< 3D software rendered world
    SETTINGSINGAME      ///< settings window opened (with the 3D image in the background)
};

/**
 * \brief crashes the program and prints an error screen
 * \param[in] fault an identifier of the fault that happened
 * \param[in] lr the link-register (i.e. return address)
 **/
void crash ( int fault, void* lr );

/**
 * \brief prints the ARGV-inputs into the prompt like any old echo-command
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int echoCmd ( int argc, char** argv );
/**
 * \brief unused
 **/
int cake ( int argc, char** argv );
/**
 * \brief prints a help text, which is very helpful indeed.
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int help ( int argc, char** argv );
/**
 * \brief print the kernel name and version number
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int uname ( int argc, char** argv );
/**
 * \brief run the demo, i.e. start into the 3D-renderer
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int run ( int argc, char** argv );
/**
 * \brief exit the program interely
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int yil_exit ( int argc, char** argv );
/**
 * \brief open the settings window
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int setStart ( int argc, char** argv );
/**
 * \brief clear the prompt from output
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int cmdClear ( int argc, char** argv );
/**
 * \brief prints the crash-screen and hangs the program
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int cmdCrash ( int argc, char** argv );
/**
 * \brief prints every registered command to screen
 * \param[in] argc number of arguments for the call
 * \param[in] argv the arguments itself
 * \return the result-value ("return of the main-routine of the subprogram")
 **/
int listCommands ( int argc, char ** argv );
#endif
