#include "settings.h"

struct yil_ui_setting yil_settings [ YIL_MAX_SETTINGS ];

int yil_settings_min = 0;
int yil_settings_max = YIL_SETTINGS_SHOW_MAX;
int yil_settings_current = 0;
int yil_settings_focus = 0;
int yil_settings_add ( int hwnd, const char* name, enum CtrlType type, void* value )
{
    if ( yil_settings_current >= YIL_MAX_SETTINGS )
        return -1;
    
    if ( type != CHECKBOX && type != TEXTBOX )
        return -1;
    
    int i = yil_settings_current++;
    yil_settings[i].name = name;
    yil_settings[i].value = value;
    yil_settings[i].type = type;
    yil_settings[i].hwnd = hwnd;
    
    return i;
}

int yil_settings_move_focus ( int hwnd, int byLines )
{
    yil_settings_focus += byLines;
    
    if ( yil_settings_focus < 0 )
        yil_settings_focus = 0;
    else if ( yil_settings_focus >= yil_settings_current )
        yil_settings_focus = yil_settings_current - 1;
    else if ( yil_settings_focus >= yil_settings_max || yil_settings_focus < yil_settings_min )
        yil_settings_moveRow ( hwnd, byLines );
    return 0;
}

int yil_settings_get_focus ()
{
    return yil_settings_focus;
}

int yil_settings_moveRow ( int hwnd, int byLines )
{
    yil_settings_min += byLines;
    yil_settings_max += byLines;
    
    if ( yil_settings_min < 0 )
    {
        yil_settings_min = 0;
        yil_settings_max = YIL_SETTINGS_SHOW_MAX;
    }
    else if ( yil_settings_max > yil_settings_current )
    {
        yil_settings_min = yil_settings_current - YIL_SETTINGS_SHOW_MAX - 1;
        yil_settings_max = yil_settings_current-1;
        if ( yil_settings_min < 0 )
        {
            yil_settings_min = 0;
            yil_settings_max = YIL_SETTINGS_SHOW_MAX;
        }
    }
    return 0;
}

int yil_settings_invalidate ( int hwnd )
{
    pios_window_empty ( hwnd );
    
    int y = 40; 
    int x = 30;
    int x2 = 260;
    
    int prev = pios_window_createControl ( hwnd, 30, 10, BUTTON, "<" );
    pios_window_control_disable ( hwnd, prev );
    if ( yil_settings_min > 0 )
    {
        pios_window_control_enable ( hwnd, prev );
    }
    
    int succ = pios_window_createControl ( hwnd, 320, 10, BUTTON, ">" );
    pios_window_control_disable ( hwnd, succ );
    if ( yil_settings_max < yil_settings_current )
    {
        pios_window_control_enable ( hwnd, succ );
    }
    
    for ( int i=yil_settings_min; i<yil_settings_max; i++ )
    {    
        if ( i >= yil_settings_current )
        {
            break;
        }
        
        //pios_window_createControl ( hwnd, x, y, LABEL, yil_settings[i].name );
        if ( i == yil_settings_focus )
        {
            pios_window_control_focus ( hwnd, pios_window_createControl ( hwnd, x, y, LABEL, (void*) yil_settings[i].name ) );
        }
        else
        {
            pios_window_createControl ( hwnd, x, y, LABEL, (void*) yil_settings[i].name );
        }
        if ( yil_settings[i].type != LABEL )
            pios_window_createControl ( hwnd, x2, ((yil_settings[i].type == TEXTBOX) ? y-2 : y), yil_settings[i].type, yil_settings[i].value );
        
        y+=20;
    }
    
    return 0;
}

int yil_settings_translate ( struct yil_settings_data* data, struct yil_ctx* ctx )
{
    ctx->rendermode = ( data->AlphaRender ? YIL_ALPHA_BLEND : (data->Wire ? YIL_WIRE : YIL_FILL ));
    ctx->interpolation = (data->BilinearInterpol ? YIL_BILINEAR : YIL_NEAREST );
    ctx->culling = (data->Culling ? YIL_FFC : YIL_BOTH );
    ctx->texmode = (data->Texture ? YIL_TEX_REPLACE : YIL_TEX_NONE );
    ctx->depthtest = data->Depthtest;
    ctx->fog_zmin = atoi ( data->FogMin );
    ctx->fog_zmax = atoi ( data->FogMax );
    ctx->fog_enable = data->Fog;
    ctx->texture_interpol = (data->texture_interpol ? YIL_BILINEAR : YIL_NEAREST);
    ctx->upscale = (data->Bilinear ? YIL_BILINEAR : YIL_NEAREST );
    ctx->extra.noise = data->Noise;
    ctx->extra.motionBlur = data->Blur;
    float level = atoi ( data->BlurLevel );
    ctx->extra.motionBlurLevel = ((level > 100) ? 100 : (level < 0) ? 0 : level) / 100;
    ctx->lighting_enable = (data->Lighting);
    ctx->map_constraints = (!data->map_constraints);
    
    return 0;
}
