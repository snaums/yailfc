/** 
 * \file light.h
 * \author Stefan Naumann
 * \brief lighting definitions and functions for calculating lighted-colours
 **/

#ifndef YIL_LIGHTING_SIMPLE
#define YIL_LIGHTING_SIMPLE

/// maximal amount of lights allowed in the engine
#define YIL_MAX_LIGHT 5

#include <stdbool.h>

#include "camera.h"
#include "image.h"
#include "geometry.h"

struct yil_geometry;

/**
 * \brief defines types of lights allowed
 **/
enum yil_lightType 
{
    AMBIENT,            ///< an ambient light (any nothing more)
    DIRECTIONAL,        ///< directional light (render as POINT in 0, -20, 5)
    POINT               ///< point light (with a position, but no direction
    /// SPOT            ///< spot light 
};

/**
 * \brief datastructure for light-sources
 **/
struct yil_lightSource 
{
    bool enable;        ///< is it on?

    void* coloramb;     ///< ambient color (RGB(A)!)
    void* colordiff;    ///< diffuse color (RGB(A)!)
    void* colorspec;    ///< specular color (RGB(A)!)
    
    float x,            ///< the x-coordinate of the light
          y,            ///< y-coordinate
          z;            ///< z-coordinate
    float range;        ///< the range of the light (useful for POINT and SPOT-lights)
    
    enum yil_lightType type;    ///< the type of the light-source
    
    /*float dx, dy, dz;
    float cutoff;*/
};

/**
 * \brief add a (generic) light object
 * \param[in] t the type of the light
 * \param[in] amb the ambient light-color
 * \param[in] diff the diffuse light color
 * \param[in] spec the specular light color
 * \param[in] px the x-coordinate of the light-position
 * \param[in] py the y-coordinate
 * \param[in] pz the z-coordinate
 * \return handle of the light
 **/
int yil_addLight ( enum yil_lightType t, void* amb, void* diff, void* spec, float px, float py, float pz );
/**
 * \brief add an ambient light
 * \param[in] amb ambient color for the ambient light
 * \return handle of the light
 **/
int yil_addAmbientLight ( void* amb );

/**
 * \brief disable a light-source
 * \param[in] light the light-source
 **/
void yil_lightDisable ( int light );
/**
 * \brief enable a light-source
 * \param[in] light the light-source
 **/
void yil_lightEnable ( int light );
/**
 * \brief toggle a light-source
 * \param[in] light the light-source
 **/
void yil_lightToggle ( int light );
/**
 * \brief set the range of the light-source (POINT, SPOT)
 * \param[in] light the light-source
 * \param[in] range the maximal range of the light-source
 **/
void yil_lightRange ( int light, float range );
/**
 * \brief moves an existing light to a new position
 * \param[in] light the handle of the light
 * \param[in] px the x-coordinate of the light-position
 * \param[in] py the y-coordinate
 * \param[in] pz the z-coordinate
 **/
int yil_lightMove ( int light, float px, float py, float pz );
/**
 * \brief returns the position of the light
 * \param[in] light the handle of the light
 * \param[out] coord the xyz-coordinates
 **/
int yil_lightPosition ( int light, float* coord );
/**
 * \brief light vertices according to their position in the camera space
 * \param[in] pts the vertices (4 coordinates)
 * \param[in] geo the geometry-object
 * \param[out] colors the array of OUTPUT-colors (will be black <-> white for textured drawing)
 * \param[in] cam the camera object
 * \param[in] normal the normals at the vertices (as transformed in the camera-space)
 **/
int yil_light ( float* pts, struct yil_geometry *geo, struct rgb* colors, struct _camera* cam, float* normal );

#endif
