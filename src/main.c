#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "pios_port_config.h"

#ifndef PIOS_PLATFORM_RPI
    #ifdef __APPLE__ 
        #include <GLUT/glut.h>
    #else
        #include <GL/glut.h>
        #include <GL/gl.h>
    #endif
    
    #include <pthread.h>
    
    #include <termios.h>
    #include <sys/time.h>
    #include <unistd.h>
#else
    #include <pios/mmu.h>
    #include <pios/native_io.h>
    #include <pios/uart.h>
    
    #include <pios/gpio.h>
    #include <pios/irq.h>
    #include <pios/timer.h>
#endif
 
#include "image.h"
#include "renderer.h"
#include "geometry.h"
#include "ui.h"
#include "settings.h"
#include "console.h" 
#include "matrix.h"
#include "camera.h"
#include "cmd.h"
#include "color.h"

// for printNum
#define pc pios_uart_putchar
#ifndef PIOS_PLATFORM_RPI
#define gc getchar
#else
#define gc pios_uart_getchar
#endif

void saveScreenshot ( ) ;
void crash ( int, void* ) __attribute__((noreturn));

#define WIDTH 480 // 320
#define HEIGHT 320 // 200
 
enum _state yil_state=CONSOLE;

struct yil_settings_data cfg; 
int CfgIDs [ YIL_MAX_SETTINGS ];

int c64_window, hwndPiSet, RenderWin;
 
extern unsigned char wall_data[];
extern unsigned char water_data[];
extern unsigned char stone_data[];
extern unsigned char boxtex_data[];
extern unsigned char fire_data[];

unsigned char* fire_ptr[10] = 
{
    fire_data,
    fire_data+128*128*4,
    fire_data+128*128*2*4,
    fire_data+128*128*3*4,
    fire_data+128*128*4*4,
    fire_data+128*128*5*4,
    fire_data+128*128*6*4,
    fire_data+128*128*7*4,
    fire_data+128*128*8*4,
    fire_data+128*128*9*4
};
 
struct rgb rdata[WIDTH*WIDTH];
struct rgba buffdata[WIDTH*HEIGHT*4];
#ifdef PIOS_PLATFORM_RPI
struct rgba* outdata;
#else
struct rgba outdata[WIDTH*HEIGHT*4];
#endif

struct Image* ptrimg, *outputimg, *buffimg, *renimg;
struct rgba* depthbuffer;
struct Image* fireTexture;
 
struct rgb blue = {
    .red=0, .green=0, .blue=200
};
struct rgb red = {
    .red=200, .green=0, .blue=0
};
struct rgb green = {
    .red=0, .green=200, .blue=0
};
struct rgb cyan = {
    .red=0, .green=200, .blue=200
};
struct rgb yellow = {
    .red=200, .green=200, .blue=0
};
struct rgb viola = {
    .red=200, .green=0, .blue=200
};

struct rgb fog = {
    .red=160, .green=160, .blue=200
};

struct rgb lfog = {
    .red=0x00, .green=0x00, .blue=0x00
};

struct rgb colors[4*6], colors2[4*6];
struct rgb black = { 0,0,0 };

struct _camera cam;
struct Image heightmap;

int32_t null = 0x7fffffff;
float texcoord[] = { 
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0 
};

float watercoord[] = { 
    1.0, 1.0,
    2.0, 1.0,
    2.0, 2.0,
    1.0, 2.0 
};

float stonecoord[] = { 
    0.0, 0.0,
    5.0, 0.0,
    5.0, 5.0,
    0.0, 5.0 
};

uint8_t mapdata[25*20] = {
    40, 40, 38, 36, 34, 32,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    40, 40, 38, 36, 34, 32,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    40, 40, 38, 36, 34, 32,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    40, 40, 38, 36, 34, 32,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    40, 40, 38, 36, 34, 32,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    
    30, 30, 30, 30, 30, 30,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    30, 30, 30, 30, 30, 30,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    30, 30, 30, 30, 30, 30,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    30, 30, 30, 30, 30, 30,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    30, 30, 30, 30, 30, 30,30,30,30,30,30,30, 30,30,30,30,30,30,30,30,
    
    28, 28, 28, 28, 28, 0,0,0,0,0,0,0,0,0,0, 28, 28, 28, 28, 28,
    26, 26, 26, 26, 26, 0,0,0,0,0,0,0,0,0,0, 26, 26, 26, 26, 26,
    24, 24, 24, 24, 24, 0,0,0,0,0,0,0,0,0,0, 24, 24, 24, 24, 24,
    22, 22, 22, 22, 22, 0,0,0,0,0,0,0,0,0,0, 22, 22, 22, 22, 22,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,

    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
    20, 20, 20, 20, 20, 0,0,0,0,0,0,0,0,0,0, 20, 20, 20, 20, 20,
};

int num = 1824;
float map[1824*4]={0};
float mapnormals[1824*4];

float l=-.15, n=.1f, f=100.f;

float meh[4] = {1,1,1,1};
float solidBox[4*6] = {
    1,1,1,1,
    1,1,1,1,
    1,1,1,1,
    1,1,1,1,
    1,1,1,1,
    1,1,1,1
};
float boxtexcoord[] = { 
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0 ,
    
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0 ,
    
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0 ,
    
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0 ,
    
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0 
};

float waterAlpha[4] = { .33, .33, .33, .33 };
float cz=0, cx=0, px=0, py=0, cy=0;

struct Image* wallTexture;
struct Image* waterTexture;
struct Image* stoneTexture;
struct Image* boxTexture;

// GEOMETRY
float beam[4 * 4 * 1 * 4], nBeam[4*4*4];
float boxL[6*16],          nBoxL[6*16];
float boxM[6*16],          nBoxM[6*16];
float boxS[6*16],          nBoxS[6*16];
float water[16*100],       nWater[16*100];
float firePlane[16*4],     nfire[16*4];

float cylinder[4 * 6 * 10 * 4], nCyl[4*6*10*4];
float cone[4 * 6 * 3 * 4 ], nCone[4*6*3*4];
  
int lightPoint, lightAmb, lightDir, lightFire;
  
#ifndef PIOS_PLATFORM_RPI
struct termios old_tio;
#endif

#ifdef PIOS_PLATFORM_RPI
uint32_t printNum ( uint32_t num, uint32_t base, uint32_t length )
{
    if ( base == 16 )
    {
        pc('0');
        pc('x');
    }
    uint32_t count=0;
    uint32_t veryold=num;
    do 
    {
        count++;
        num /= base;
    }
    while ( num > 0 );
    
    uint32_t i=0;
    for (; i<(length-count); i++)
    {
        pc('0');
    }

    uint32_t b = 1;
    for (uint32_t j = 1; j<count; j++)   
    {
        b *= base;      
    }

    num=veryold;
    for (; i<length; i++)
    {
        uint32_t old=num;
        uint32_t a = num / b;
        num = old - (a * b);;
        b /= base;
        uint32_t res = a;

        if (res <= 9)
        {
            pc(('0'+res));
        }
        else
        {
            pc(('A'+(res-10)));
        }
    }
    pc('\n');
    pc('\r');
    return length;
}
#endif

void init1 ()
{
    #ifdef PIOS_PLATFORM_RPI
    pios_uart_init();
    
    pios_uart_puts ("Bootin YAILFC\n\r");
    
    while ( 1 )
    {
        struct vc4_fbinfo* ptr = framebufferInit ( WIDTH*2, HEIGHT*2, 32 );
        if ( ptr == 0 )
        {
            pios_gpio_pinmode ( PIOS_IO_ACT_LED, PIOS_GPIO_OUTPUT );
            pios_gpio_write ( PIOS_IO_ACT_LED, PIOS_GPIO_HIGH );
            
            wait (1);
            
            pios_gpio_pinmode ( PIOS_IO_ACT_LED, PIOS_GPIO_OUTPUT );
            pios_gpio_write ( PIOS_IO_ACT_LED, PIOS_GPIO_LOW );
        
            wait(1);
        }
        else
        {   
            outdata = (struct rgba*)ptr->fb_ptr;
             
            pios_mmu_scrap_table ();
            pios_uart_puts ("\n\rEnable MMU\n\r");
            
            // set all sections to 1:1 virtual = physical translation, no cache, no buffers
            for ( uint32_t sections = 0 ; ; sections += 0x00100000 )
            {
                if ( sections < 0x20000000 )
                    pios_mmu_section ( sections, sections, PIOS_MMU_CACHE | PIOS_MMU_BUFFER );
                else
                    pios_mmu_section ( sections, sections, 0 );
                if ( sections >= 0x0fff00000 ) 
                    break;        
            }
            
            pios_mmu_init();
            pios_mmu_enable ( PIOS_CONTROL_M | PIOS_CONTROL_Z | PIOS_CONTROL_I | PIOS_CONTROL_C );
            
            return;
        }
    }
    #endif
}


void init(void )
{
    #ifndef PIOS_PLATFORM_RPI
    /* select clearing (background) color */
    glClearColor( 0.0, 0.0, 0.0, 0.0 );

    /* initialize viewing values */
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho( 0.0, 1.0, 0.0, 1.0, -1.0, 1.0 );
    glClear( GL_COLOR_BUFFER_BIT );
    glRasterPos2f ( 0, 0 );
    
	struct termios new_tio;
    /* get the terminal settings for stdin */
    tcgetattr(STDIN_FILENO,&old_tio);

    /* we want to keep the old setting to restore them a the end */
    new_tio=old_tio;

    /* disable canonical mode (buffered i/o) and local echo */
    new_tio.c_lflag &=(~ICANON);

    /* set the new settings immediately */
    tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
    #endif
}

void cleanup ()
{
    #ifndef PIOS_PLATFORM_RPI
    tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
    #endif
}

void noise ( struct Image* img, float alpha )
{   
    uint8_t rx = rand();
    int32_t r;
    const uint32_t a = 0xef00, b=1, c=73;
    uint8_t *ptr = img->payload;
    for (uint32_t y = 0; y < img->height; y++ )
    {
        //rx=rand();
        for ( uint32_t x = 0; x < img->width; x++ )
        {
            rx =  a*rx+b*rx + c;
            r = (rx)* alpha;
            for (int i=0; i<img->colorsize; i++)
            {
                if ( r+(*ptr) > 255 )
                    *ptr=255;
                else if ( r+(*ptr) < 0 )
                    *ptr=0;
                else
                    *ptr += r;
                ptr++;
            } 
        }
    }
} 

void yil_settings_read ()
{
    char a = getchar();
    switch (a)
    {
        case 's': 
            yil_settings_move_focus ( hwndPiSet, 1 );
            break;
        case 'w' :
            yil_settings_move_focus ( hwndPiSet, -1 );
            break;
        case ' ':
        {
            int focus = yil_settings_get_focus ();
            if ( yil_settings[focus].type == CHECKBOX )
            {
                (*(bool*)yil_settings[focus].value) = !(*(bool*)yil_settings[focus].value);
            }
            break;
        }
        case '0': case '1': case '2': case '3': case '4': 
        case '5': case '6': case '7': case '8': case '9':
        {
            int focus = yil_settings_get_focus ();
            if ( yil_settings[focus].type == TEXTBOX )
            {
                char* str = (char*) yil_settings[focus].value;
                strncat ( str, &a, 1 );
            }    
            break;
        }
        case '\b': case 'a': case '<':
        {
            int focus = yil_settings_get_focus ();
            if ( yil_settings[focus].type == TEXTBOX )
            {
                char* str = (char*) yil_settings[focus].value;
                str[strlen(str)-1]=0;
            }    
            break;
        }
        case 'q':
            if ( yil_state == SETTINGS )
                yil_state = CONSOLE;
            else if ( yil_state == SETTINGSINGAME )
                yil_state = RENDERER;
    }
}

void display ()
{
    #ifndef PIOS_PLATFORM_RPI
    swap (ptrimg, SWAP_Y);
    #endif
    if ( yil_context.extra.motionBlur )
    {        
        static bool firstRun = true;
        if (firstRun)
        {
            firstRun = false;
            resize ( buffimg, ptrimg, ((yil_context.upscale == YIL_BILINEAR) ? BILINEAR : NEAREST) );
            copy ( outputimg, buffimg, REPLACE );
        }   
        else
        {
            resize ( buffimg, ptrimg, ((yil_context.upscale == YIL_BILINEAR) ? BILINEAR : NEAREST) );
            blend ( outputimg, buffimg, yil_context.extra.motionBlurLevel );
        }
    }
    else
    {
        resize ( outputimg, ptrimg, ((yil_context.upscale == YIL_BILINEAR) ? BILINEAR : NEAREST) );  
    }
    
    if ( yil_context.extra.noise )
    {
        float myal = .055;
        noise ( outputimg, myal );  
    }
    
    #ifndef PIOS_PLATFORM_RPI
    glDrawPixels (WIDTH*2, HEIGHT*2, GL_RGBA, GL_UNSIGNED_BYTE, outdata);
    glutPostRedisplay();
    glFlush();
    #endif
}

void displayLoop ( void )
{
    while ( 1 )
    {
        bool waterEffect=false;
        // first inputs
        switch (yil_state)
        {
            case CONSOLE:
                pios_fbconsole_read();
                break;
            case SETTINGS: case SETTINGSINGAME:
                yil_settings_read();
                yil_settings_translate ( &cfg, &yil_context );
                break;
            case RENDERER:
            {
                char a = getchar();
                cx=0; cz=0; px=0; py=0;
                cy = 0;
                float phi = cam.yphi / 180 * M_PI;
                switch (a)
                {
                    case 'a':
                        cx=-.2 * cos ( phi );
                        cz=-.2 * sin ( phi );
                        break;
                    case 'd':
                        cx= .2 * cos ( phi );
                        cz= .2 * sin ( phi );
                        break;
                    case 'w':
                        cx= .2 * sin ( phi );
                        cz=-.2 * cos ( phi );
                        break;
                    case 's':
                        cx=-.2 * sin ( phi );
                        cz= .2 * cos ( phi );
                        break;
                    case 'b':
                        cy += 0.2;
                        break;
                    case 'g' :
                        cy -= 0.2;
                        break;
                    case 'i':
                        px=5;
                        break;
                    case 'k':
                        px=-5;
                        break;
                    case 'j':
                        py=-5;
                        break;
                    case 'l':
                        py=5;
                        break;
                    case '1':
                        yil_lightToggle ( lightAmb );
                        break;
                    case '2':
                        yil_lightToggle ( lightDir );
                        break;
                    case '3':
                        yil_lightToggle ( lightPoint );
                        break;
                    case '4':
                        yil_lightToggle ( lightFire );
                        break;
                    case 'c':
                        yil_context.map_constraints = !yil_context.map_constraints;
                        break;
                    case 't':
                        saveScreenshot();
                        break;
                    case 'f':
                        yil_context.ortho = !yil_context.ortho;
                        break;
                    case 'm': 
                        yil_state = SETTINGSINGAME;
                        break;
                }
                if (yil_context.map_constraints)
                {
                    cx = ((cam.tx >= 18.f && cx > 0.f) || (cam.tx <= 1.f && cx < 0.f)) ? 0 : cx;
                    cz = ((cam.tz >= 23.f && cz > 0.f) || (cam.tz <= 1.f && cz < 0.f)) ? 0 : cz;
                }

                yil_cameraMove ( &cam, cx, cy, cz );
                if ( cam.ty > 2.5 )
                {
                    waterEffect=true;;
                }
                
                yil_cameraRotate ( &cam, px, py, 0 );
                yil_lightMove ( lightPoint, cam.tx, cam.ty, cam.tz );
                break;
            }
            default:
                yil_state = CONSOLE;
                break;
        }
  
        // window managment
        switch (yil_state)
        {
            case CONSOLE:
                pios_window_hide ( hwndPiSet );
                pios_window_hide ( RenderWin );
                pios_window_show ( c64_window );
                break;
            case SETTINGS:
                pios_window_show ( hwndPiSet );
                pios_window_hide ( RenderWin );
                pios_window_show ( c64_window );
                yil_settings_invalidate ( hwndPiSet );
                break;
            case SETTINGSINGAME:
                pios_window_show ( hwndPiSet );
                pios_window_show ( RenderWin );
                pios_window_hide ( c64_window );
                yil_settings_invalidate ( hwndPiSet );
                break;
            case RENDERER:
            {
                        #ifndef PIOS_PLATFORM_RPI
                        struct timeval tv1, tv2;
                        gettimeofday ( &tv1, NULL );
                        #endif
                        
                        static int fireoffset = 1;
                        fireTexture->payload = fire_ptr[fireoffset];
                        fireoffset++;
                        if ( fireoffset >= 10 )
                            fireoffset = 0;
                                                
                        if ( !yil_context.ortho )
                            yil_frustumAdj ( yil_context.perspective_matrix, l, yil_context.viewport.xsize / (float)yil_context.viewport.ysize, n, f );   
                        else
                            yil_orthoAdj ( yil_context.perspective_matrix, l, yil_context.viewport.xsize / (float)yil_context.viewport.ysize, n, f );  
                            
                        static float WaterOffset = 0;
                        static float time = 0;
                        WaterOffset += cos(time)*.02;
                        time += .1;
                        //if ( WaterOffset > 1 ) WaterOffset++;
                        watercoord[0] = 1;
                        watercoord[1] = WaterOffset + 1;
                        watercoord[2] = 2;
                        watercoord[3] = WaterOffset + 1;
                        watercoord[4] = 2;
                        watercoord[5] = WaterOffset + 2;
                        watercoord[6] = 1;
                        watercoord[7] = WaterOffset + 2;
                   
                        yil_cylinder ( beam, 4*4, 4, 1, nBeam );
                   
                        struct yil_geometry bbeam;
                        bbeam.type = YIL_RECT;
                        bbeam.pts = beam;
                        bbeam.numPts = 4*4;
                        bbeam.texture = NULL;
                        bbeam.texcoord = boxtexcoord;
                        bbeam.normal = nBeam;
                        bbeam.shinyness = 0;
                        struct rgb beamcol = {
                            .red = 59, .green = 141, .blue = 189 // ,.alpha=0
                        };
                        struct rgb beam_col[] = {
                            beamcol, beamcol, beamcol, beamcol
                        };
                        bbeam.colors = beam_col;
                        float beam_alpha[16] = { 
                            .5, 0, 0, .5, 
                            .5, 0, 0, .5, 
                            .5, 0, 0, .5, 
                            .5, 0, 0, .5
                        };
                        bbeam.alpha = beam_alpha;
                        bbeam.loop  = true;
                        
                        struct yil_geometry ffire = 
                        {    
                            .type = YIL_RECT, 
                            .pts = firePlane, 
                            .numPts = 4,
                            .texture = fireTexture,
                            .texcoord = boxtexcoord,
                            .colors = beam_col,
                            .alpha = solidBox,
                            .loop = true,
                            .normal = nfire,
                            .shinyness = 0
                        };
                    
                        static float alpha = 0;
                        alpha+=3;
                        yil_scale ( beam, 1.5, 15, 1.5, 4*4 );
                        yil_rotate ( beam, YIL_X, 270, 4*4 );
                        yil_rotate ( beam, YIL_Z, 45, 4*4 );
                        yil_rotate ( beam, YIL_Y, alpha, 4*4 );
                        yil_translate ( beam, -1, -11, 25.5, 4*4 );  
                   
                        // init background AND deptbuffer
                        fill ( renimg, (yil_context.lighting_enable ? &lfog : &fog) );
                        struct rgba *ptr = (struct rgba*) depthbuffer;
                        for (uint32_t y = 0; y < HEIGHT; y++)
                        {
                            for (uint32_t x = 0; x < WIDTH; x++)
                            {
                                *ptr = *((struct rgba*) (&null));
                                ptr++;
                            }
                        }

                        /// transform the camera
                        //int indx = cam.ty + ((int)cam.tz)*20;
                                                
                        yil_geometry_drawAll ( renimg, &cam );
                        // disable lighting for the beam, as it is a beam and should look "illuminated" always!
                        bool old = yil_context.lighting_enable;
                        int oldFFC = yil_context.culling;
                        yil_context.lighting_enable = false;
                        yil_geometry_draw ( renimg, &bbeam, &cam );
                        yil_context.culling = YIL_BOTH;
                        yil_geometry_draw ( renimg, &ffire, &cam );
                        yil_context.culling = oldFFC;
                        yil_context.lighting_enable = old;
                        
                        /// if enabled: run fog
                        yil_context.fog_color = (yil_context.lighting_enable ? &lfog : &fog);
                        yil_fog ( renimg );
                        
                        if (waterEffect)
                        {
                            uint8_t* p = renimg->payload;
                            #define sadd8(a,b) a = ((a > 0xff - b) ? 0xff : a + b);
                            #define ssub8(a,b) a = ((a < b) ? 0x00 : a - b);
                            for ( int i = 0; i<renimg->width * renimg->height; i++)
                            {
                                *p = *p * 0.4 + 0x29 * 0.6;
                                p++;
                                *p = *p * 0.4 + 0x80 * 0.6;
                                p++;
                                *p = *p * 0.4 + 0xb9 * 0.6;
                                
                                p += renimg->colorsize-2;
                            }
                        }

                        #ifndef PIOS_PLATFORM_RPI
                        gettimeofday ( &tv2, NULL );
                        
                        if ( tv2.tv_sec > tv1.tv_sec )
                        {
                            tv2.tv_usec += (tv2.tv_sec - tv1.tv_sec)*1000000;
                        }
                        tv2.tv_usec -= tv1.tv_usec;
                                
                        char num[20]={'\0'};
                        int index=18;
                        while ( tv2.tv_usec > 0 )
                        {   
                            int a = tv2.tv_usec % 10;
                            tv2.tv_usec /= 10;
                            num[index]='0'+a;
                            index--;
                        }
                        
                        struct rgb white = {
                            .red = 0xbe, .green = 0xde, .blue = 0xce 
                        };
                        int off = printText ( renimg, 8, renimg->height - 32, &white, "mikrosec used: \0", false);
                        printText ( renimg, (++off*8), renimg->height - 32, &white, &num[++index], false);
                        #endif
                        
                        pios_window_hide ( hwndPiSet );
                        pios_window_hide ( c64_window );
                        pios_window_show ( RenderWin );
                break;
            }
            default:
                yil_state = CONSOLE;
                break;
        }
        
        pios_window_update ();
  
        display();
    }
}
 
void saveScreenshot ( ) 
{
    #ifndef PIOS_PLATFORM_RPI
    swap (outputimg, SWAP_Y);
    char name[14]={0};
    bool found=false;
    for ( int i=0; i<999; i++)
    {
        sprintf ( name, "yailfc%03d.raw", i );
        FILE *f = fopen ( name, "r" );
        if ( f == NULL )
        {
            found=true;
            break;
        }
        else
        {
            fclose(f);
        }
    }
    if (!found)
    {
        printf ("Could not save screenshot");
        return;
    }
    printf ("%s\n", name );

    FILE* f = fopen ( name, "a"); 
    uint8_t *ptr = outputimg->payload; 
    for ( uint32_t y = 0; y<outputimg->height; y++)
    {
        for ( uint32_t x=0; x<outputimg->width; x++)
        {
            for ( int i = 0; i<outputimg->colorsize; i++ )
                fputc ( ptr[i], f );
            ptr+=outputimg->colorsize;
        }
    }
    fclose(f);
    swap (outputimg, SWAP_Y);
    #endif
} 
 
int main(int argc, char* argv[])
{
    /* create a window with glut */
    #ifndef PIOS_PLATFORM_RPI
    glutInit( &argc, (char **) argv );
    glutInitDisplayMode( GLUT_SINGLE | GLUT_RGB );
    glutInitWindowSize( WIDTH*2, HEIGHT*2 );
    glutInitWindowPosition( 100, 100 );
    glutCreateWindow( "PiOS Linux Build" );

    atexit ( cleanup );

    init();
    #endif

    struct rgba dbuffer[WIDTH*HEIGHT];
    depthbuffer = dbuffer;

    struct rgb rendererdata[WIDTH*HEIGHT], consoledata[WIDTH*HEIGHT];
    struct Image rimg = createImage ( RGB, WIDTH, HEIGHT, (void*) rdata );
    struct Image pimg = createImage ( RGBA, WIDTH*2, HEIGHT*2, (void*) outdata ); 
    struct Image bimg = createImage ( RGBA, WIDTH*2, HEIGHT*2, (void*) buffdata ); 
    struct Image console = createImage ( RGB, WIDTH, HEIGHT, (void*) consoledata );
    struct Image renderimg = createImage ( RGB, WIDTH, HEIGHT, (void*) rendererdata );

    ptrimg = &rimg;
    outputimg = &pimg;
    buffimg = &bimg;
    renimg = &renderimg;
        
    // initiate the context 
    init_context ( depthbuffer, WIDTH, HEIGHT, &fog );
    struct Image wallTex  = createImage ( RGB, 256, 256, wall_data );     
    struct Image waterTex = createImage ( RGB,  64,  64, water_data );
    struct Image stoneTex = createImage ( RGB, 256, 256, stone_data );
    struct Image boxTex   = createImage ( RGB, 128, 128, boxtex_data );
    struct Image fireTex = createImage ( RGBA, 128, 128, (void*) fire_ptr[0] );
    wallTexture = &wallTex;
    waterTexture = &waterTex;
    stoneTexture = &stoneTex;
    boxTexture = &boxTex;
    fireTexture = &fireTex;
    
    yil_context.texture = wallTexture;
       
    cam = yil_cameraInit();
    yil_cameraSet ( &yil_ego );
    cam.tx = 17.5;
    cam.tz = 22.5;
    cam.ty=-4;
    cam.yphi = -90;
   
    yil_frustumAdj ( yil_context.perspective_matrix, l, yil_context.viewport.xsize / (float)yil_context.viewport.ysize, n, f );   
          
    heightmap = createImage ( GRAY, 20, 25, mapdata );
    yil_heightmap ( &heightmap, map, &num, +6, -.2, mapnormals );
    
    yil_planeEx ( water, 8, 10, 100*16, nWater );
    yil_plane ( firePlane, 16*4, nfire );
    
    // create the boxes
    yil_box ( boxL, 6*16, nBoxL );
    yil_box ( boxM, 6*16, nBoxM );
    yil_box ( boxS, 6*16, nBoxS );
        
    yil_cylinder ( cylinder, 6 * 10 * 4, 6, 10, nCyl );
    yil_cone ( cone, 6 * 3 * 4, 6, 3, nCone );
    
    // transform the fire
    yil_rotate ( firePlane, YIL_X, 90, 4 );
    yil_rotate ( nfire, YIL_X, 90, 4 );
    yil_scale ( firePlane, 2, 2, 2, 4 );
    yil_translate ( firePlane, 8,-2,7, 4 );

    // transform the boxes
    yil_rotate ( boxL, YIL_Y, 30, 4*6 );
    yil_rotate ( nBoxL, YIL_Y, 30, 4*6 );
    yil_translate ( boxL, 12,-1,7, 4*6 );
    
    yil_scale ( boxM, .5, .5, .5, 4*6 );
    yil_rotate ( boxM, YIL_Y, 25, 4*6 );
    yil_rotate ( nBoxM, YIL_Y, 25, 4*6 );
    yil_translate ( boxM, 14,-.5, 6.6, 4*6 );
    
    yil_scale ( boxS, .5, .5, .5, 4*6 );
    yil_rotate ( boxS, YIL_Y, 20, 4*6 );
    yil_rotate ( nBoxS, YIL_Y, 20, 4*6 );
    yil_translate ( boxS, 11.5,-2.5, 7.2, 4*6 );

    // transform the "water"-plane
    #ifndef PIOS_PLATFORM_RPI       // TODO: Why does the Pi crash here? Maybe division by 0?
    yil_scale ( water, 1.5, 1, 1.5, 4*100 );
    yil_translate ( water, 3.5, 2.5, 9, 4*100 );
    #endif   
    
    // transform the tower
    yil_scale ( cylinder, 2, 2, 2, 4 * 6 * 10 );
    yil_translate ( cylinder, -1, -12, 26, 4 * 6 * 10 );
            
    // top of the tower
    yil_scale ( cone, 2, 2, 2, 4*3*6 );
    yil_rotate ( cone, YIL_X, 180, 4*3*6 );
    // don't turn the normal vectors around, as they point nicely up
    //yil_rotate ( nCone, YIL_X, 180, 4*3*6 );
    yil_translate ( cone, -1, -12, 26, 4*3*6 );  

    struct rgb c[] = {
        blue, blue, blue, blue,
        red, red, red, red,
        green, green, green, green,
        cyan,cyan,cyan,cyan,
        yellow, yellow, yellow, yellow,
        viola, viola, viola, viola
    };  

    struct rgb c2[] = {
        blue, red, blue, green,
        red, green, red, cyan,
        green, cyan, green, yellow,
        cyan,yellow,cyan,viola,
        yellow, viola, yellow, blue,
        viola, blue, viola, red
    };
    
    for ( int i=0; i<4*6; i++)
    {
        colors[i] = c[i];
        colors2[i] = c2[i];
    }

    yil_geometry_add ( YIL_RECT, cone, 4*3*6, wallTexture, texcoord, colors2, meh, true, nCone, 20 );
    yil_geometry_add ( YIL_RECT, cylinder, 4*10*6, wallTexture, texcoord, colors2, meh, true, nCyl, 20 );
    yil_geometry_add ( YIL_RECT, boxL, 4*6, boxTexture, boxtexcoord, colors2, solidBox, false, nBoxL, 5 );
    yil_geometry_add ( YIL_RECT, boxM, 4*6, boxTexture, boxtexcoord, colors2, solidBox, false, nBoxM, 5 );
    yil_geometry_add ( YIL_RECT, boxS, 4*6, boxTexture, boxtexcoord, colors2, solidBox, false, nBoxS, 5);
    yil_geometry_add ( YIL_RECT, map, 1824, wallTexture, texcoord, colors2, meh, true, mapnormals, 20 );
    yil_geometry_add ( YIL_RECT, water, 4*100, waterTexture, watercoord, colors, waterAlpha, true, nWater, 2 );

    // lights
    struct rgb amb = {.red=0x25, .green=0x25, .blue=0x25 };
    lightAmb = yil_addAmbientLight ( (void*) &amb );
    
    struct rgb diff = {.red=0x78, .green=0x87, .blue = 0x9a };
    lightDir = yil_addLight ( DIRECTIONAL, yil_fixedcolor_black, &diff, NULL, 0, 0, 0 );

    struct rgb pointDiff = {.red=0x85, .green=0x80, .blue=0x50 };
    struct rgb pointSpec = {.red=0xab, .green=0xba, .blue=0x30 };
    struct rgb colFire = {.red=0xbc, .green=0x8a, .blue=0x38};
    lightPoint = yil_addLight ( POINT, yil_fixedcolor_black, &pointDiff, &pointSpec, cam.tx, cam.ty, cam.tz );
    lightFire = yil_addLight ( POINT, yil_fixedcolor_black, &colFire, yil_fixedcolor_black, 8, -2, 7 );
    yil_lightRange ( lightPoint, 20 );
    yil_lightRange ( lightFire, 5 );

    // c64 prompt
    struct rgb fillcolor = { .red=0x86, .green=0x7a, .blue=0xde };
    struct rgb innercolor = { .red=0x48, .green=0x3a, .blue=0xaa };
    pios_fbconsole_init ( &console, (void*)&fillcolor, (void*) &innercolor, (void*)&innercolor );
    fill ( &console, &fillcolor );
    
    yil_flatRectangle ( &console, 32, 32, WIDTH-32, HEIGHT-32, &innercolor );

    pios_fbconsole_setPosition ( 13, 2 );
    pios_fbconsole_write ( "**** COMMODORE 64 BASIC V2 ****\0" );
    pios_fbconsole_setPosition ( 8, 4 );
    pios_fbconsole_write ( "64 K RAM SYSTEM  38911 BASIC BYTES FREE\0" );
    pios_fbconsole_setPosition ( 3, 6 );
    pios_fbconsole_write ( "READY.\n\0" );
    pios_fbconsole_write ("$ \0");
    
    pios_fbconsole_addCommand ( "echo\0", echoCmd );
    pios_fbconsole_addCommand ( "help\0", help );
    pios_fbconsole_addCommand ( "uname\0", uname );
    pios_fbconsole_addCommand ( "run\0", run );
    pios_fbconsole_addCommand ( "cake\0", cake );
    pios_fbconsole_addCommand ( "exit\0", yil_exit );
    pios_fbconsole_addCommand ( "setting\0", setStart );
    pios_fbconsole_addCommand ( "control\0", setStart );
    pios_fbconsole_addCommand ( "clear\0", cmdClear );
    pios_fbconsole_addCommand ( "crash\0", cmdCrash );
    pios_fbconsole_addCommand ( "list\0", listCommands );

    pios_window_init ( &rimg );
    
    c64_window = pios_window ( 0,0, WIDTH, HEIGHT );
    pios_window_control_image ( c64_window, 0, 0, WIDTH, HEIGHT, &console );
    pios_window_border ( c64_window, 0, 0, 0, 0 );
    
    cfg.Wire = false;
    cfg.Texture = false;
    cfg.texture_interpol = false;
    cfg.Fog = false;
    cfg.Lighting = false;
    cfg.Bilinear = false;
    cfg.Blur = false;
    cfg.Noise = true;
    cfg.Depthtest = true;
    cfg.Culling = true;
    cfg.AlphaRender = false;
    cfg.BilinearInterpol = false;
    memset ( cfg.BlurLevel, 0, 50 ); 
    memset ( cfg.FogMin, 0, 50 ); 
    memset ( cfg.FogMax, 0, 50 ); 
    
    cfg.BlurLevel[0] = '0';
    cfg.BlurLevel[1] = '8';
    cfg.BlurLevel[2] = '0';
    
    cfg.FogMin[0] = '9';
    cfg.FogMin[1] = '0';
    cfg.FogMin[2] = '0';
    
    cfg.FogMax[0] = '9';
    cfg.FogMax[1] = '4';
    cfg.FogMax[2] = '0';
    
    yil_settings_translate ( &cfg, &yil_context );

    RenderWin = pios_window ( 0,0, WIDTH, HEIGHT );
    pios_window_control_image ( RenderWin, 0, 0, WIDTH, HEIGHT, renimg );
    pios_window_border ( RenderWin, 0, 0, 0, 0 );
        
    hwndPiSet = pios_window ( 50, 50, 380, 220 );
    pios_windowTitle ( hwndPiSet, "PiOS SettingsManager\0");

    CfgIDs[0]  = yil_settings_add ( hwndPiSet, "Enable Alpha Render", CHECKBOX, &cfg.AlphaRender );
    CfgIDs[1]  = yil_settings_add ( hwndPiSet, "Enable Textures", CHECKBOX, &cfg.Texture );
    CfgIDs[2]  = yil_settings_add ( hwndPiSet, "Bilinear Texture-Filter", CHECKBOX, &cfg.texture_interpol );
    CfgIDs[3]  = yil_settings_add ( hwndPiSet, "Pen and Ink Mode", CHECKBOX, &cfg.Wire );
    CfgIDs[4]  = yil_settings_add ( hwndPiSet, "Dark City", CHECKBOX, &cfg.Lighting );
    CfgIDs[5]  = yil_settings_add ( hwndPiSet, "Enable Noise", CHECKBOX, &cfg.Noise );
    CfgIDs[6]  = yil_settings_add ( hwndPiSet, "Upscale with Bilinear", CHECKBOX, &cfg.Bilinear );
    CfgIDs[7]  = yil_settings_add ( hwndPiSet, "Enable Depthtest", CHECKBOX, &cfg.Depthtest );
    CfgIDs[8]  = yil_settings_add ( hwndPiSet, "Enable Front Face Culling", CHECKBOX, &cfg.Culling );
    CfgIDs[9]  = yil_settings_add ( hwndPiSet, "Pretty Colours", CHECKBOX, &cfg.BilinearInterpol );
    CfgIDs[10] = yil_settings_add ( hwndPiSet, "Ignore Map Borders", CHECKBOX, &cfg.map_constraints );
    CfgIDs[11] = yil_settings_add ( hwndPiSet, "Enable Motion Blur", CHECKBOX, &cfg.Blur );
    CfgIDs[12] = yil_settings_add ( hwndPiSet, "Motion Blur level in %", TEXTBOX, cfg.BlurLevel );
    CfgIDs[13] = yil_settings_add ( hwndPiSet, "Enable Fog", CHECKBOX, &cfg.Fog );
    CfgIDs[14] = yil_settings_add ( hwndPiSet, "Fog Z-Minimum", TEXTBOX, cfg.FogMin );
    CfgIDs[15] = yil_settings_add ( hwndPiSet, "Fog Z-Maximum", TEXTBOX, cfg.FogMax );

    #ifndef PIOS_PLATFORM_RPI
    swap (&rimg, SWAP_Y);
    #endif

    #ifndef PIOS_PLATFORM_RPI
    glutDisplayFunc( displayLoop );
    glutMainLoop();
    #else
    displayLoop();
    void* lr;
    __asm volatile ( "mov %0, pc\n"
        : "=r" ( lr ) );
    crash ( 25, lr );
    #endif
    
    return 0;
}

enum crashFault 
{
    ABORT=1, 
    UNKNOWNIRQ=2, 
    DATA=3, 
    UNKNOWNINSTRUCTION=4, 
    RESET=5
};

void crash ( int fault, void* lr )
{
    uint8_t black[4] = { 0,0,0,0 };
    #ifndef PIOS_PLATFORM_RPI
    uint8_t red[4] = { 0xff, 0,0,0 };
    #else
    uint8_t red[4] = { 0x00, 0,0xff,0 };
    #endif
    
    fill ( outputimg, black );
    
    uint8_t* ptr[8] = {((uint8_t*)outputimg->payload)+(8*outputimg->width + 10) * outputimg->colorsize};
    for ( int i=1; i<4; i++)
        ptr[i] = ptr[i-1] + outputimg->width*outputimg->colorsize;
    
    ptr[4] = ptr[0] + 56*outputimg->width*outputimg->colorsize;
    for ( int i=5; i<8; i++)
        ptr[i] = ptr[i-1] + outputimg->width*outputimg->colorsize;
    
    
    for ( uint32_t x = 10; x < outputimg->width - 10; x++)
    {
        for ( int j=0; j<8; j++)
        {
            for ( int i = 0; i<outputimg->colorsize; i++)
            {
                ptr[j][i] = red[i];
            }
            ptr[j]+=outputimg->colorsize;
        }
    }
    
    uint8_t* ptr2[8] = {((uint8_t*)outputimg->payload)+(8*outputimg->width + 10) * outputimg->colorsize};
    for ( int i=1; i<4; i++)
        ptr2[i] = ptr2[i-1] + outputimg->colorsize;
    
    ptr2[4] = ptr[0] + (outputimg->width-4)*outputimg->colorsize;
    for ( int i=5; i<8; i++)
        ptr2[i] = ptr2[i-1] + outputimg->colorsize;

    for (uint32_t y = 10; y < 66; y++ )
    {
        for ( int j=0; j<8; j++)
        {
            for ( int i = 0; i<outputimg->colorsize; i++)
            {
                ptr2[j][i] = red[i];
            }
            ptr2[j]+=outputimg->width*outputimg->colorsize;
        }
    }
    
    char* msg;
    switch ( fault )
    {
        case ABORT:
            msg = "          ABORT Exception. Please reboot.          ";
            break;
        case UNKNOWNIRQ:
            msg = "    Uncaught IRQ. Can't recover. Please reboot.    ";
            break;
        case DATA:
            msg = "       DATA ABORT Exception. Please reb00t.        ";
            break;
        case UNKNOWNINSTRUCTION:
            msg = " Unknown instruction. Can't recover. Ple453 r3t44b ";
            break;
        case RESET:
            msg = "              Placeholder for RESET.               ";
            break;
        default:
            msg = "         Ups, your Pi ran into a problem.          ";
            break;
    }
    printText ( outputimg, 250, 20, red, msg, false );
    
    printText ( outputimg, 324, 40, red, "Guru Meditation  lr:", false );
    char lrStr[11]={0};
    sprintf ( lrStr, "%8p", (void*) lr ); 
    printText ( outputimg, 324+170, 40, red, lrStr, false );
    
    #ifndef PIOS_PLATFORM_RPI
    swap (outputimg, SWAP_Y);
    glDrawPixels (WIDTH*2, HEIGHT*2, GL_RGBA, GL_UNSIGNED_BYTE, outdata);
    glutPostRedisplay();
    glFlush();
    #endif
    
    while (1);
}


// interrupt service routines
#ifdef PIOS_PLATFORM_RPI
void __attribute__((interrupt("UNDEF"))) pios_exception_undef(void)
{
    void* lr;
    __asm volatile ( "mov %0, lr\n"
        : "=r" (lr) 
    );
    crash ( UNKNOWNINSTRUCTION, lr );
    while( 1 );
}
void __attribute__((interrupt("IRQ"))) pios_exception_irq(void)
{
    void* lr;
    __asm volatile ( "mov %0, lr\n"
        : "=r" (lr) 
    );
    crash ( UNKNOWNIRQ, lr );
    while( 1 );    
}
void __attribute__((interrupt("FIQ"))) pios_exception_fiq(void)
{
    void* lr;
    __asm volatile ( "mov %0, lr\n"
        : "=r" (lr) 
    );
    crash ( UNKNOWNIRQ, lr );
    while( 1 );
}
void __attribute__((interrupt("SWI"))) pios_exception_swi(void)
{
    void* lr;
    __asm volatile ( "mov %0, lr\n"
        : "=r" (lr) 
    );
    crash ( UNKNOWNIRQ, lr );
    while( 1 );
}
void __attribute__((interrupt("ABORT"))) pios_exception_abort(void)
{
    void* lr;
    __asm volatile ( "mov %0, lr\n"
        : "=r" (lr) 
    );
    crash ( DATA, lr );
    while( 1 );
}
void __attribute__((interrupt("ABORT"))) pios_exception_reset(void)
{
    void* lr;
    __asm volatile ( "mov %0, lr\n"
        : "=r" (lr) 
    );
    crash ( RESET, lr );
    while( 1 );
}
#endif
