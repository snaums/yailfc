/**
 * \file matrix.h
 * \author Stefan Naumann
 * \brief interface for matrix transformations
 * 
 * \page transformations Matrix transformations
 *
 * # Matrix transformations
 *
 * Transformations in YAILFC are not described as matrizes per se, but rather as
 * functions. For example, if you want to use a rotation, then use the function
 * yil_rotate(). It will use the rotation matrix implicitely but calculate 
 * the correct values for the coordinates and return the new coordinates that way. 
 *
 * This in turn means, that you have to build a stack essentially, i.e. do the 
 * operations you want done last first. 
 *
 * This model hopefully saves some multiplications of the matrix and just 
 * calculates the coordinates without the need of intermediate matrizes to 
 * represent the operations you want to do. 
 * 
 * ## Data types and models
 * 
 * 3D objects are described as float-array, i.e. as vectors with 4 float-components. 
 * The transformations are done with this float-model. The calculations to NDC 
 * or in NDC would not be possible in in integer-space. 
 *
 * Once the transformations are done, and the vertices or objects ought to be
 * rendered in the 2D-space, the points are converted to integer-values. A more
 * detailed description of the vertices would not be useful, as the pixel raster
 * used integer coordinates in itself. 
 *
 * ## Problems
 *
 * A problem would be that the programmer has to care him-/herself about saving
 * the old original vertex-data before any transformation is done. You need the 
 * old data in the next frame, when the camera might have changed or the 
 * projection matrix might have changed. It would be nicer to have the framework 
 * care about preserving the old values. 
 **/

#ifndef YIL_MATH
#define YIL_MATH

#include <stdint.h>
#include <stdio.h>

#include <math.h>

#ifndef M_PI
/// Pi for systems, where it is not yet defined
#define M_PI 3.14159265358979
#endif

#ifndef YIL_Z_MAX
/// the maximal value in Z-direction
#define YIL_Z_MAX 10000000
#endif

/**
 * \brief enumeration for usage on rotation
 **/
enum YIL_AXIS
{
    YIL_X,          ///< x axis
    YIL_Y,          ///< y axis
    YIL_Z           ///< noone reads this comment, right?
};

/**
 * \brief implements a matrix with matrix addition (for quadratic matrices only)
 * \param[inout] result the first matrix and also the result matrix
 * \param[in] b the second matrix
 * \param[in] num the number of rows of the matrices
 **/
void yil_matAdd ( float* result, float* a, int num );
/**
 * \brief implements a matrix with matrix multiplication (for quadratic matrices only)
 * \param[inout] a the first matrix and also the result matrix
 * \param[in] b the second matrix
 * \param[in] num the number of rows of the matrices
 **/
void yil_matMul ( float* a, float* b, int num );
/**
 * \brief implements a matrix with vector multiplication (for quadratic matrices only)
 * \param[in] a the first matrix
 * \param[inout] b the vector AND the result vector!
 * \param[in] num the number of rows of the matrix and vector
 **/
void yil_matVecMul ( float* a, float* b, int num );
/**
 * \brief scale every component of a matrix by a certain factor
 * \param[inout] a the matrix
 * \param[in] the factor
 * \param[in] num the number of rows of the matrix
 **/
void yil_matScale ( float* a, float factor, int num );
/**
 * \brief scale every component of a vector by a certain factor
 * \param[inout] a the vector
 * \param[in] the factor
 * \param[in] num the number of rows of the matrix
 **/
void yil_vecScale ( float* result, float a, int num );

/** 
 * \brief translate a vector by x,y,z
 * \param[inout] a the vector and the result vector
 * \paran[in] x the value in x-direction
 * \paran[in] y the value in y-direction
 * \paran[in] z the value in z-direction
 * \param[in] num the number of rows in the vector
 **/
void yil_translate ( float* a, float x, float y, float z, int num );
/** 
 * \brief rotate a vector around an axis by a specified angle
 * \param[inout] a the vector and the result vector
 * \paran[in] axis the axis for turning the point around
 * \paran[in] alpha the angle for the rotation
 * \param[in] num the number of rows in the vector
 **/
void yil_rotate ( float* a, enum YIL_AXIS axis, float alpha, int num );
/** 
 * \brief scale a vector by x,y,z
 * \param[inout] a the vector and the result vector
 * \paran[in] x the value in x-direction
 * \paran[in] y the value in y-direction
 * \paran[in] z the value in z-direction
 * \param[in] num the number of rows in the vector
 **/
void yil_scale ( float* a, float x, float y, float z, int num );

/**
 * \brief writes an matrix for frustum into result, for known aspect ratios
 * \param[out] result the identity frustum 
 * \param[in] l the left plane
 * \param[in] r the right plane
 * \param[in] t the top plane
 * \param[in] b the bottom plane
 * \param[in] n the near-plane
 * \param[in] f the far plane
 **/
void yil_frustum ( float* result, float l, float r, float t, float b, float n, float f );
/**
 * \brief writes an matrix for frustum into result, for known aspect ratios
 * \param[out] result the identity frustum 
 * \param[in] l the left plane
 * \param[in] aspect the aspect ration between width/height
 * \param[in] n the near-plane
 * \param[in] f the far plane
 * \note sets right=-l, and calculates good values for top (bottom = -top)
 **/
void yil_frustumAdj ( float* result, float l, float aspect, float n, float f );
/**
 * \brief writes an identify-matrix for frustum into result
 * \param[out] result the identity frustum 
 **/
void yil_frustumIdentity ( float* result );

/**
 * \brief generate an orthogonal matrix for projection
 * \param[out] result the resulting matrix
 * \param[in] l the left plane
 * \param[in] aspect the aspect ration between width/height
 * \param[in] n the near-plane
 * \param[in] f the far plane
 * \note sets right=-l, and calculates good valued for top (bottom = -top)
 **/
void yil_orthoAdj ( float* result, float l, float aspect, float n, float f );
/**
 * \brief generate an orthogonal matrix for projection
 * \param[out] result the resulting matrix
 * \param[in] l the left plane
 * \param[in] r the right plane
 * \param[in] t the top plane
 * \param[in] b the bottom plane
 * \param[in] n the near-plane
 * \param[in] f the far plane
 **/
void yil_ortho ( float* result, float l, float r, float t, float b, float n, float f );

/**
 * \brief convert points from float to int32_t
 * \param[out] out the points as integers
 * \param[in] in the points as floats
 * \param[in] num the number of points to be converted
 * \note assumes 4 components per point
 **/
void yil_copyPoints ( int32_t* out, float* in, int num );
/**
 * \brief print a flaot-vector for debug purposes
 * \param[in] a the vector
 * \param[in] num the number of rows in the vector
 **/
void yil_vecfDebug ( float* a, int num );
/**
 * \brief convert points from Normalized Device Coordinates (NDC) to Window Coordinates
 * \param[inout] a the vector and the result vector
 * \param[in] bounds proportions of the window / viewport (xmin, ymin, xmax, ymax)
 * \param[in] num the number of rows in the vector
 **/
void yil_NDC2Window ( float* vec, uint32_t* bounds, int num );
#endif
