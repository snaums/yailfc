#include "renderer.h"
#include "light.h"

int yil_addLight ( enum yil_lightType t, void* amb, void* diff, void* spec, float px, float py, float pz )
{
    int i = yil_context.lighting_num;
    if ( i >= YIL_MAX_LIGHT )
        return -1;

    yil_context.lighting_src[i].enable = true;

    yil_context.lighting_src[i].type = t;
    yil_context.lighting_src[i].coloramb = amb;
    yil_context.lighting_src[i].colordiff = diff;
    yil_context.lighting_src[i].colorspec = spec;
    if ( /*t == SPOT || */t == POINT )
    {
        yil_context.lighting_src[i].x = px;
        yil_context.lighting_src[i].y = py;
        yil_context.lighting_src[i].z = pz;
        yil_context.lighting_src[i].range = 100;
    }
    if ( t == DIRECTIONAL )
    {
        yil_context.lighting_src[i].x = 0;
        yil_context.lighting_src[i].y = -20;
        yil_context.lighting_src[i].z = -5;
    }
    
    yil_context.lighting_num++;
    
    return i;
}

void yil_lightDisable ( int light )
{
    if ( light >= 0 && yil_context.lighting_num > light )
    {
        yil_context.lighting_src[light].enable=false;
    }
}

void yil_lightEnable ( int light )
{
    if ( light >= 0 && yil_context.lighting_num > light )
    {
        yil_context.lighting_src[light].enable=true;
    }
}

void yil_lightToggle ( int light )
{
    if ( light >= 0 && yil_context.lighting_num > light )
    {
        yil_context.lighting_src[light].enable=!yil_context.lighting_src[light].enable;
    }
}

void yil_lightRange ( int light, float range )
{
    if ( light >= 0 && yil_context.lighting_num > light )
    {
        yil_context.lighting_src[light].range=range;
    }
}

int yil_addAmbientLight ( void* amb )
{
    int i = yil_context.lighting_num;
    if ( i >= YIL_MAX_LIGHT )
        return -1;
        
    yil_context.lighting_src[i].enable = true;
    yil_context.lighting_src[i].type = AMBIENT;
    yil_context.lighting_src[i].coloramb = amb;
    yil_context.lighting_num++;
    
    return i;
}

int yil_lightMove ( int light, float px, float py, float pz )
{
    if ( light >= 0 && yil_context.lighting_num > light )
    {
        yil_context.lighting_src[light].x = px;
        yil_context.lighting_src[light].y = py;
        yil_context.lighting_src[light].z = pz;
    }
    return 0;
}

int yil_lightPosition ( int light, float* coord )
{
    if ( light >= 0 && yil_context.lighting_num > light )
    {
        coord[0] = yil_context.lighting_src[light].x;
        coord[1] = yil_context.lighting_src[light].y;
        coord[2] = yil_context.lighting_src[light].z;
    }
    return 0;
}

#define sadd8(a,b) a = ((a > 0xff - b) ? 0xff : a + b);

int yil_light ( float* pts, struct yil_geometry* geo, struct rgb* colors, struct _camera* cam, float* normal )
{
    for ( int j = 0; j < geo->numPts; j++ )
    {
        struct rgb shiny = {0};
        colors[j].red = 0;
        colors[j].green = 0;
        colors[j].blue = 0;
        for ( int i = 0; i < yil_context.lighting_num; i++ )
        {
            struct yil_lightSource* ls = &yil_context.lighting_src[i];
            if ( ls->enable == false )
                continue;
                
            // ambient light color
            sadd8 ( colors[j].red, ((struct rgb*)ls->coloramb)->red );
            sadd8 ( colors[j].green, ((struct rgb*)ls->coloramb)->green );
            sadd8 ( colors[j].blue, ((struct rgb*)ls->coloramb)->blue );
            
            // diffuse light color
            float L[3] = { ls->x - pts[j*4+0], 
                           ls->y - pts[j*4+1], 
                           ls->z - pts[j*4+2] };
            float norm = sqrt ( L[0] * L[0] + L[1] * L[1] + L[2] * L[2] );
            if (norm != 0)
            {
                L[0] /= norm;
                L[1] /= norm;
                L[2] /= norm;
            }
            else
            {
                break;
            }
            
            if ( ls->type == DIRECTIONAL || ls->type == POINT /* || ls->type==SPOT*/ )
            {
                float intense = L[0] * normal[j*4] + L[1] * normal[j*4+1] + L[2] * normal[j*4+2];
                // decline of the POINT-sources lights
                if ( ls->type == POINT /* || ls->type == SPOT */ )
                {
                    float coeff = 1 - (norm / ls->range);
                    if (coeff < 0) 
                        coeff = 0;
                    intense *= coeff;
                }
                
                if ( intense > 0 ) 
                {
                    sadd8( colors[j].red,   ((struct rgb*)ls->colordiff)->red   * intense);
                    sadd8( colors[j].green, ((struct rgb*)ls->colordiff)->green * intense);
                    sadd8( colors[j].blue,  ((struct rgb*)ls->colordiff)->blue  * intense);  
                }
                
            }
            
            // specular lighting
            if ( ls->type == POINT /* || ls->type == SPOT*/ )
            {
                float E[3] = { 
                    cam->tx - pts[j*4+0],
                    cam->ty - pts[j*4+1],
                    cam->tz - pts[j*4+1]
                };
                float Enorm = sqrt ( E[0] * E[0] + E[1] * E[1] + E[2] * E[2] );
                E[0] /= Enorm;
                E[1] /= Enorm;
                E[2] /= Enorm;
                float H[3] = { L[0] - E[0],
                               L[1] - E[1],
                               L[2] - E[2] };
                float Hnorm = sqrt ( H[0] * H[0] + H[1] * H[1] + H[2] * H[2] );
                H[0] /= Hnorm;
                H[1] /= Hnorm;
                H[2] /= Hnorm;
                
                float intense = H[0] * normal[j*4] + H[1] * normal[j*4+1] + H[2] * normal[j*4+2];
                if ( intense > 0 )
                {
                    intense = powf ( intense, geo->shinyness );
                    
                    sadd8( shiny.red,   ((struct rgb*)ls->colorspec)->red   * intense);
                    sadd8( shiny.green, ((struct rgb*)ls->colorspec)->green * intense);
                    sadd8( shiny.blue,  ((struct rgb*)ls->colorspec)->blue  * intense);   
                }
            }
    
        }
                
        if (yil_context.texture == NULL || yil_context.texmode == YIL_TEX_NONE)
        {
            colors[j].red =   ((float) colors[j].red   / 255.f) * (((struct rgb*)geo->colors)[ (geo->loop ? j%4 : j) ].red   );
            colors[j].green = ((float) colors[j].green / 255.f) * (((struct rgb*)geo->colors)[ (geo->loop ? j%4 : j) ].green );
            colors[j].blue =  ((float) colors[j].blue  / 255.f) * (((struct rgb*)geo->colors)[ (geo->loop ? j%4 : j) ].blue  );
        }
        
        sadd8 ( colors[j].red, shiny.red );
        sadd8 ( colors[j].green, shiny.green );
        sadd8 ( colors[j].blue, shiny.blue );
    }
    
    return 0;
}
