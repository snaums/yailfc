/**
 * \file console.h
 * \author Stefan Naumann
 * defines functions for writing to the framebuffer commandline;
 * fb-console is tailored for working on the raspberry pi 
 * without hardware acceleration (for the better and worse)
 *
 * \page fbconsole_section Framebuffer-Console
 *
 * The framebuffer console can be used as a terminal emulator, which can execute
 * commands, when they were registered as commands beforehand. The normal steps
 * to set up the console are the following:
 * 1. run pios_fbconsole_init() to initiate the fb-console
 * 2. run pios_fbconsole_addCommand() as long as you want to add new commands
 *    to the command-list
 * 3. run pios_fbconsole_read() for reading a single command and executing it. 
 *    If the command input does not exist, an error message will be generated. 
 **/
  
#ifndef PIOS_FRMBFF_CONSOLE
#define PIOS_FRMBFF_CONSOLE

#include <stdio.h>
#include <string.h>

#include "image.h"
#include "fontd.h"

/// define the overscan of the console
#define PIOS_CONSOLE_OVERSCAN_X 10
/// define the overscan of the console
#define PIOS_CONSOLE_OVERSCAN_Y 10

/// maximal amount of commands to be added
#define PIOS_CONSOLE_MAX_COMMANDS 12
/// maximal length of the command-words
#define PIOS_CONSOLE_MAX_COMMAND_LENGTH 10
/// maximal length of the input buffer
#define PIOS_CONSOLE_MAX_INPUT_LENGTH 20

extern int pios_console_x,      ///< width of the framebuffer-console
           pios_console_y;      ///< height of the framebuffer-console
extern void* fgcolor,           ///< frontcolour 
           * bgcolor;           ///< background-colour
extern void* fillcolor;         

/// framebuffer for the framebuffer-console
extern struct Image* console_fb;

/**
 * \brief initiates the framebuffer-console prompt
 * \param[in] img the framebuffer to be used for console-things
 * \param[in] fg the foreground-colour
 * \param[in] bg the background-colour
 * \param[in] fill the fillcolour
 **/
void pios_fbconsole_init ( struct Image* img, void* fg, void* bg, void* fill );
/**
 * \brief set the colour triple (fg, bg, fill)
 * \param[in] fg the foreground-colour
 * \param[in] bg the background-colour
 * \param[in] fill the fillcolour
 **/
void pios_fbconsole_setColor ( void* fg, void* bg, void* fill );
/**
 * \brief set the position of the cursor in the console
 * \param[in] x the x-position of the cursor
 * \param[in] y the y-position of the cursor
 **/
void pios_fbconsole_setPosition ( int x, int y );
/**
 * \brief clear the console and redraw the background
 **/
void pios_fbconsole_clear( );

/**
 * \brief switch the buffer (maybe use as double-buffered console?)
 * \param[in] src the new buffer to be used
 **/
void pios_fbconsole_switchBuffer ( struct Image* src );

/**
 * \brief clear the current line (i.e. fill with backcolor)
 **/
int pios_fbconsole_clearLine ();

/**
 * \brief a simple write-command to write a string to the console
 * \param[in] str a string to be written to the console
 **/
void pios_fbconsole_write ( const char* str );

/**
 * \brief a simple read command, which reads characters (using getchar()) until \n, then executes the command
 **/
int pios_fbconsole_read ();
/**
 * \brief interprete a given command and execute it
 * \note the commands will get parameters with (int argc, char** argv)-semantics
 * \return the commands return-value
 **/
int pios_fbconsole_execute ();

/**
 * \brief returns the command character-string of the i-th command
 **/
char* pios_fbconsole_getCommand ( int i );
/**
 * \brief returns the number of registered commands
 **/
int pios_fbconsole_getCommandCount ();

/**
 * \brief add a command to the console
 * \param[in] cmd the name of the command, i.e. what does the user have to type to run the command?
 * \param[in] cmdfunc the function to be executed, when the command is recognized
 * \note the commands will get parameters with (int argc, char** argv)-semantics
 * \return true on success, false otherwise
 **/
bool pios_fbconsole_addCommand ( char* cmd, int (*cmdfunc) (int, char**) );

#endif
