/**
 * \file settings.h
 * \author Stefan Naumann
 * \brief a simple interface to add a list of settings to a PiOS-window
 *
 * \page settings_section Settings
 *
 * The settings-interface is used to generate a list of settings and put it into
 * a PiOS Window (with the given handle). Settings consist of a name (for the
 * label of the control element) and a (value, type)-pair. The type can only be
 * CHECKBOX or TEXTBOX at the moment. Every input has to be processed from a 
 * calling function, at the moment there is no way to process input from the 
 * settings-interface itself
 **/

#ifndef YIL_UI_SETTINGS
#define YIL_UI_SETTINGS

#include "ui.h"
#include "renderer.h"

/// the maximal number of settings to be added
#define YIL_MAX_SETTINGS 16
/// the maximal number of settings to be shown on the Window
#define YIL_SETTINGS_SHOW_MAX 7

/**
 * \brief a struct describing settings for displaying in a window
 **/
struct yil_ui_setting 
{
    const char* name;       ///< the name of the setting to be used as Label-text
    void* value;            ///< a ptr to the value to be modified, when input dictates so
    enum CtrlType type;     ///< the type of the wanted control-element (LABEL | CHECKBOX)
    int hwnd;               ///< handle of the window, to which the controls are attached
};

/// a list of settings
extern struct yil_ui_setting yil_settings [ YIL_MAX_SETTINGS ];

/**
 * \brief contains the values of the real settings of the program
 **/
struct yil_settings_data
{
    bool Wire,                  ///< enable Wireframe mode
         Texture,               ///< enable textures
         Fog,                   ///< enable Fog
         Lighting,              ///< enable Lighting    
         Bilinear,              ///< enable Bilinear Upscaling
         Blur,                  ///< enable Motion Blur
         Noise,                 ///< enable noise on the image
         Depthtest,             ///< enable Depthtest
         Culling,               ///< enable Front Face Culling
         BilinearInterpol,      ///< enable bilinear colour interpolation
         AlphaRender,           ///< enable Alpha-Rendering mode (disables Wireframe)
         texture_interpol,      ///< enable Bilinear texture filtering
         map_constraints;       ///< ignore the map-constraints
    char BlurLevel[50],         ///< the blur-level (should stay between 0, 100), a good value may be 90
         FogMin[50],            ///< the fog-min level, z-value where the fog should start
         FogMax[50];            ///< the fog-max level, z-value where the fog is no longer transparent
};

/**
 * \brief translates the yil_settings_data struct into the drawing context yil_ctx
 * \param[in] data struct that is generated / modified by manipulating the settings
 * \param[out] ctx the new context, which has accepted all the user-made changes
 **/
int yil_settings_translate ( struct yil_settings_data* data, struct yil_ctx* ctx );

/**
 * \brief add a new property to the dialog
 * \param[in] hwnd handle to the window
 * \param[in] name for the setting / property
 * \param[in] type the type of the control
 * \param[inout] value the value of the control-element
 **/
int yil_settings_add ( int hwnd, const char* name, enum CtrlType type, void* value );
/**
 * \brief moves the whole screen byLines lines to the bottom or top
 * \param[in] hwnd the handle to the window
 * \param[in] byLines move by how may lines?
 **/
int yil_settings_moveRow ( int hwnd, int byLines );
/**
 * \brief moves the focus byLines lines to the bottom or top
 * \param[in] hwnd the handle to the window
 * \param[in] byLines move by how may lines?
 * \note when the bottom / top is reached yil_settings_moveRow() may be called
 **/
int yil_settings_move_focus ( int hwnd, int byLines );
/**
 * \brief invalidate the settings-window and redraw it (emtpy, add new control-elements, redraw)
 * \param[in] hwnd the handle to the window
 **/
int yil_settings_invalidate ( int hwnd );
/**
 * \brief returns the index of the currently focused settings-element
 **/
int yil_settings_get_focus ();

#endif
