/**
 * \file texture.h
 * \author Stefan Naumann
 * \brief interface for managing texture as well as render them "onto" objects
 **/
 
#ifndef YIL_TEXTURE
#define YIL_TEXTURE

#include <stdint.h>

#include "renderer.h"
#include "image.h"

/**
 * \brief calculates the colour of a pixel given the coordinates of a point on the texture and the ptr to the framebuffer for the according pixel
 * \param[in] img the framebuffer 
 * \param[in] ptr the pointer to the framebuffer location of the pixel
 * \param[in] alpha an alpha-value to blend with (i.e. don't replace the framebuffer content, but blend)
 * \param[in] tx the x-coordinate of the pixel inside the texture
 * \param[in] ty the y-coordinate of the pixel inside the texture
 * \param[in] icol the colour value of the lighting-operation
 * \note will blend with lighting-colours of lighting_enable is set true; use return value 1 for resetting the depthbuffer to original value
 * \warn resetting the depth-buffer with 1 return value may result in arkward behaviour
 * \return 1 if alpha-blend with an alpha < 0.999, 0 otherwise
 **/
int yil_texturize ( struct Image* img, uint8_t* ptr, float alpha, float tx, float ty, void* icolor );

/**
 * \brief texturize a point in the framebuffer with an RGBA-image (i.e. Image with Alpha-value)
 * \param[in] ptr the pointer to the framebuffer position to be coloured in
 * \param[in] color the color to be used
 * \param[in] colorsize the size of the a pixel in the framebuffer
 * \param[in] overallAlpha is an alpha-value which the texture has overall; will be multiplied to the alpha-value of the texture itself
 * \return 1 if the final alpha is < 0.999, 0 otherwise
 **/
int yil_texturizeAlpha ( uint8_t* ptr, uint8_t* color, int colorsize, float overallAlpha );

/*int yil_addTexture ( struct Image* img, int flags );
int yil_removeTexture ( int index );
int yil_replaceTexture ( int index, struct Image* newTex, int flags );
struct Image* yil_indexTexture ( int index );
int yil_setTextureCoordinates ( int intex, float tx, float ty );

int yil_createMipMap ( struct Image* img, struct Image* target );*/

#endif
