#include "image.h"
#include "fontd.h"

#include <stdio.h>

#define TABSIZE 4
#define YIL_NUM_FORMAT 4

void (*fn_brighten[YIL_NUM_FORMAT]) (struct Image*, int16_t) = { brightenRGB, yil_brightenRGBA, NULL, NULL };
void (*fn_contrast[YIL_NUM_FORMAT]) (struct Image*, int16_t) = { contrastRGB, yil_contrastRGBA, NULL, NULL };
int (*fn_resize[YIL_NUM_FORMAT]) (struct Image*, struct Image*, enum Interpolation_t) = { resizeRGB, yil_resizeRGBA, NULL, NULL };
void (*fn_swap[YIL_NUM_FORMAT]) (struct Image*, int) = {swapRGB, yil_swapRGBA, NULL, NULL};
int (*fn_rotate[YIL_NUM_FORMAT]) (struct Image*, struct Image*, float, enum Interpolation_t) = { rotateRGB, yil_rotateRGBA, NULL, NULL };
int (*fn_printChar[YIL_NUM_FORMAT]) (struct Image*, uint32_t, uint32_t, void*, const char) = { printCharRGB, yil_printCharRGBA, NULL, NULL };
int (*fn_fill[YIL_NUM_FORMAT]) (struct Image*, void*) = {fillRGB, yil_fillRGBA, NULL};
int (*fn_copy[YIL_NUM_FORMAT]) (struct Image*, struct Image*, enum CopyMode) = {copyRGB, yil_copyRGBA, NULL, NULL};
void (*fn_interpolate[YIL_NUM_FORMAT]) (void *, void*, float*, int ) = {yil_interpolateRGB, yil_interpolateRGBA, NULL, NULL};
void* (*fn_get[YIL_NUM_FORMAT]) (struct Image*, int, int) = { getRGB, yil_getRGBA, NULL, getGray };
void (*fn_set[YIL_NUM_FORMAT]) (struct Image*, int, int, void*) = {setRGB, yil_setRGBA, NULL, NULL };
void (*fn_blend[YIL_NUM_FORMAT]) (struct Image*, struct Image*, float) = { blendRGB, yil_blendRGBA, NULL, NULL };
int (*fn_sprite[YIL_NUM_FORMAT]) (struct Image*, struct Image*, int32_t, int32_t, uint32_t, uint32_t) = { yil_spriteRGB, NULL, NULL, NULL };

/// sizes of colors for reading in accordance to the bitdepth-enum
int _csizes [YIL_NUM_FORMAT] = { 3, 4, 3, 1 };

struct Image createImage ( enum ImageColor bitdepth, int32_t w, int32_t h, void* payload )
{
    struct Image img;
    img.bitdepth = bitdepth;
    img.width = w;
    img.height = h;
    img.payload = payload;
    img.colorsize = _csizes[(int) bitdepth];
    
    img.vtable.brighten = fn_brighten[(int) bitdepth];
    img.vtable.contrast = fn_contrast[(int) bitdepth];
    img.vtable.resize   = fn_resize  [(int) bitdepth];
    img.vtable.swap     = fn_swap    [(int) bitdepth];
    img.vtable.rotate   = fn_rotate  [(int) bitdepth];
    img.vtable.printChar = fn_printChar [(int) bitdepth];
    img.vtable.fill     = fn_fill [(int) bitdepth];
    img.vtable.copy     = fn_copy [(int) bitdepth];
    img.vtable.interpolate = fn_interpolate[(int) bitdepth];
    img.vtable.get = fn_get[(int) bitdepth];
    img.vtable.set = fn_set[(int) bitdepth];
    img.vtable.blend = fn_blend[(int) bitdepth];
    img.vtable.sprite = fn_sprite[(int) bitdepth];
    
    return img;
}

void yil_flatRectangle ( struct Image* img, int x, int y, int xend, int yend, void* color )
{
    uint8_t *ptr;
    for (int i=y; i<yend; i++)
    {
        ptr = (uint8_t*) img->payload;
        ptr+=(i*img->width + x)*img->colorsize;
        for (int j=x; j<xend; j++)
        {
            setPtr ( img->bitdepth, ptr, color );
            ptr+=img->colorsize;
        }
    }
}

void* get ( struct Image* img, int x, int y )
{
    if (img->vtable.get != NULL)
        return img->vtable.get( img, x, y );
    return NULL;
}
void set ( struct Image* img, int x, int y, void* color )
{
    if (img->vtable.set != NULL)
        img->vtable.set ( img, x, y, color );
}



void setPtrAlpha ( struct Image* img, void* ptr, void* color, float alpha )
{
    uint8_t* col = (uint8_t*) color;
    uint8_t* p = (uint8_t*) ptr;
    uint8_t buffIn[img->colorsize*2];
    for (int i=0; i<img->colorsize; i++)
    {
        buffIn[i] = col[i];
        buffIn[i+img->colorsize] = p[i];
    }
    uint8_t buffer[img->colorsize];
    float a[2] = { alpha, 1-alpha };

    yil_interpolateColors ( img, buffer, buffIn, a, 2 );
    
    uint8_t *b = buffer;   
    for (int i=0; i<img->colorsize; i++)
    {
        *p = *b;
        p++;
        b++;
    }
}

void setPtr ( enum ImageColor index, void* ptr, void* color )
{
    uint8_t* col = (uint8_t*) color;
    uint8_t* p = (uint8_t*) ptr;
    
    if (color == NULL)
        return;

    for (int i=0; i<_csizes[index]; i++)
    {
        *p = *col;
        p++;
        col++;
    }
}

int yil_sprite ( struct Image* backdrop, struct Image* sprite, int32_t x, int32_t y, uint32_t width, uint32_t height )
{
    if (backdrop->vtable.sprite != NULL && backdrop->vtable.sprite == sprite->vtable.sprite)
        return backdrop->vtable.sprite(backdrop, sprite, x, y, width, height);
    return -1;
}

int yil_spriteRGB ( struct Image* backdrop, struct Image* sprite, int32_t x, int32_t y, uint32_t width, uint32_t height )
{
    struct rgb* bdptr = (struct rgb*) backdrop->payload;
    struct rgb* spptr = (struct rgb*) sprite->payload;
    
    // competely in bounds, no boundry-checks needed
    if (x >= 0 && y >= 0 && width + x <= backdrop->width && height + y <= backdrop->height )
    {
        bdptr += (backdrop->width * y) + x;
        
        for (uint32_t h = 0; h < sprite->height && h<height; h++)
        {
            for (uint32_t w = 0; w < sprite->width && w<width; w++)
            {
                *bdptr = *spptr;
                bdptr++;
                spptr++;
            }
            spptr = (struct rgb*) sprite->payload + sprite->width * h;
            bdptr = (struct rgb*) backdrop->payload;
            bdptr += backdrop->width * (y+h) + x;
        }
    }
    // check for every line | pixel if in bounds of the backdrop
    else
    {
        int32_t xoff = 0, yoff = 0;
        if ( y >= 0 )
            bdptr += backdrop->width * y;
        else
        {
            yoff = y;
            spptr -= sprite->width * yoff;
        }
        if ( x >= 0 )
            bdptr += x;
        else
        {
            xoff = x;
            spptr -= xoff;
        }
        
        /**
         * yoff, xoff is either 0 or a negative value if the sprite begins left outside of the backdrop 
         **/
        for (uint32_t h = 0; h < sprite->height + yoff && h < height + yoff; h++)
        {
            if ( h + (y>=0 ? y : 0) >= backdrop->height)
                return 0;
            for (uint32_t w = 0; w < sprite->width + xoff && w < width + xoff; w++)
            {
                if ( w + (x>=0 ? x : 0) >= backdrop->width )
                    break;
                *bdptr = *spptr;
                bdptr++;
                spptr++;
            }
            spptr = (struct rgb*) sprite->payload + sprite->width * (h-yoff) - xoff;
            bdptr = (struct rgb*) backdrop->payload;
            bdptr += backdrop->width * (y+h) + x;
        }
    }
    return 0;
}

void yil_interpolateColors ( struct Image* img, void *colbuff, void* colors, float* coeff, int num )
{
    if (img->vtable.interpolate != NULL)
        img->vtable.interpolate ( colbuff, colors, coeff, num );
}

void brighten ( struct Image* img, int16_t value )
{
    if (img->vtable.brighten != NULL)
        img->vtable.brighten(img, value);
}

void swap ( struct Image* img, int type )
{
    if (img->vtable.swap != NULL)
        img->vtable.swap(img, type);
}

void contrast ( struct Image* img, int16_t value )
{
    if (img->vtable.contrast != NULL)
        img->vtable.contrast( img, value );
}

int rotate ( struct Image* dest, struct Image* src, float alpha, enum Interpolation_t interpolation )
{
    if (src->bitdepth != dest->bitdepth)
        return -2;
        
    if (src->vtable.rotate != NULL)
        return src->vtable.rotate ( dest, src, alpha, interpolation );
        
    return -1;
}

int resize ( struct Image* dest, struct Image* src, enum Interpolation_t interpolation )
{
    if (src->bitdepth == RGB && dest->bitdepth == RGBA )
        resizeRGB2RGBA ( dest, src, interpolation );

    if (src->bitdepth != dest->bitdepth)
        return -2;
    
    if (src->vtable.resize != NULL)
        return src->vtable.resize ( dest, src, interpolation );
    
    return -1;
}

int fill ( struct Image* img, void* color )
{
    if (img->vtable.fill != NULL)
        return img->vtable.fill ( img, color );
    
    return -1;
}

int copy ( struct Image* dest, struct Image* src, enum CopyMode mode )
{
    if ( src->bitdepth == RGB && dest->bitdepth == RGBA )
        return copyRGB2RGBA ( dest, src, mode );

    if (src->bitdepth != dest->bitdepth)
        return -2;
        
    if (src->width != dest->width || src->height != dest->height)
        return -3;
    
    if (src->vtable.copy != NULL)
        return src->vtable.copy ( dest, src, mode );
    
    return -1;
}

int printIcon ( struct Image* img, uint32_t x, uint32_t y, void* color, uint8_t* icon, int xsize, int ysize )
{
    uint8_t *fptr = icon;
    struct rgb* imgptr = (struct rgb*) img->payload + x + y*img->width;
    for (int yc = 0; yc<ysize; yc++)
    {
        int cx = 0;
        for (int xc=0; xc<xsize; xc+=8)
        {
            cx++;
            uint8_t mask = 0x80;
            while ( mask > 0 )
            {
                if (((*fptr)&mask) != 0)
                {
                    *imgptr = *(struct rgb*) color;
                }
                mask >>= 1;
                imgptr++;
            }
            fptr ++;
        }
        imgptr+=img->width;
        imgptr-=xsize;
    }
    return 1;
}

int printText ( struct Image* img, int x, int y, void* color, const char* str, bool home )
{
    int characters=0;
    int startx=x;
    if (img->vtable.printChar == NULL)
        return -2;
    if ((x+ms_font_info.x) > img->width)
    {
        y += ms_font_info.y;
        if (home)
            x=0;
        else
            x=startx;
    }
    if (y > img->height) 
        return -1;
    while (*str && *str!='\n' && *str!='\r')
    {
        if ((*str)=='\t')
        {
            for (int i=0; i<TABSIZE; i++)
            {
                img->vtable.printChar ( img, x, y, color, ' ' );
            }
            
            characters+=TABSIZE;
            x += ms_font_info.x * TABSIZE;
            if ((x+ms_font_info.x) > img->width)
            {
                y += ms_font_info.y;
                if (home)
                    x=0;
                else
                    x=startx;
            }
            if (y > img->height) 
                return characters;
        }  
        else
        {    
            characters++;
            img->vtable.printChar ( img, x, y, color, *str );
        
            x += ms_font_info.x;
            if ((x+ms_font_info.x) > img->width)
            {
                y += ms_font_info.y;
                if (home)
                    x=0;
                else
                    x=startx;
            }
            if (y > img->height) 
                return characters;
        }
        str++;   
    }
    return characters;
}

void blend ( struct Image* dest, struct Image* src, float alpha )
{
    if (src->bitdepth != dest->bitdepth)
        return ;
    
    if (src->vtable.blend != NULL)
        src->vtable.blend ( dest, src, alpha );
    return;
}

int filter ( struct Image* img, void (*filterFunc) ( struct Image*, uint8_t *, void* ), void* data)
{
    uint8_t* ptr = (uint8_t*) img->payload;
    for (int y=0; y<img->height; y++)
    {
        for (int x=0; x<img->width; x++)
        {
            filterFunc ( img, ptr, data );
            ptr+=img->colorsize;
        }   
    }
    return 0;
}

