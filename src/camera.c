#include "camera.h"
#include "matrix.h"

void (*fn_camera) ( struct _camera*, float*, int );

struct _camera yil_cameraInit ()
{
    fn_camera = NULL;
    struct _camera cam = { .tx=0, .ty=0, .tz=0, .xphi =0, .yphi=0, .zphi=0 };
    return cam;
}

//void yil_cameraLookAt ( struct _camera* cam, float x, float y, float z, float tx, float ty, float tz, float ux, float uy, float uz );
void yil_cameraMove ( struct _camera* cam, float x, float y, float z )
{
    cam->tx += x;
    cam->ty += y;
    cam->tz += z;
}
void yil_cameraRotate ( struct _camera* cam, float xphi, float yphi, float zphi )
{
    cam->xphi += xphi;
    cam->yphi += yphi;
    cam->zphi += zphi;
}
void yil_camera ( struct _camera* cam, float* pts, int num )
{
    if (fn_camera != NULL)
    {
        fn_camera ( cam, pts, num );
    }
}

void yil_cameraSet ( void (*fn)(struct _camera*, float*, int) )
{
    fn_camera = fn;
}

void yil_ego ( struct _camera* cam, float* pts, int num )
{
    yil_translate ( pts, -cam->tx, -cam->ty, -cam->tz, num);
    yil_rotate ( pts, YIL_Y, cam->yphi, num );
    yil_rotate ( pts, YIL_X, cam->xphi, num );
}
