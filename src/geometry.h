/**
 * \file geometry.h
 * \author Stefan Naumann
 * \brief geometry based functions
 **/

#ifndef YIL_GEOMETRY
#define YIL_GEOMETRY

#include "image.h"
#include "camera.h"
#include "matrix.h"
#include "renderer.h"

#include <math.h>
#include <stdint.h>

/// maximal number of geometry objects to be saved in the global array
#define YIL_GEOMETRY_MAX 10

/**
 * \brief describes the type of the primitives contained in a field of points
 **/
enum yil_geometry_primitive
{
    YIL_RECT=4,           ///< the object contains rectangles
    YIL_TRI=3,            ///< triangles
    YIL_LINE=2,           ///< lines only
    YIL_UNDEF=0           ///< undefinied - why would you do that?
};

/**
 * \brief describes 3D-objects
 **/
struct yil_geometry
{
    enum yil_geometry_primitive type;       ///< what primitives are contained within?
    float* pts;                             ///< the field of points (x,y,z,w, ... )
    int numPts;                             ///< the number of points in pts
    struct Image* texture;                  ///< the texture of the object (or NULL if none)
    float* texcoord;                        ///< the texture coordinates (please do always!)
    void* colors;                           ///< an array of colors (please do set!) 
    float* alpha;                           ///< alpha-values per vertex
    bool loop;                              ///< do you have defined color, texcoord and alpha-values per every vertex or do they loop on per primitive level?
    float* normal;                          ///< array of normals for the lighting-calculations
    float shinyness;                        ///< coefficient for the shinyness for specular light calculations
};

/// the number of currently saved static 3d-geometry
extern int yil_geometry_num;
/// static geometry data, which can be rendered one after another without object-space transformations
extern struct yil_geometry yil_geometry_array[YIL_GEOMETRY_MAX];

/**
 * \brief add a geometry object to the list of static objects
 * \param[in] type what primitives are contained within?
 * \param[in] pts the field of points (x,y,z,w, ... )
 * \param[in] numPts the number of points in pts
 * \param[in] texture the texture of the object (or NULL if none)
 * \param[in] texcoord the texture coordinates (please do always!)
 * \param[in] colors an array of colors (please do set!) 
 * \param[in] alpha alpha-values per vertex
 * \param[in] loop do you have defined color, texcoord and alpha-values per every vertex or do they loop on per primitive level?
 * \param[in] normals buffer for the normals of the object
 * \param[in] shinyness coefficient for the shinyness for specular light calculations
 * \return the struct created
 **/
struct yil_geometry* yil_geometry_add ( enum yil_geometry_primitive type, float* pts, int numPts, struct Image* texture, float* texcoord, void* colors, float* alpha, bool loop, float* normals, float shinyness );
/**
 * \brief draw all geometry objects saved in the array
 * \param[inout] img image-object to draw to
 * \param[in] cam the camera object for the camera-transformation
 **/
int yil_geometry_drawAll ( struct Image* img, struct _camera* cam );
/**
 * \brief draw a geometry object
 * \param[inout] img image-object to draw to
 * \param[in] geo the geometry you want to draw
 * \param[in] cam the camera object for the camera-transformation
 **/
int yil_geometry_draw ( struct Image* img, struct yil_geometry* geo, struct _camera* cam );

/**
 * \brief create vertex-data (rectangles) from an greyscale-image (heightmap)
 * \param[in] height the input-heightmap
 * \param[out] pts the vertex-data created from heightmap (should be big enough!)
 * \param[inout] num the number of vertices(!) that can be stored in pts; afterwards: the number of vertices that have been stored into pts
 * \param[in] offset a number that will be added to the y-value of every vertex
 * \param[in] scale a scaling-factor that will be multiplied to every vertex' y-value
 * \param[out] normals buffer for the normals of the object
 **/
void yil_heightmap ( struct Image* height, float* pts, int *num, float offset, float scale, float* normals );

/**
 * \brief copy coordinates of a box to buff (from -1 to 1 on every axis)
 * \param[out] buff the output buffer
 * \param[in] num the space in buff in floats
 * \param[out] normals buffer for the normals of the object
 **/
void yil_box ( float* buff, int num, float* normals );
/**
 * \brief copy coordinates of a plane to buff (from 0 to 1 on every axis)
 * \param[out] buff the output buffer
 * \param[in] num the space in buff in floats
 * \param[out] normals buffer for the normals of the object
 **/
void yil_plane ( float* buff, int num, float* normals );

/**
 * \brief generates a plane and saves it inton the buff-parameter; generates several rectangles!
 * \param[out] buff the output buffer
 * \param[in] x the number of rectangles in x-direction
 * \param[in] z the number of rectangles in z-direction
 * \param[in] num the space in buff in floats
 * \param[out] normals buffer for the normals of the object
 **/
void yil_planeEx ( float* buff, int x, int z, int num, float* normals );

/**
 * \brief copy coordinates from in to buff
 * \param[in] in the input buffer
 * \param[out] buff the output buffer
 * \param[in] minNum the number of floats in in
 * \param[in] num the space in buff in floats
 * \param[out] normalbuff buffer for the normals of the object
 * \param[in] normalIn the input buffer for normal-data to that specific object
 **/
void yil_geometry ( float * in, float* buff, int minNum, int num, float* normalbuff, float* normalIn );

/**
 * \brief create a sphere and copy the coordintes to buff
 * \param[out] buff the output buffer
 * \param[in] num the space in buff in floats
 * \param[in] angles the number of slaces in phi and psi directions
 * \param[out] normals buffer for the normals of the object
 * \note contains cos() and sin()!
 **/
void yil_sphere ( float* buff, int num, int angles, float* normals );
/**
 * \brief create a disk and copy the coordintes to buff
 * \param[out] buff the output buffer
 * \param[in] num the space in buff in floats
 * \param[in] angles the number of slaces in phi direction
 * \param[out] normals buffer for the normals of the object
 * \note contains cos() and sin()!
 **/
void yil_disk ( float* buff, int num, int angles, float* normals );
/**
 * \brief create a cylinder and copy the coordintes to buff
 * \param[out] buff the output buffer
 * \param[in] num the space in buff in floats
 * \param[in] angles the number of slaces in phi direction
 * \param[in] height the number of stacks in y-direction
 * \param[out] normals buffer for the normals of the object
 * \note contains cos() and sin()!
 **/
void yil_cylinder ( float* buff, int num, int angles, int height, float* normals );
/**
 * \brief create a cone and copy the coordintes to buff
 * \param[out] buff the output buffer
 * \param[in] num the space in buff in floats
 * \param[in] angles the number of slaces in phi direction
 * \param[in] height the number of stacks in y-direction
 * \param[out] normals buffer for the normals of the object
 * \note contains cos() and sin()!
 **/
void yil_cone ( float* buff, int num, int angles, int height, float* normals );
#endif
