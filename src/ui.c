#include "ui.h"

struct Image* fb = NULL;
struct WindowDescr pios_window_descr[ PIOS_MAX_WIN ];

int pios_window_init ( struct Image* framebuffer )
{
    fb = framebuffer;
    for ( int i = 0; i < PIOS_MAX_WIN; i++)
    {
        pios_window_descr[i].hwnd = -1;
    }
    return 0;
}

void debugWinDescr ( struct WindowDescr win )
{
    #ifdef DEBUG
    printf ("\
struct WindowDescr\n\
{\n\
    int hwnd = %d;\n\
    int32_t x = %d, y=%d;\n\
    uint32_t width = %u, height = %u;\n\
    struct CtrlDescr controls[ PIOS_MAX_WIN_CTRL ] = { ... };\n\
};\n\
    \n", win.hwnd, win.x, win.y, win.width, win.height );
    #endif
}

int pios_window_border ( int hwnd, int16_t top, int16_t right, int16_t bottom, int16_t left )
{
    if (hwnd < 0 || hwnd >= PIOS_MAX_WIN )
        return -1;
    
    struct WindowDescr* ptr = &pios_window_descr[hwnd];
    
    ptr->border.top = (top>=0 ? top : ptr->border.top);
    ptr->border.left = (left>=0 ? left : ptr->border.left);
    ptr->border.right = (right>=0 ? right : ptr->border.right);
    ptr->border.bottom = (bottom>=0 ? bottom : ptr->border.bottom);
    
    return 0;
}

int pios_window_create( int32_t x, int32_t y, uint32_t width, uint32_t height )
{
    for ( int i = 0; i < PIOS_MAX_WIN; i++)
    {
        if ( pios_window_descr[i].hwnd == -1 )
        {
            struct WindowDescr* ptr = &pios_window_descr[i];
            ptr->hwnd = i;
            ptr->x = x;
            ptr->y = y;
            ptr->width = width;
            ptr->height = height;
            
            ptr->state = VISIBLE;
            
            ptr->border.top = PIOS_WIN_BORDER_TOP;
            ptr->border.left = PIOS_WIN_BORDER_LEFT;
            ptr->border.right = PIOS_WIN_BORDER_RIGHT;
            ptr->border.bottom = PIOS_WIN_BORDER_BOTTOM;
            
            pios_windowTitle ( i, PIOS_WINDOW_DEFAULT_TITLE);
            
            for (int j=0; j < PIOS_MAX_WIN_CTRL; j++)
            {
                ptr->controls[j].hctrl = -1;
            }
            return i;
        }
    }
    
    return -1;
}

int pios_window_empty ( int hwnd )
{
    if (hwnd < 0 || hwnd >= PIOS_MAX_WIN )
        return -1;
        
    if ( pios_window_descr[hwnd].hwnd != -1 )
    {
        struct WindowDescr* ptr = &pios_window_descr[hwnd];
        for (int j=0; j < PIOS_MAX_WIN_CTRL; j++)
        {
            ptr->controls[j].hctrl = -1;
        }
    }
    return 0;
}

int pios_window ( int32_t x, int32_t y, uint32_t width, uint32_t height )
{   
    int hwnd = pios_window_create ( x, y, width, height );
    if ( hwnd == -1 )
        return -1;

    //pios_window_invalidate( hwnd );
    
    return hwnd;
}

int pios_window_update ()
{
    //for ( int i = PIOS_MAX_WIN-1; i >= 0; i--)
    for ( int i = 0; i < PIOS_MAX_WIN; i++)
    {
        if ( pios_window_descr[i].hwnd >= 0 && ((pios_window_descr[i].state & VISIBLE) != 0) )
        {
            int hwnd = pios_window_descr[i].hwnd;
            pios_window_invalidate ( hwnd );
        }
    }
    return 0;
}

int (*fn_drawCtrl[PIOS_CTRL_ENDTYPELIST]) (struct CtrlDescr*, struct WindowDescr*) = {
    pios_window_control_drawLabel,  pios_window_control_drawCheckBox,  pios_window_control_drawRadio,
    pios_window_control_drawTextbox, pios_window_control_drawBtn, pios_window_control_drawImage };

int pios_window_control_drawLabel (struct CtrlDescr* ctrl, struct WindowDescr* win)
{
    struct rgb black = { .red=0x44, .green=0x44, .blue=0x44 };
    char str[2] = { 0 };
    const char* text = (const char*) ctrl->state;    
    int32_t x = win->x + ctrl->x + win->border.left;
    int32_t y = win->y + ctrl->y + win->border.top;
    while ( *text != '\0' )
    {
        str[0] = *text;
        printText ( fb, x, y, &black, (const char*) str, false );
        x+=8; 
        text++;
        if ( win->x + win->width < x )
            return 0;
    }

    return 0;
}

int pios_window_control_drawCheckBox (struct CtrlDescr* ctrl, struct WindowDescr* win)
{
    struct rgb black = { .red=0x44, .green=0x44, .blue=0x44 };
    struct rgb grey = { .red=0xa7, .green=0xa7, .blue=0xa7 };
    int32_t x = win->x + ctrl->x + win->border.left; // offset of 2 pixel + BORDERS ( 20 px )
    int32_t y = win->y + ctrl->y + win->border.top; // offset of 2 pixel + BORDERS ( 1 px )
    bool val = *(bool*) ctrl->state;
    printIcon ( fb, x, y, (((ctrl->focus & ENABLED) == ENABLED) ? &black : &grey), (val == true ? pios_checkBtn_true : pios_checkBtn_false), 16, 16 );
    return 0;
}

int pios_window_control_drawImage (struct CtrlDescr* ctrl, struct WindowDescr* win)
{
    struct Image* img = (struct Image*) ctrl->state;
    return yil_sprite ( fb, img, ctrl->x+win->x+win->border.left, ctrl->y+win->y+win->border.top, 
                        (ctrl->x+ctrl->width+win->border.left + win->border.right > win->width ? win->width-(win->border.left + win->border.right) : ctrl->width ), 
                        (ctrl->y+ctrl->height+win->border.top + win->border.bottom > win->height ? win->height-(win->border.top + win->border.bottom) : ctrl->height) );
}

int pios_window_control_drawRadio (struct CtrlDescr* ctrl, struct WindowDescr* win)
{
    struct rgb black = { .red=0x44, .green=0x44, .blue=0x44 };
    struct rgb grey = { .red=0xa7, .green=0xa7, .blue=0xa7 };
    int32_t x = win->x + ctrl->x + win->border.left; // offset of 2 pixel + BORDERS ( 20 px )
    int32_t y = win->y + ctrl->y + win->border.top; // offset of 2 pixel + BORDERS ( 1 px )
    bool val = *(bool*) ctrl->state;
    printIcon ( fb, x, y, (((ctrl->focus & ENABLED) == ENABLED) ? &black : &grey), (val == true ? pios_radioBtn_true : pios_radioBtn_false), 16, 16 );
    return 0;
}

int pios_window_control_drawTextbox (struct CtrlDescr* ctrl, struct WindowDescr* win)
{
    struct rgb black = { .red=0x44, .green=0x44, .blue=0x44 };
    struct rgb grey = { .red=0xa7, .green=0xa7, .blue=0xa7 };
    int32_t x = win->x + ctrl->x + win->border.left; // offset of 2 pixel + BORDERS ( 20 px )
    int32_t y = win->y + ctrl->y + win->border.top; // offset of 2 pixel + BORDERS ( 1 px )
    
    // y bars
    struct rgb* col = (((ctrl->focus & ENABLED) == ENABLED) ? &black : &grey);
    for ( int32_t i = y; i<win->y + ctrl->y + ctrl->height + win->border.top+1; i++)
    {
        if ( i < win->y )
            continue;
        if ( i > win->y + win->height )
            return -1;
        if ( x >= win->x && x < win->width + win->x )
            set ( fb, x, i, col );
        if ( x+ctrl->width >= win->x && x+ctrl->width < win->width + win->x )
            set ( fb, x+ctrl->width, i, col );
    }
    
    for ( int32_t i = x; i<win->x + ctrl->x + ctrl->width +win->border.left; i++)
    {
        if ( i < win->x )
            continue;
        if ( i > win->x + win->width )
            return -1;
        if ( y >= win->y && y < win->height + win->y )
            set ( fb, i, y, col );
        if ( y+ctrl->height >= win->y && y+ctrl->height < win->height + win->y )
            set ( fb, i, y + ctrl->height, col );
    }
    
    char str[2] = { 0 };
    const char* text = (const char*) ctrl->state;    
    x = win->x + ctrl->x + 4 + win->border.left; // offset of 2 pixel + BORDERS ( 20 px )
    y = win->y + ctrl->y + 3 + win->border.top; // offset of 2 pixel + BORDERS ( 1 px )
    while ( *text != '\0' )
    {
        str[0] = *text;
        printText ( fb, x, y, col, (const char*) str, false );
        text++;  
        x+= 8; 
        if ( win->x + win->width < x || x > ctrl->width + ctrl->x + win->x )
            return 0;
    }
    return 0;
}

int pios_window_control_drawBtn (struct CtrlDescr* ctrl, struct WindowDescr* win)
{
    struct rgb black = { .red=0x82, .green=0xb9, .blue=0x65 };
    struct rgb white = { .red=0xdd, .green=0xdd, .blue=0xdd };
    struct rgb grey = { .red=0xa7, .green=0xa7, .blue=0xa7 };
    char str[2] = { 0 };
    const char* text = (const char*) ctrl->state;    
    int offset = (strlen ( text ) +1) << 3;
    
    int32_t x = win->x + ctrl->x + win->border.left; // offset of 2 pixel
    int32_t y = win->y + ctrl->y + win->border.top; // offset of 2 pixel
    
    struct rgb* col = (((ctrl->focus & ENABLED) == ENABLED) ? &black : &grey);
    if ( y < -20 || y > win->height || x < -20 || x > win->width )
        return -1; 
        
    for (int i=y; i<=y+20; i++)
    {
        for ( int j = x; j < x+offset; j++ )
        {
            set ( fb, j, i, col );
        }
    }
    
    x = win->x + ctrl->x + 4 + win->border.left; // offset of 2 pixel + BORDERS ( 20 px )
    y = win->y + ctrl->y + 3 + win->border.top; // offset of 2 pixel + BORDERS ( 1 px )
    while ( *text != '\0' )
    {

        str[0] = *text;
        printText ( fb, x, y, &white, (const char*) str, false );
        x += 8;
        text++;  
        if ( win->x + win->width < x )
            return 0; 
    }

    return 0;
}

int pios_window_control_showFocus (struct CtrlDescr* ctrl, struct WindowDescr* win)
{
    struct rgb black = { .red=0x82, .green=0xb9, .blue=0x65 };

    int32_t x = win->x + ctrl->x + win->border.left; // offset of 2 pixel
    int32_t y = win->y + ctrl->y + win->border.top; // offset of 2 pixel
    
    if ( x-20 < win->x || x+20 >= win->x + win->width || y < win->y || y+16 >= win->y + win->height )
        return -1;
    
    printIcon ( fb, x-20, y, &black, pios_selected, 16, 16 );
    return 0;
}

int pios_window_control_draw ( int hwnd )
{
    if (hwnd < 0 || hwnd >= PIOS_MAX_WIN)
        return -1;
    
    struct CtrlDescr* ptr = pios_window_descr[hwnd].controls;
    for (int i=0; i<PIOS_MAX_WIN_CTRL; i++)
    {
        if (ptr[i].hctrl >= 0 && ptr[i].hctrl < PIOS_MAX_WIN_CTRL && ptr[i].type >= 0 
         && ptr[i].type < PIOS_CTRL_ENDTYPELIST && fn_drawCtrl[ptr[i].type] != NULL)
        {
            fn_drawCtrl[ptr[i].type] ( &ptr[i], &pios_window_descr[hwnd] );
            if ( (ptr[i].focus & FOCUS) == FOCUS )
            {
                pios_window_control_showFocus ( &ptr[i], &pios_window_descr[hwnd] );
            }
        }
    }
    putchar('\n');
    
    return 0;
}

int pios_window_invalidate ( int hwnd )
{
    if (hwnd < 0 || hwnd >= PIOS_MAX_WIN)
        return -1;
        
    struct WindowDescr* win = &pios_window_descr[hwnd];
    if ( !(win->hwnd >= 0 && ((win->state & VISIBLE) == VISIBLE)) )
        return -1;
    
    int32_t y = win->y;
    int32_t x = win->x;
    uint32_t width = win->width;
    uint32_t height = win->height;
    
    /// fill the window
    for (int32_t i=y; i<y+height; i++)
    {
        uint8_t* ptr = fb->payload;
        ptr+=(((i*fb->width)+x)*fb->colorsize);
        for (int32_t j=x; j<x+width; j++)
        {
            uint32_t m = i, n = j;
            if (m>=0 && m<fb->height && n>=0 && n<fb->width)
            {
                for (uint8_t k=0; k<fb->colorsize; k++)
                {
                    ptr[k] = PIOS_WINDOW_FILLCOLOUR;
                }
            }
            ptr+=fb->colorsize;
        }
    }
    
    
    // border top
    for (int32_t i=y; i<y+win->border.top; i++)
    {
        uint8_t* ptr = fb->payload;
        ptr+=(((i*fb->width)+x)*fb->colorsize);
        for (int32_t j=x; j<x+width; j++)
        {
            uint32_t m = i, n = j;
            if (m>=0 && m<fb->height && n>=0 && n<fb->width)
            {
                for (uint8_t k=0; k<fb->colorsize; k++)
                {
                    ptr[k] = PIOS_WINDOW_BORDERCOLOUR;
                }
            }
            ptr+=fb->colorsize;
        }
    }
    
    /// border sides
    uint8_t* ptr = fb->payload, *ptr2 = fb->payload;
    ptr +=((((y+win->border.top)*fb->width)+x)*fb->colorsize);
    ptr2+=((((y+win->border.top)*fb->width)+x+width-1)*fb->colorsize);
    for (int32_t i=y+win->border.top; i<y+height-1; i++)
    {
        uint32_t m=i, n=x;
        if (m>0 && m<fb->height && n>0 && n<fb->width)
        {
            for (uint8_t s=0; s<win->border.left; s++)
            {
                for (uint8_t k=0; k<fb->colorsize; k++)
                {
                    ptr[k] = PIOS_WINDOW_BORDERCOLOUR;
                }

                ptr+= fb->colorsize;
            }
        }
        if (m>0 && m<fb->height && (n+width)>0 && (n+width)<fb->width)
        {
            for (uint8_t s=win->border.right; s>0; s--)
            {
                for (uint8_t k=0; k<fb->colorsize; k++)
                {
                    ptr2[k] = PIOS_WINDOW_BORDERCOLOUR;
                }
                ptr2 -= fb->colorsize;
            }
        }

        ptr += (fb->width - win->border.left) * fb->colorsize;                
        ptr2+= (fb->width + win->border.right)* fb->colorsize;        
    }
    
    // border bottom
    for (uint8_t i=0; i<win->border.bottom; i--)
    {
        ptr = fb->payload;
        ptr+=((((y+height-i-1)*fb->width)+x)*fb->colorsize);
        for (int32_t j=x; j<x+width; j++)
        {
            uint32_t m = y+height, n = j;
            if (m>=0 && m<fb->height && n>=0 && n<fb->width)
            {
                for (uint8_t k=0; k<fb->colorsize; k++)
                {
                    *(ptr+k) = PIOS_WINDOW_BORDERCOLOUR;
                }
            }
            ptr+=fb->colorsize;
        }
    }
    
    if ( win->border.top >= 20 )
    {
        pios_windowDrawTitle ( hwnd );
        pios_windowButtons ( hwnd );
    }
   
    return pios_window_control_draw ( hwnd );
}

int pios_windowDrawTitle ( int hwnd )
{
    if ( hwnd >= 0 && hwnd < PIOS_MAX_WIN )
    {
        struct rgb white = {
            .red = 0xdd, .green=0xdd, .blue=0xdd
        };
        struct WindowDescr* ptr = &pios_window_descr[hwnd];
        printText ( fb, ptr->x+4, ptr->y+3, &white, ptr->title, false );
    }
    
    return 0;
}

int pios_windowTitle ( int hwnd, const char* title )
{
    if ( hwnd >= 0 && hwnd < PIOS_MAX_WIN )
    {
        int i=0;
        for ( ; title[i] && i<29; i++)
        {
            pios_window_descr[hwnd].title[i] = title[i];
        }
        for ( ; i<29; i++)
        {
            pios_window_descr[hwnd].title[i] = '\0';
        }
        
        pios_window_descr[hwnd].title[29] = '\0';
        pios_windowDrawTitle ( hwnd );
    }
    return 0;
}

int pios_windowButtons ( int hwnd )
{
    if ( hwnd < 0 || hwnd >= PIOS_MAX_WIN )
        return -1;
    
    struct WindowDescr* ptr = &pios_window_descr[hwnd];
    int32_t x = ptr->x;
    int32_t y = ptr->y;
    uint32_t width = ptr->width;
    //uint32_t height = ptr->height;

    struct rgb white = {
        .red = 0xdd, .green=0xdd, .blue=0xdd
    };
    struct rgb grey = {
        .red = 0x66, .green=0x66, .blue=0x66
    };
    if (true) // enabled 
    {
        printIcon ( fb, (uint32_t) x+width-18,(uint32_t) y+2, &white, pios_closeBtn, 16, 16 );
    }
    else
    {        
        printIcon ( fb, (uint32_t)x+width-18, (uint32_t)y+2, &grey, pios_closeBtn, 16, 16 );
    }
    
    if (false) // disabled 
    {
        printIcon ( fb, (uint32_t)x+width-36, (uint32_t)y+2, &white, pios_maxBtn, 16, 16 );
    }
    else
    {        
        printIcon ( fb, (uint32_t)x+width-36, (uint32_t)y+2, &grey, pios_maxBtn, 16, 16 );
    }
    
    if (false) // disabled 
    {
        printIcon ( fb, (uint32_t)x+width-54, (uint32_t)y+2, &white, pios_minBtn, 16, 16 );
    }
    else
    {        
        printIcon ( fb, (uint32_t)x+width-54, (uint32_t)y+2, &grey, pios_minBtn, 16, 16 );
    }
    return 0;
}

int pios_windowResize ( int hwnd, int w, int h )
{
    if ( hwnd >= 0 && hwnd < PIOS_MAX_WIN )
    {
        pios_window_descr[hwnd].width = w;
        pios_window_descr[hwnd].height= h;
        
        return pios_window_update();
    }
    return -1;
}

int pios_windowMove ( int hwnd, int x, int y )
{
    if ( hwnd >= 0 && hwnd < PIOS_MAX_WIN )
    {
        pios_window_descr[hwnd].x = x;
        pios_window_descr[hwnd].y = y;
        
        return pios_window_update();
    }
    return -1;
}


struct EventDescr pios_wait ( int event );
int pios_window_createControl ( int hwnd, int32_t x, int32_t y, enum CtrlType type, void* state )
{
    if (hwnd >= PIOS_MAX_WIN || hwnd <= -1)
    {
        return -1;
    }
    
    for (int i=0; i<PIOS_MAX_WIN_CTRL; i++)
    {   
        if ( pios_window_descr[hwnd].controls[i].hctrl == -1 )
        {
            struct CtrlDescr* ptr = &pios_window_descr[hwnd].controls[i];
            ptr->hctrl = i;
            ptr->type = type;
            ptr->state = (void*) state;
            ptr->focus = ENABLED;
            ptr->x = x;
            ptr->y = y;
            ptr->width = 100;
            ptr->height = 20;
            ptr->gid = -1;
            return i;
        }
    }
    
    return -1;
}

int pios_window_control_label ( int hwnd, int32_t x, int32_t y, const char* str )
{
    return pios_window_createControl ( hwnd, x, y, LABEL, (void*) str );
}

int pios_window_control_checkbox ( int hwnd, int32_t x, int32_t y, const char* str, int* value )
{
    pios_window_createControl ( hwnd, x+20, y+1, LABEL, (void*) str );
    return pios_window_createControl ( hwnd, x, y, CHECKBOX, value );
}
int pios_window_control_radiobox ( int hwnd, int gid, int32_t x, int32_t y, const char* str, int* value )
{
    pios_window_createControl ( hwnd, x+20, y+1, LABEL, (void*) str );
    int rbox = pios_window_createControl ( hwnd, x, y, RADIOBOX, value );
    pios_window_control_setGid ( hwnd, rbox, gid );
    return rbox;
}

int pios_window_control_image ( int hwnd, int32_t x, int32_t y, uint32_t width, uint32_t height, struct Image* buffer )
{
    int img = pios_window_createControl ( hwnd, x, y, IMAGE, (void*) buffer );
    if (img > -1 && img < PIOS_MAX_WIN_CTRL)
    {
        pios_window_descr[hwnd].controls[img].width = width;
        pios_window_descr[hwnd].controls[img].height = height;
    }
    return img;
}

int pios_window_control_textbox ( int hwnd, int32_t x, int32_t y, uint32_t width, uint32_t height, char* value )
{
    int tbox = pios_window_createControl ( hwnd, x, y, TEXTBOX, value );
    if (tbox > -1 && tbox < PIOS_MAX_WIN_CTRL)
    {
        pios_window_descr[hwnd].controls[tbox].width = width;
        pios_window_descr[hwnd].controls[tbox].height = height;
    }
    return tbox;
}

int pios_window_control_button ( int hwnd, int32_t x, int32_t y, char* value )
{
    return pios_window_createControl ( hwnd, x, y, BUTTON, value );
}

int pios_window_nameControl ( int hwnd, int hctrl, const char* name )
{
    if (hwnd >= PIOS_MAX_WIN || hwnd <= -1 || hctrl >= PIOS_MAX_WIN_CTRL || hctrl <= -1)
    {
        return -1;
    }
    
    char* target = pios_window_descr[hwnd].controls[hctrl].name;

    for (int i=0; i<19; i++)
    {
        target[i] = name[i];
        if ( name[i] == '\0' )
            break;
    }
    target[20] = '\0';
    
    return 0;
}

int pios_window_control_activate ( int hwnd, int controlId )
{
    // todo do a callback for the controls
    return -1;
}
    
int pios_window_control_focus ( int hwnd, int controlId )
{
    if (hwnd >= PIOS_MAX_WIN || hwnd <= -1 || controlId <= -1 || controlId >= PIOS_MAX_WIN_CTRL )
    {
        return -1;
    }
    
    for (int i=0; i<PIOS_MAX_WIN_CTRL; i++)
    {   
        if ( (pios_window_descr[hwnd].controls[i].focus & FOCUS) == FOCUS )
        {
            pios_window_descr[hwnd].controls[i].focus &= ~FOCUS;
        }
    }
    
    pios_window_descr[hwnd].controls[controlId].focus |= FOCUS;
    
    return 0;
}

int pios_window_show ( int hwnd )
{
    if (hwnd >= PIOS_MAX_WIN || hwnd <= -1 )
    {
        return -1;
    }
    
    pios_window_descr[hwnd].state |= VISIBLE;
    
    return 0;
}

int pios_window_hide ( int hwnd )
{
    if (hwnd >= PIOS_MAX_WIN || hwnd <= -1 )
    {
        return -1;
    }
    
    pios_window_descr[hwnd].state &= ~VISIBLE;
    
    return 0;
}


int pios_window_control_enable ( int hwnd, int controlId )
{
    if (hwnd >= PIOS_MAX_WIN || hwnd <= -1 || controlId <= -1 || controlId >= PIOS_MAX_WIN_CTRL )
    {
        return -1;
    }
    
    pios_window_descr[hwnd].controls[controlId].focus |= ENABLED;
    return 0;
}
int pios_window_control_disable ( int hwnd, int controlId )
{
    if (hwnd >= PIOS_MAX_WIN || hwnd <= -1 || controlId <= -1 || controlId >= PIOS_MAX_WIN_CTRL )
    {
        return -1;
    }
    
    pios_window_descr[hwnd].controls[controlId].focus &= ~ENABLED;
    return 0;
}

int pios_window_control_setGid ( int hwnd, int controlId, int gid )
{
    if (hwnd >= PIOS_MAX_WIN || hwnd <= -1 || controlId <= -1 || controlId >= PIOS_MAX_WIN_CTRL )
    {
        return -1;
    }
    
    pios_window_descr[hwnd].controls[controlId].gid = gid;
    return gid;
}
