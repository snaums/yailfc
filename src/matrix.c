#include "matrix.h"
#include "stdbool.h"

#include "pios_port_config.h"

void yil_matMul ( float* a, float* b, int num )
{
    float result[num*num];
    for (int i=0; i<num; i++)
    {
        for (int j=0; j<num; j++)
        {
            result[i*num + j] = 0;
            for (int k=0; k<num; k++)
            {
                result[i*num + j] += a[i*num + k] * b[k*num + j];
            }
        }
    }
    
    float* out = a;
    float* ptr=result;
    for (int i=0; i<num*num; i++)
    {
        *out = *ptr;
        out++;
        ptr++;
    }
}

void yil_NDC2Window ( float* vec, uint32_t* bounds, int num )
{
    uint32_t xmin = bounds[0], ymin = bounds[1];
    uint32_t xfactor = (bounds[2]+xmin)/2;
    uint32_t yfactor = (bounds[3]+ymin)/2;
    int32_t zmax = YIL_Z_MAX;
    for (int i=0; i<num*4; i+=4)
    {
        vec[i]=((vec[i]+1) * xfactor) + xmin;
        vec[i+1]=((vec[i+1]+1) * yfactor) + ymin;
        vec[i+2]=(((vec[i+2]+1)/2) * zmax);
    }
}

void yil_matAdd ( float* result, float* a, int num )
{
    float* ptr=result;
    for (int i=0; i<num*num; i++)
    {
        *ptr += *a;
        a++;
        ptr++;
    }
}

void yil_vecScale ( float* result, float a, int num )
{
    for (int i=0; i<num; i++)
    {
        result[i] *= a;
    }
}

void yil_vecfDebug ( float* a, int num )
{
#ifndef PIOS_PLATFORM_RPI
    for (int i=0; i<num; i++)
    {
        printf (": %f  %f  %f  %f \n", a[i*4], a[i*4+1], a[i*4+2], a[i*4+3]);
        if ((i%4)==3)
            putchar('\n');
    }
#endif
}
void yil_vecI32Debug ( int32_t* a, int num )
{
#ifndef PIOS_PLATFORM_RPI
    for (int i=0; i<num; i++)
    {
        printf (": %d  %d  %d \n", a[i*3], a[i*3+1], a[i*3+2]);
        if ((i%4)==3)
            putchar('\n');
    }
#endif
}

void yil_matVecMul ( float* a, float* b, int num )
{
    float result[num];
    float* ptr = result;
    for (int i=0; i<num; i++)
    {
        *ptr = 0;
        for (int j=0; j<num; j++)
        {
            *ptr += a[i*num + j] * b[j];
        }
        ptr++;
    }
    
    ptr = result;
    for (int i=0; i<num; i++)
    {
        *b = *ptr;
        b++;
        ptr++;
    }
}

void yil_matScale ( float* a, float factor, int num )
{
    float* ptr = a;
    for (int i=0; i<num*num; i++)
    {
        *ptr = *ptr * factor;
        ptr++;
    }
}

void yil_translate ( float* pts, float x, float y, float z, int num )
{
    float* ptr=pts;
    for ( int i=0; i<num; i++ )
    {
        *ptr += x;
        ptr++;
        *ptr += y;
        ptr++;
        *ptr += z;
        ptr++; ptr++;
    }
}

void yil_rotate ( float* pts, enum YIL_AXIS axis, float alpha, int num )
{
    float c = cos ( (alpha * M_PI) / 180.f );
    float s = sin ( (alpha * M_PI) / 180.f );
    
    float tmp;
    for ( int i=0; i<num; i++ )
    {
        switch (axis)
        {
            case YIL_X:
                tmp =   c * pts[(i*4)+1] - s * pts[(i*4)+2];
                pts[(i*4)+2] =   s * pts[(i*4)+1] + c * pts[(i*4)+2];
                pts[(i*4)+1] = tmp;
                break;
            case YIL_Y:
                tmp =   c * pts[(i*4)+0] + s * pts[(i*4)+2];
                pts[(i*4)+2] = - s * pts[(i*4)+0] + c*pts[(i*4)+2];
                pts[(i*4)+0] = tmp;
                break;
            case YIL_Z:
                tmp =   c * pts[(i*4)+0] - s * pts[(i*4)+1]; 
                pts[(i*4)+1] =   s * pts[(i*4)+0] + c * pts[(i*4)+1]; 
                pts[(i*4)+0] = tmp; 
                break;
        }
    }
}


void yil_scale ( float* pts, float x, float y, float z, int num )
{
    float* ptr=pts;
    for ( int i=0; i<num; i++ )
    {
        *ptr *= x;
        ptr++;
        *ptr *= y;
        ptr++;
        *ptr *= z;
        ptr++; ptr++;
    }
}

void yil_frustum ( float* result, float l, float r, float t, float b, float n, float f )
{
    for (int i=0; i<16; i++)
    {
        result[i] = 0.0f;
    }
    result[0] = (2*n) / (r-l);
    result[2] = (r+l) / (r-l);
    result[5] = (2*n) / (t-b);
    result[6] = (t+b) / (t-b);
    result[10] = - ((f+n) / (f-n));
    result[11] = - ((2*f*n) / (f-n));
    result[14] = -1;
}

void yil_orthoAdj ( float* result, float l, float aspect, float n, float f )
{
    float tmp = l/aspect;
    float abs = 1;//((f-n)/2);
    for (int i=0; i<16; i++)
    {
        result[i] = 0.0f;
    }
    result[0] = (-l * abs);
    result[5] = (-tmp * abs);
    result[10] = -1/(f-n);
    result[15] = 1;
}

void yil_ortho ( float* result, float l, float r, float t, float b, float n, float f )
{
    float abs = 1;//((f-n)/2);
    for (int i=0; i<16; i++)
    {
        result[i] = 0.0f;
    }
    result[0] =  (r-l) * abs;
    result[5] = (t-b) *abs;
    result[10] = -1/(f-n);
    result[15] = 1;
}

void yil_frustumAdj ( float* result, float l, float aspect, float n, float f )
{
    float tmp = l/aspect;
    for (int i=0; i<16; i++)
    {
        result[i] = 0.0f;
    }
    result[0] = (n / -l);
    result[5] = (n / -tmp);
    result[10] = - ((f+n) / (f-n));
    result[11] = - ((2*f*n) / (f-n));
    result[14] = -1;
}

void yil_frustumIdentity ( float* result )
{
    result[0] = 1;
    result[1] = 0;
    result[2] = 0;
    result[3] = 0;
    result[4] = 0;
    result[5] = 1;
    result[6] = 0;
    result[7] = 0;
    result[8] = 0;
    result[9] = 0;
    result[10] = 1;
    result[11] = 0;
    result[12] = 0;
    result[13] = 0;
    result[14] = -1;
    result[15] = 0;
}

void yil_copyPoints ( int32_t* out, float* in, int num )
{
    for ( int i=0; i<(num<<2); i+=4)
    {
        out[i]   = (in[i]   > 0x7fffffff ? 0x7fffffff : in[i]);
        out[i+1] = (in[i+1] > 0x7fffffff ? 0x7fffffff : in[i+1]);
        out[i+2] =  in[i+2];
        out[i+3] =  (in[i+3] == 0) ? 0 : (1/in[i+3]) * YIL_Z_MAX;
    }
}
