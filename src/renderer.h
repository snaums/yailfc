/** 
 * \file renderer.h
 * \author Stefan Naumann
 * \brief interfaces for rendering, yil-context
 * 
 * renderer.h contains interfaces for rendering primitives like square, triangles and lines.
 * It's code will draw the shapes defined by some points, colours and textures to the screen.
 **/

#ifndef NAUMS_YAILFC_RENDERER
#define NAUMS_YAILFC_RENDERER

#include "matrix.h"
#include "image.h"
#include "texture.h"
#include "light.h"

/// maximal number of textures allowed
#define YIL_MAX_TEX 3

/// describes the fill-mode 
enum _rmode
{
    YIL_ALPHA_BLEND,    ///< blend polygons onto earlier ones with ALPHA-values
    YIL_FILL,           ///< fill polygons
    YIL_WIRE,           ///< draw only lines
    YIL_NONE            ///< do not draw anything
};

/// describes the viewport
struct _rviewport
{
    int32_t xmin,      ///< top left x-value of the viewport
            ymin;      ///< top left y-value of the viewport
    int32_t xsize,     ///< bottom right x-value of the viewport
            ysize;     ///< bottom right y-value of the viewport
};

/// describes interpolation modes
enum _interpolate 
{
    YIL_BILINEAR,       ///< bilinear interpolation
    YIL_NEAREST         ///< nearest neighbor (hard edges)
};

/**
 * \brief describes the culling-mode (backside, frontside, both)
 **/
enum _culling
{
    YIL_FFC =1,         ///< front face culling
    YIL_BFC =2,         ///< back face culling
    YIL_BOTH=3          ///< both sides please!
};

/**
 * \brief describes different texturing-modes (no textures, textures only, textures and backcolor)
 **/
enum _texture
{
    YIL_TEX_NONE,       ///< disable textures
    YIL_TEX_REPLACE,    ///< textures replace the color-values
};

enum _render
{
    YIL_RENDER_SOLID,       ///< render solidly
    YIL_RENDER_ALPHA_BLEND  ///< render with alpha-values and blend
};

enum _texture_cap
{
    YIL_TEX_CAP_NONE = 0x00,
    YIL_TEX_CAP_MIPMAP = 0x01
};

/**
 * \struct yil_ctx a number of properties of the "pipeline" of the software-renderer
 **/
struct yil_ctx
{
    enum _rmode rendermode;                 ///< fill, wireframes, none (hidden)
    struct _rviewport viewport;             ///< current viewport (will constrain drawing-procedures to these bounds)
    enum _interpolate interpolation;        ///< interpolation-mode for colours
    enum _culling culling;                  ///< only front sides, back sides or both
    bool depthtest;                         ///< is depthtest enabled?
    struct Image depthbuffer;               ///< the depthbuffer (used as RGBA (32Bit) buffer)
    enum _texture texmode;                  ///< mode for rendering textures onto the object
    enum _interpolate texture_interpol;     ///< interpolation mode for textures
    //int (*texture_op) ( void*, void*, float* );     ///< !deprecated!
    //int texture_num;
    //int texture_cap[YIL_MAX_TEX];
    //float texture_coord[YIL_MAX_TEX][2];
    struct Image* texture;                  ///< the currently enabled texture
    
    bool ortho;                             ///< is the projection matrix an orthogonal matrix?
    
    float fog_zmin,                         ///< the minimal value for visible fog
          fog_zmax;                         ///< the z-value for the maximal fog opaqueness
    void* fog_color;                        ///< the color of the fog
    bool  fog_enable;                       ///< is fog enabled?
    
    bool lighting_enable;                   ///< is the light enabled?
    bool lighting_phong;                    ///< Phong shading (yes) or Gourard-Shading (no)
    struct yil_lightSource lighting_src[ YIL_MAX_LIGHT ];   ///< light sources in the scene
    int lighting_num;                       ///< current number of light-sources added
    bool map_constraints;                   ///< ignore the map-constraints if false
    
    struct 
    {
        bool  noise;                        ///< is noise-filter enabled?
        bool  motionBlur;                   ///< is motion blur enabled?
        float motionBlurLevel;              ///< what is the factor of motion blur [0,1]
    } extra;
    
    enum _interpolate upscale;              ///< interpolation-mode for upscaling the image
    
    float perspective_matrix[16];           ///< projection matrix (which does the projection itself)
} typedef yil_ctx;

/// the context and major settings / properties for rendering
extern struct yil_ctx yil_context;

/**
 * \brief initiates the context (placeholder)
 **/
bool init_context ( void* buffer, int32_t width, int32_t height, void* fog );

/**
 * \brief triangle-renderer as last step of the "pipeline"
 * \param[in] img the image to be used as framebuffer
 * \param[in] pts a number of points making up the triangle (format: x1, y1, x2, y2, x3, y3)
 * \param[in] colors the color-values of the points 1,2,3 in that order, use the colormodel of img
 * \param[in] texcoord coordinates for the textures at the specific points (u1, v1, u2, v2,...) \in [0,1]
 * \param[in] alpha alpha-values for blending per vertex \in [0,1]
 * \param[in] normals the normal-vectors at the vertizes (should be 1 unit long!)
 * \return 0
 **/
int yil_triangle ( Image* img, int32_t* pts, void* colors, float* texcoord, float* alpha, float* normals );

/**
 * \brief rectangle renderer as last step of the pipeline (draws two triangles)
 * \param[in] img the image to be used as framebuffer
 * \param[in] pts a number of points making up the rectangle (from top left, top right, bottom right, bottom left)
 * \param[in] colours for colour-values for each of the points (tl, tr, br, bl)
 * \param[in] texcoord coordinates for the textures at the specific points (u1, v1, u2, v2,...) \in [0,1]
 * \param[in] alpha alpha-values for blending per vertex \in [0,1]
 * \param[in] normals the normal-vectors at the vertizes (should be 1 unit long!)
 * \return 0
 **/
int yil_rectangle ( Image* img, int32_t* pts, void* colors, float* texcoord, float* alpha, float* normals );
/**
 * \brief render a line using bresenhams algorithm
 * \param[inout] img the image to be rendered on
 * \param[in] pts the points for the line (endpoints (x,y,z,w))
 * \param[in] colors array of two colors for the endpoints of the line
 * \param[in] indizes index[0] for the first point in pts, index[1] for the second
 * \return 0
 **/
int yil_line ( Image* img, int32_t* pts, void* colors, int* indizes );

/**
 * \brief test for depth and overlapping pixels (fragments)
 * \param[in] z array of three z-values for the three endpoints of a triangle
 * \param[in] coeff the determinants for the three endpoints
 * \param[inout] depth depthbuffer
 * \param[in] num number of points
 * \param[in] offset for the z-values (so I don't need to copy them)
 * \return visible true, false if overlapped
 **/
bool yil_depthtest ( int32_t* z, float* coeff, int32_t *depth, int num, int offset );

/**
 * \brief render fog onto the image using the contexts depthbuffer
 * \param[inout] img for rendering the fog onto
 **/
int yil_fog ( Image* img );
#endif
