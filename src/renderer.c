#include "image.h"
#include "renderer.h"
#include "color.h"

#include <stdio.h>
#include <math.h>

#define YIL_FOG_PRESCALE 1000

struct yil_ctx yil_context;

bool init_context ( void* buffer, int32_t width, int32_t height, void* fog )
{
    yil_context.depthbuffer = createImage ( RGBA, width, height, buffer );

    yil_context.rendermode = YIL_FILL;
    yil_context.viewport.xmin = 0; 
    yil_context.viewport.ymin = 0;   
    yil_context.viewport.xsize = width;
    yil_context.viewport.ysize = height;     
    yil_context.interpolation = YIL_NEAREST;
    yil_context.culling = YIL_BOTH;
    yil_context.depthtest = true;
    yil_context.ortho = false;
    yil_context.texmode = YIL_TEX_NONE;
    /*
    yil_context.texture_num = 0;
    for (int i=0; i<YIL_MAX_TEX; i++)
    {*/
        yil_context.texture=NULL;
    /*    yil_context.texture_cap[i] = 0;
        yil_context.texture_coord[i][0] = 0;
        yil_context.texture_coord[i][1] = 0;
    }*/
    
    yil_context.fog_enable = true;
    yil_context.fog_zmin = 2;
    yil_context.fog_zmax = 5;
    yil_context.fog_color = fog;
    yil_context.map_constraints = true;
    
    return true;
}

int yil_fog ( Image* img )
{
    if (yil_context.fog_enable)
    {
        const float zmin=yil_context.fog_zmin*YIL_FOG_PRESCALE, zmax=yil_context.fog_zmax*YIL_FOG_PRESCALE;
        const float zdiff = zmax - zmin;
        float coeff[2];
        uint8_t fogcol[ img->colorsize*2 ];
        for (int i=0; i<img->colorsize; i++)
        {
            fogcol[i] = ((uint8_t*) yil_context.fog_color)[i];
        }

        int32_t* depth = yil_context.depthbuffer.payload;
        uint8_t* pay = img->payload;

        #pragma omp parallel for private ( coeff )
        for (int32_t y=0; y<img->height; y++)
        {
            for (int32_t x=0; x<img->width; x++)
            {
                float d = depth[y*yil_context.depthbuffer.width + x];
                if (d >= zmax)
                {
                    for (int i=0; i<img->colorsize; i++)
                    {
                        // fogcol takes both the fogcolor (see above) and the color of the "background"
                        pay[((y*img->width+x)*img->colorsize)+i] = fogcol[i];
                    }
                }
                else if (d > zmin)
                {
                    float diff = d - zmin;
                    diff /= zdiff;
                    
                    coeff[0] = diff;
                    coeff[1] = 1-diff;
                    
                    for (int i=0; i<img->colorsize; i++)
                    {
                        // fogcol takes both the fogcolor (see above) and the color of the "background"
                        pay[((y*img->width+x)*img->colorsize)+i] = fogcol[i] * coeff[0] + pay[((y*img->width+x)*img->colorsize)+i] * coeff[1];
                    }
                }
            }
        }
    }
    return 0;
}

int yil_rectangle ( Image* img, int32_t* pts, void* colors, float* texcoord, float* alpha, float* normals )
{   
    #define CLIPZ(a) pts[(a*4)+2]
    if (yil_context.rendermode == YIL_WIRE)
    {
        int p=1;
        int indizes[2];
        float zmin, zmax;
        for (int i=0; i<4; i++)
        {
            indizes[0] = i;
            indizes[1] = p;
            zmin = (CLIPZ(i)<CLIPZ(p) ? CLIPZ(i) : CLIPZ(p));
            zmax = (CLIPZ(i)>CLIPZ(p) ? CLIPZ(i) : CLIPZ(p));
            
            if (zmax > 0 && zmin < YIL_Z_MAX)
            {    
                yil_line ( img, pts, (yil_context.lighting_enable ? yil_fixedcolor_white : yil_fixedcolor_black), indizes );
            }
            p++;
            if (p>=4)
                p=0;
        }
        return 1;
    }

    yil_triangle ( img, pts, colors, texcoord, alpha, normals );    
    int32_t tripts[] = { pts[0], pts[1], pts[2], pts[3],
                         pts[8], pts[9], pts[10], pts[11],
                         pts[12], pts[13], pts[14], pts[15] };
    float tritexcoord[] = { texcoord[0], texcoord[1],  
                            texcoord[4], texcoord[5],
                            texcoord[6], texcoord[7] };
    float trinorm[] = { normals[0], normals[1], normals[2],
                        normals[6], normals[7], normals[8],
                        normals[9], normals[10], normals[11] };
    float trialpha[] = { alpha[0], alpha[2], alpha[3] };
    uint8_t* col=colors;
    uint8_t* ptr;
    uint8_t tricol[ img->colorsize*3 ];
    uint8_t index[] = {0, 2, 3};
    for (int i=0; i<3; i++)
    {
        int current = index[i] * img->colorsize;
        ptr = &col[current];
        
        for (int s=0; s<img->colorsize; s++)
        {
            tricol[(i*img->colorsize)+s] = *ptr;
            ptr++;
        }
    }
    
    yil_triangle ( img, tripts, tricol, tritexcoord, trialpha, trinorm );
    return 1;
}

bool yil_depthtest ( int32_t* z, float* coeff, int32_t *depth, int num, int offset )
{
    if (!yil_context.depthtest)
    {
        return true;
    }
    
    float currentZ=0;
    for (int i=0; i<num; i++)
    {
        currentZ += z[i+(i*offset)] * coeff[i];
    }
    
    if (currentZ < 0 || currentZ > YIL_Z_MAX)
        return false;
    
    currentZ -= 9000000.f;
    if (currentZ < 0 )
        currentZ = 9000000.f;
        
    uint32_t c = currentZ+.5;
    if (c <= *depth)
    {
        *depth = c;
        return true;
    }
    return false;
}

int yil_line ( Image* img, int32_t* pts, void* colors, int* indizes )
{
    int32_t x[2] = { pts[indizes[0]*4], pts[indizes[1]*4] };
    int32_t y[2] = { pts[indizes[0]*4+1], pts[indizes[1]*4+1] };

    #define COORD_X(a) x[a]
    #define COORD_Y(a) y[a]

    int32_t deltax=0, deltay=0, stepx=0, stepy=0;
    if (COORD_X(0) > COORD_X(1)) 
    {
        deltax = COORD_X(0) - COORD_X(1);
        stepx = -1;
        if ( COORD_X(1) >= img->width || COORD_X(0) < 0 )
            return 0;
    }
    else
    {
        deltax = COORD_X(1) - COORD_X(0);
        stepx = +1;
        if ( COORD_X(0) >= img->width || COORD_X(1) < 0 )
            return 0;
    }
    
    if (COORD_Y(0) > COORD_Y(1))
    {
        deltay = (COORD_Y(1) - COORD_Y(0));
        stepy = -1;
        if ( COORD_Y(1) >= img->height || COORD_Y(0) < 0 )
            return 0;
    }
    else
    {
        deltay = (COORD_Y(0) - COORD_Y(1));
        stepy = +1;
        if ( COORD_Y(0) >= img->height || COORD_Y(1) < 0 )
            return 0;
    }
    
    uint8_t *ptr = (uint8_t*)img->payload;
    ptr += (COORD_X(0) + COORD_Y(0)*img->width) * img->colorsize;
    int32_t error = deltax + deltay;
    COORD_X(1) += stepx;
    COORD_Y(1) += stepy;
    while ( COORD_X(0) != COORD_X(1) && COORD_Y(0) != COORD_Y(1) )
    {
        if ( COORD_X(0) < img->width && COORD_X(0) >= 0 && 
             COORD_Y(0) < img->height && COORD_Y(0) >= 0 )
        {
            for ( int i=0; i<img->colorsize; i++ )
            {
                ptr[i] = ((uint8_t*)colors)[i];
            }
        }
        if (deltay <= (error<<1))
        {
            COORD_X(0) += stepx;
            ptr += img->colorsize*stepx;
            error += deltay;
        }
        if (deltax >= (error<<1))
        {
            COORD_Y(0) += stepy;
            ptr += img->width * img->colorsize * stepy;
            error += deltax;
        }
    }

    return 0;
}

int yil_triangle ( Image* img, int32_t* pts, void* colors, float* texcoord, float* alpha, float* normals )
{
    if (yil_context.rendermode == YIL_NONE)
        return 0;
    
    #define CLIPZ(a) pts[(a*4)+2]
    int32_t zmin=CLIPZ(0), zmax=CLIPZ(0);
    if (zmax < CLIPZ(1)) zmax = CLIPZ(1);
    if (zmax < CLIPZ(2)) zmax = CLIPZ(2);
    if (zmin > CLIPZ(1)) zmin = CLIPZ(1);
    if (zmin > CLIPZ(2)) zmin = CLIPZ(2);
    
    // "clipping"
    if (//(ymax < yil_context.viewport.ymin) || (ymin > yil_context.viewport.ymin + yil_context.viewport.ysize) ||
        //(xmax < yil_context.viewport.xmin) || (xmin > yil_context.viewport.xmin + yil_context.viewport.xsize) ||
        (zmax < 0 || zmin > YIL_Z_MAX))
        return 0;  
    
    // wireframe
    if (yil_context.rendermode == YIL_WIRE)
    {
        int p=1;
        int indizes[2];
        for (int i=0; i<3; i++)
        {
            indizes[0] = i;
            indizes[1] = p;
                    
            yil_line ( img, pts, (yil_context.lighting_enable ? yil_fixedcolor_white : yil_fixedcolor_black), indizes );
            p++;
            if (p>=3)
                p=0;
            
        }
        return 0;
    }  
    
    int32_t x[3] = { pts[0], pts[4], pts[8] };
    int32_t y[3] = { pts[1], pts[5], pts[9] };
    int32_t z[3] = { pts[2], pts[6], pts[10] };
    float w[3] = { ((float)pts[3])/YIL_Z_MAX, 
                   ((float)pts[7])/YIL_Z_MAX, 
                   ((float)pts[11])/YIL_Z_MAX };    
        
    int32_t xmin=x[0], xmax=x[0], ymin=y[0], ymax=y[0];
    for (int i=1; i<3; i++)
    {
        if (x[i] < xmin) xmin = x[i];
        if (y[i] < ymin) ymin = y[i];            
        if (xmax < x[i]) xmax = x[i];
        if (ymax < y[i]) ymax = y[i];
    }
        
    if (ymin < yil_context.viewport.ymin)  
        ymin = yil_context.viewport.ymin;
    if (ymax > (yil_context.viewport.ysize + yil_context.viewport.ymin)) 
        ymax = yil_context.viewport.ysize + yil_context.viewport.ymin-1;
    if (xmin < yil_context.viewport.xmin)  
        xmin = yil_context.viewport.xmin;
    if (xmax > (yil_context.viewport.xsize + yil_context.viewport.xmin)) 
        xmax = yil_context.viewport.xsize + yil_context.viewport.xmin-1;
    
    // culling
    const float flaeche = ((x[1]-x[0])*(y[2]-y[0]) - (y[1]-y[0])*(x[2]-x[0]));
    bool renderTriangle = false;
    if ((yil_context.culling & YIL_FFC) == YIL_FFC && flaeche>0)
    {
        renderTriangle=true;
    }
    if ((yil_context.culling & YIL_BFC) == YIL_BFC && flaeche<0)
    {
        renderTriangle=true;
    }
    if (!renderTriangle)
        return 0;
            
    /**
     if ( yil_ctx.lighting_enable )
     {
        // calculate lighting,
        // multiplicate with the vertizes of the edge-points
        // interpolate below
        // TODO: if edge-points lighting only, do in yil_rectangle, and disable lighting_enable for slight performance boost.
     }      
     **/     
            
    uint8_t *outcol=colors;
    uint8_t colbuff[img->colorsize];
    float det[3], coeff[3], c3[3];
    int32_t * depthbuffer = yil_context.depthbuffer.payload;
    int8_t * imagebuffer = img->payload;
    int32_t * depth=depthbuffer;
    bool lineWasIn;
    uint32_t oldDepth = 0;
    //#pragma omp parallel for private ( coeff, det, c3, colbuff, depth, lineWasIn, normals, outcol )
    for (int32_t py = ymin; py<=ymax; py++)
    {
        uint8_t * ptr = (uint8_t*) &imagebuffer[(xmin+(py*img->width))*img->colorsize];
        if (yil_context.depthtest)
            depth = &depthbuffer[xmin+(py*yil_context.depthbuffer.width)];
        lineWasIn = false;
        for (int32_t px = xmin; px<=xmax; px++)
        {
            det[0] = (x[0] - px) * (y[1] - py) - (y[0] - py)*(x[1] - px);
            det[1] = (x[1] - px) * (y[2] - py) - (y[1] - py)*(x[2] - px);
            det[2] = (x[2] - px) * (y[0] - py) - (y[2] - py)*(x[0] - px);
            
            if (det[0] < 0)
            {
                for (int i=0; i<3; i++)
                    coeff[i] = -det[i] / flaeche;
            }
            else
            {
                for (int i=0; i<3; i++)
                    coeff[i] = det[i] / flaeche;
            }

            if ((det[0] >= 0 && det[1] >= 0 && det[2] >= 0)|| 
                (det[0] <= 0 && det[1] <= 0 && det[2] <= 0))
            {
                lineWasIn = true;
                c3[0] = fabs(coeff[1]);
                c3[1] = fabs(coeff[2]);
                c3[2] = fabs(coeff[0]);
                if (yil_context.depthtest) 
                    oldDepth = *depth;
                if (yil_depthtest ( z, c3, depth, 3, 0 ))
                {
                    float wf = fabs(1.f / (c3[0] * w[0] + c3[1] * w[1] + c3[2] * w[2]));
                    c3[0] *= wf * fabs(w[0]);
                    c3[1] *= wf * fabs(w[1]);
                    c3[2] *= wf * fabs(w[2]);
                                    
                    if (yil_context.texture != NULL && yil_context.texmode == YIL_TEX_REPLACE)
                    {
                        float tx = c3[0] * texcoord[0] + c3[1] * texcoord[2] + c3[2] * texcoord[4];
                        float ty = c3[0] * texcoord[1] + c3[1] * texcoord[3] + c3[2] * texcoord[5];

                        float myalpha=1;
                        if (yil_context.rendermode == YIL_ALPHA_BLEND)
                        {
                            myalpha = alpha[0] * c3[0] + alpha[1] * c3[1] + alpha[2] * c3[2];
                        }
                        
                        void *icol;
                        if ( yil_context.lighting_enable )
                        {
                            uint8_t lightcol[4];
                            icol = &lightcol;
                            for ( int i = 0; i<img->colorsize; i++ )
                            {
                                lightcol[i] = 0;
                                for ( int j = 0; j<3; j++ )
                                {
                                    lightcol[i] += *(((uint8_t*)colors) + j*img->colorsize + i) *c3[j]; 
                                }
                            }
                        }
                        
                        if (yil_texturize ( img, ptr, myalpha, tx, ty, icol ) == 1 && yil_context.depthtest)
                        {
                            *depth = oldDepth;
                        }
                    }
                    else
                    {
                        if (yil_context.interpolation == YIL_BILINEAR)
                        {
                            for ( int i = 0; i<img->colorsize; i++ )
                            {
                                colbuff[i] = 0;
                                for ( int j = 0; j<3; j++ )
                                {
                                    colbuff[i] += *(((uint8_t*)colors) + j*img->colorsize + i) *c3[j]; 
                                }
                            }
                            
                            //yil_interpolateColors ( img, &colbuff, colors, c3, 3 );
                            outcol = colbuff;
                        }
                        else if (yil_context.interpolation == YIL_NEAREST)
                        {
                            if (det[0] > det[1] && det[0] > det[2])
                                outcol = (((uint8_t*) colors)+img->colorsize+img->colorsize);
                            else if (det[1] > det[2])
                                outcol = colors;
                            else
                                outcol = (((uint8_t*) colors)+img->colorsize);
                        }
                    
                        if (yil_context.rendermode == YIL_ALPHA_BLEND)
                        {
                            float myalpha = alpha[0] * c3[0] + alpha[1] * c3[1] + alpha[2] * c3[2];                            
                            for ( int i = 0 ; i<img->colorsize; i++ )
                            {
                                ptr[i] = ptr[i] * (1-myalpha) + outcol[i] * myalpha;
                            }
                        }
                        else
                        {
                            for ( int i = 0 ; i<img->colorsize; i++ )
                            {
                                ptr[i] = outcol[i];
                            }
                        }
                    }
                }
            }
            else
            {
                if ( lineWasIn )
                    break;
            }
            
            depth++;
            ptr+=img->colorsize;
        }
    }
    return 0;
}
