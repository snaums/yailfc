#include "console.h"

#define PIOS_FBCONSOLE_MAX_LINES 17

int pios_console_x = 0;
int pios_console_y = 0;

void* fgcolor=NULL, *bgcolor=NULL;
void* fillcolor=NULL;

struct Image* console_fb;

struct pios_fbconsole_command
{
    char command[PIOS_CONSOLE_MAX_COMMAND_LENGTH];
    int (*cmdfunc) (int, char**);
};

struct pios_fbconsole_command commandList[PIOS_CONSOLE_MAX_COMMANDS];
int commandListLength = 0;
static char inputBuffer[PIOS_CONSOLE_MAX_INPUT_LENGTH]={0};
static int inputBufferLength = 0;
int homex=0, homey=0;

char* pios_fbconsole_getCommand ( int i )
{
    if ( i < commandListLength )
        return commandList[i].command;
    else return NULL;
}

int pios_fbconsole_getCommandCount ()
{
    return commandListLength;
}

void pios_fbconsole_init ( struct Image* img, void* fg, void* bg, void* fl )
{
    console_fb = img; 
    fgcolor = fg;
    bgcolor = bg;
    fillcolor = fl;
    
    memset ( inputBuffer, 0, PIOS_CONSOLE_MAX_INPUT_LENGTH );
    
    if (fillcolor==NULL)
    {
        struct rgba col = {.red=0, .green=0, .blue=0, .alpha=0 };
        fill ( console_fb, (void*) &col );
    }
    else
    {
        fill ( console_fb, fillcolor );
    }
}
void pios_fbconsole_setColor ( void* fg, void* bg, void* fill )
{
    fgcolor = fg;
    bgcolor = bg;
    fillcolor = fill;
}

void pios_fbconsole_setPosition ( int x, int y )
{
    pios_console_x = x;
    pios_console_y = y;
    
    homex=x;
    homey=y;
}

void pios_fbconsole_clear( )
{
    pios_console_y = homey;
    for ( int y = homey; y < PIOS_FBCONSOLE_MAX_LINES; y++ )
    {
        pios_fbconsole_clearLine();
        pios_console_y++;
    }
    pios_console_y = homey;
}

void pios_fbconsole_switchBuffer ( struct Image* src )
{
    copy ( console_fb, src, REPLACE );
}

void pios_fbconsole_write ( const char* str )
{    
    int begin = 0;
    int characters = 0;
    uint32_t posx, posy;
    for (int i=0; str[i]; i++)
    {
        if (str[i]=='\n')
        {
            posx = pios_console_x * ms_font_info.x + PIOS_CONSOLE_OVERSCAN_X;
            posy = pios_console_y * ms_font_info.y + PIOS_CONSOLE_OVERSCAN_Y;
            characters = printText ( console_fb, posx, posy, (void*) fgcolor, str+begin, false );
            
            pios_console_x=homex;
            pios_console_y++;
            begin=i+1;
                        
            if ( pios_console_y >= PIOS_FBCONSOLE_MAX_LINES )
            {
                pios_fbconsole_clear();
            }
        }
    }
    posx = pios_console_x * ms_font_info.x + PIOS_CONSOLE_OVERSCAN_X;
    posy = pios_console_y * ms_font_info.y + PIOS_CONSOLE_OVERSCAN_Y;
    characters = printText ( console_fb, posx, posy, (void*) fgcolor, str+begin, false );
    if (characters > 0)
        pios_console_x+=characters;
}

int pios_fbconsole_read ()
{    
    char in = getchar();
    if ( in=='<' )
    {
        if (inputBufferLength > 0)
        {
            inputBufferLength--;
            inputBuffer[inputBufferLength] = 0;
            pios_fbconsole_clearLine ( );
            pios_fbconsole_write ( "$ \0" );
            pios_fbconsole_write ( inputBuffer );
        }
    }
    else 
    {
        if (inputBufferLength >= PIOS_CONSOLE_MAX_INPUT_LENGTH)
            return -1;
        inputBuffer[inputBufferLength] = in;
        pios_fbconsole_write ( &inputBuffer[inputBufferLength] );
        inputBufferLength++;
        if (in=='\n' || in=='\r')   
            return pios_fbconsole_execute ();
    }
    return -1;
}

int pios_fbconsole_clearLine () 
{
    pios_console_x = homex; 
    int currentY = pios_console_y * ms_font_info.y + PIOS_CONSOLE_OVERSCAN_Y;
    int posx = pios_console_x * ms_font_info.x+ PIOS_CONSOLE_OVERSCAN_X;
    int maxx = console_fb->width - (pios_console_x * ms_font_info.x+ PIOS_CONSOLE_OVERSCAN_X);
    for ( int y = currentY; y < currentY+16; y++ )
    {
        for ( int x = posx; x < maxx; x++ )
        {
            set ( console_fb, x, y, fillcolor );
        }
    } 
    return 0;
}

int pios_fbconsole_execute ()
{
    // seperate command and arguments by Nulls
    int numArgs = 0;
    char* args[10] = { NULL };
    bool space=true;
    //bool escaped=false;
    for (int i=0; inputBufferLength; i++)
    {
        if (inputBuffer[i] == ' ')
        {
            inputBuffer[i] = '\0';
            if (space == false)
            {
                numArgs++;
                space=true;
            }
        }
        else
        {
            if (space==true)
            {
                args[numArgs] = &inputBuffer[i];
                space=false;
            }
        }
        if (inputBuffer[i] == '\n' || inputBuffer[i] == '\r')
        {
            inputBuffer[i] ='\0';
            numArgs++;
            break;
        }
    }
    args[numArgs] = NULL;
    
    // find the correct command
    int cmdIndex=-1;
    for (int i=0; i < commandListLength; i++)
    {
        if (strcmp ( commandList[i].command, inputBuffer ) == 0)
        {
            cmdIndex=i;
            break;
        }
    }
    
    // execute the command or issue an error message
    int result=0;
    if (cmdIndex>=0)
    {
        result=commandList[cmdIndex].cmdfunc ( numArgs, args );
    }
    else
    {
        /**pios_fbconsole_write ( "The command '" );
        pios_fbconsole_write ( args[0] );
        pios_fbconsole_write ( "' has not been found.\n" );*/
        
        pios_fbconsole_write ("?Syntax Error.\n");
    }

    // reset the inputbuffer
    for (int i=0; i<PIOS_CONSOLE_MAX_INPUT_LENGTH; i++)
    {
        inputBuffer[i] = 0;
    }
    inputBufferLength = 0;
    
    pios_fbconsole_write ("$ \0");
    return result;
}

bool pios_fbconsole_addCommand ( char* cmd, int (*cmdfunc) (int, char**) )
{    
    if (commandListLength < PIOS_CONSOLE_MAX_COMMANDS)
    {
        strncpy ( commandList[commandListLength].command, cmd, PIOS_CONSOLE_MAX_COMMAND_LENGTH );
        commandList[commandListLength].cmdfunc = cmdfunc;
        commandListLength++;
        return true;
    }
    
    return false;
}
