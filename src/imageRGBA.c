#include "image.h"
#include "fontd.h"

void yil_interpolateRGBA ( void *colbuff, void* colors, float* coeff, int num )
{
    struct rgba* out = (struct rgba*) colbuff;
    struct rgba* in = (struct rgba*) colors;
    out->red=0;
    out->green=0;
    out->blue=0;
    out->alpha=0;
    for (int i=0; i<num; i++)
    {
        out->red   += in[i].red   * coeff[i];
        out->green += in[i].green * coeff[i];
        out->blue  += in[i].blue  * coeff[i];
        out->alpha += in[i].alpha * coeff[i];
    }
}

int yil_resizeRGBA ( struct Image* dest, struct Image* src, enum Interpolation_t interpolation )
{
    switch (interpolation)
    {
        case NEAREST:
        {
            float wRatio = (float)src->width / dest->width;
            float hRatio = (float)src->height / dest->height;
       
            struct rgba* dptr = dest->payload;
            struct rgba* sptr = src->payload;
            for (int y=0; y<dest->height; y++)
            {
                for (int x=0; x<dest->width; x++)
                {
                    int h=(hRatio*y);
                    int w=(wRatio*x);
                    //printf (" %d,%d --> %d,%d\n", w,h,x,y);
                    dptr[y*dest->width + x] = sptr[ h*src->width + w ];
                }
            }
            return 1;
            break;
        }
        case BILINEAR:
        {
            float wRatio = (float)src->width / dest->width;
            float hRatio = (float)src->height / dest->height;
       
            struct rgba* dptr = dest->payload;
            struct rgba* sptr = src->payload;
            for (int y=0; y<dest->height; y++)
            {
                for (int x=0; x<dest->width; x++)
                {
                    int h=(hRatio*y);
                    int w=(wRatio*x);
                    float hdiff = (hRatio*y) - h;
                    float wdiff = (wRatio*x) - w;
                    
                    int index = h*src->width + w;
                    float a = (1-hdiff) * (1-wdiff);
                    float b = (1-hdiff) * (wdiff);
                    float c = (hdiff) * (wdiff);
                    float d = (hdiff) * (1-wdiff);
                    //struct rgba fin;
                    
                    dptr->red = sptr[index].red * a +
                              sptr[index+1].red * b + 
                              sptr[index+src->width+1].red * c +
                              sptr[index+src->width].red * d;
                    dptr->green = sptr[index].green * a +
                                sptr[index+1].green * b + 
                                sptr[index+src->width+1].green * c +
                                sptr[index+src->width].green * d;
                    dptr->blue = sptr[index].blue * a +
                               sptr[index+1].blue * b + 
                               sptr[index+src->width+1].blue * c +
                               sptr[index+src->width].blue * d;
                    dptr->alpha = sptr[index].alpha * a +
                               sptr[index+1].alpha * b + 
                               sptr[index+src->width+1].alpha * c +
                               sptr[index+src->width].alpha * d;
                    dptr++;
                }
            }
            return 1;
            break;
        }
        default:
            break;
    }
    return -1;
}

int yil_rotateRGBA ( struct Image* dest, struct Image* src, float alpha, enum Interpolation_t interpolation )
{
    
    switch (interpolation)
    {
        case NEAREST:
            // TODO write rotate-code but not now
            return 1;
            break;
        default:
            break;
    }
    return -1;
}

void yil_contrastRGBA ( struct Image* img, int16_t value )
{
    if (value > 255)
    {
        value = value % 256;
    }
    else if (value < 0)
    {
        value = -value;
        value = value % 256;
        value = -value;
    }
    struct rgba* ptr = img->payload;
    uint64_t size = img->width * img->height;
    uint8_t valR, valG, valB, valA;
    for (uint64_t i = 0; i<size; i++)
    {
        if (ptr->red < 128)
            valR = -value;
        else
            valR = value;
        
        if (ptr->green < 128)
            valG = -value;
        else
            valG = value;
        
        if (ptr->blue < 128)
            valB = -value;
        else
            valB = value;
        
        if (ptr->alpha < 128)
            valA = -value;
        else
            valA = value;
        
        if (ptr->red + valR > 255)
            ptr->red = 255;
        else if (ptr->red + valR < 0)
            ptr->red = 0;
        else
            ptr->red += valR;

        if (ptr->green + valG > 255)
            ptr->green = 255;
        else if (ptr->green + valG < 0)
            ptr->green = 0;
        else
            ptr->green += valG;
            
        if (ptr->blue + valB > 255)
            ptr->blue = 255;
        else if (ptr->blue + valB < 0)
            ptr->blue = 0;
        else
            ptr->blue += valB;
        
        if (ptr->alpha + valA > 255)
            ptr->alpha = 255;
        else if (ptr->alpha + valA < 0)
            ptr->alpha =0;
        else
            ptr->alpha += valA;
        
        ptr++;
    }
}

void yil_swapRGBA ( struct Image* img, int type )
{
    struct rgba* front = img->payload;
    struct rgba* back;
    uint64_t size;
    
    switch (type)
    {
        case SWAP_X:
            back = ((struct rgba*) img->payload + img->width -1);
            for (uint32_t y = 0; y < img->height; y++)
            {
                for (uint32_t x = 0; x < img->width/2; x++)
                {
                    struct rgba cache = *front;
                    *front = *back;
                    *back = cache;
                    
                    front++;
                    back--;
                }
                back += img->width + (img->width/2);
                front += (img->width/2);
            }
            break;
        case SWAP_Y:
            size = img->height * img->width;
            back = ((struct rgba*) img->payload + size) - img->width;
            for (uint32_t y = 0; y < img->height/2; y++)
            {
                for (uint32_t x = 0; x < img->width; x++)
                {
                    struct rgba cache = *front;
                    *front = *back;
                    *back = cache;
                    
                    front++;
                    back++;
                }
                back -= (img->width + img->width);
            }
            break;
        case SWAP_XY:
            size = img->height * img->width;
            back = ((struct rgba*) img->payload + size) -1;
            for (uint64_t i=0; i<size/2; i++)
            {
                struct rgba cache = *front;
                *front = *back;
                *back = cache;
                
                front++;
                back--;
            }
            break;
        default:
            break;
    }
}

int yil_printCharRGBA ( struct Image* img, uint32_t x, uint32_t y, void* color, const char c )
{
    if (c<0) 
        return -1;
    uint8_t *fptr = ms_font+(c*ms_font_info.y);
    struct rgba* imgptr = (struct rgba*) img->payload + x + y*img->width;
    for (int yc = 0; yc<ms_font_info.y; yc++)
    {
        for (int xc=0; xc<ms_font_info.x; xc+=8)
        {
            uint8_t mask = 0x01;
            for (int i=0; i<8; i++)
            {
                if (((*fptr)&mask) > 0)
                {
                    *imgptr = *(struct rgba*) color;
                }
                mask <<= 1;
                imgptr++;
            }
            fptr ++;
        }
        imgptr+=img->width;
        imgptr-=ms_font_info.x;
    }
    return 1;
}

void yil_brightenRGBA ( struct Image* img, int16_t value )
{
    uint64_t size = img->width * (uint64_t) img->height;
    uint8_t* ptr = (uint8_t*) img->payload;
    for (uint64_t i=0; i<size; i++)
    {
        for (char x = 0; x<4; x++)
        {
            if ( x == 3 )   // alpha-value -> pass
                continue;
            if (*ptr + (int16_t)value > 255)
                *ptr = 255;
            else if (*ptr +(int16_t)value < 0)
                *ptr = 0;
            else
                *ptr += value;
            ptr++;
        }
    }
}

int yil_fillRGBA ( struct Image* img, void* color )
{
    struct rgba *c = (struct rgba*) color;
    struct rgba *ptr = (struct rgba*) img->payload;
    for (uint32_t y = 0; y < img->height; y++)
    {
        for (uint32_t x = 0; x < img->width; x++)
        {
            *ptr = *c;
            ptr++;
        }
    }
    return 0;
}

void yil_blendRGBA ( struct Image* dest, struct Image* src, float alpha )
{
    int maxx = (dest->width < src->width) ? src->width : dest->width;
    int maxy = (dest->height < src->height) ? src->height : dest->height;
    struct rgba *dpay = dest->payload, *spay = src->payload;
    for (int y=0; y<maxy; y++)
    {
        struct rgba* d = &dpay[y*dest->width], *s = &spay[y*src->width];
        for (int x=0; x<maxx; x++)
        {
            d->red   = (1-alpha)*(d->red)   + alpha*(s->red);
            d->green = (1-alpha)*(d->green) + alpha*(s->green);
            d->blue  = (1-alpha)*(d->blue)  + alpha*(s->blue);
            d->alpha  = (1-alpha)*(d->alpha)  + alpha*(s->alpha);
            d++; s++;
        }
    }
}

int yil_copyRGBA ( struct Image* dest, struct Image* src, enum CopyMode mode )
{
    struct rgba* d = (struct rgba*) dest->payload;
    struct rgba* s = (struct rgba*) src->payload;
    if ( mode == REPLACE )
    {
        for (uint32_t y = 0; y<dest->height; y++)
        {
            for (uint32_t x = 0; x<dest->width; x++)
            {
                *d = *s;
                d++;
                s++;
            }
        }
    }
    else if ( mode == SWAP )
    {
        struct rgba save;
        for (uint32_t y = 0; y<dest->height; y++)
        {
            for (uint32_t x = 0; x<dest->width; x++)
            {
                save = *d;
                *d = *s;
                *s = save;
                d++;
                s++;
            }
        }
    }
    return 0;
}

void* yil_getRGBA ( struct Image* img, int x, int y)
{
    if (x<0 || y<0 || x>=img->width || y >= img->height)
        return NULL;
    struct rgba * ptr = (struct rgba*) img->payload;
    ptr+=x+(y*img->width);
    return (void*) ptr;
}


void yil_setRGBA ( struct Image* img, int x, int y, void* color )
{
    if (x < 0 || y < 0 || x >= img->width || y >= img->height)
        return;
    struct rgba * ptr = (struct rgba*) img->payload;
    ptr+=x+(y*img->width);
    struct rgba* c = (struct rgba*) color;
    *ptr = *c;
}
