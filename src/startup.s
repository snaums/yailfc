.section ".data"

irq_vector_start:
    ldr pc, _reset_h
    ldr pc, _undefined_instruction_vector_h
    ldr pc, _software_interrupt_vector_h
    ldr pc, _prefetch_abort_vector_h
    ldr pc, _data_abort_vector_h
    ldr pc, _unused_handler_h
    ldr pc, _interrupt_vector_h
    ldr pc, _fast_interrupt_vector_h

_reset_h:                           .word   _reset_
_undefined_instruction_vector_h:    .word   pios_exception_undef
_software_interrupt_vector_h:       .word   pios_exception_swi
_prefetch_abort_vector_h:           .word   pios_exception_abort
_data_abort_vector_h:               .word   pios_exception_abort
_unused_handler_h:                  .word   pios_exception_reset
_interrupt_vector_h:                .word   pios_exception_irq
_fast_interrupt_vector_h:           .word   pios_exception_fiq

.section ".init"

_reset_:
    ldr sp, =stack_top
    bl pios_jtag_init

    mov r0, #47
    mov r1, #1
    bl pios_gpio_pinmode
    
    mov r0, #47
    mov r1, #0
    bl pios_gpio_write    
    
    /// copies interrupt vectors to address 0x0000 0000 
    mov     r0, #0x00
    ldr     r1, =irq_vector_start
    ldmia   r0!,{r2, r3, r4, r5, r6, r7, r8, r9}    /** load 32 Byte worth of data **/
    stmia   r1!,{r2, r3, r4, r5, r6, r7, r8, r9}    /** store 32 Byte **/
    ldmia   r0!,{r2, r3, r4, r5, r6, r7, r8, r9}
    stmia   r1!,{r2, r3, r4, r5, r6, r7, r8, r9}
        
    /// load C15 control register c1-c1,0,0 (c1 Control Register) into r0
    /*mrc p15, 0, r0, c1, c0, 0
    ldr r1, =0x00400000     /// set bit 22 (U-bit)
    orr r0, r1
    mcr p15, 0, r0, c1, c0, 0*/
	
	// enable FPU
	mrc p15, 0, r0, c1, c0, 2
    orr r0, r0, #0x300000            /* single precision */
    orr r0, r0, #0xC00000            /* double precision */
    mcr p15, 0, r0, c1, c0, 2
    mov r0, #0x40000000
    fmxr fpexc,r0
	
	mov r0, #47
    mov r1, #1
    bl pios_gpio_write    
	
	bl init1
	
	bl main
	
halt:
	//wfe
	b halt
