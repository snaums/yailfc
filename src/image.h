/**
 * \file image.h
 * \author Stefan Naumann
 * \brief contains image functionality (on a plain image), supports several color-models, nearly object oriented
 * \todo give image-functions a yil_prefix, maybe rework the object-orientation-approach?
 * \todo doxygen documentation!
 **/

#ifndef PIOS_GNRC_IMG
#define PIOS_GNRC_IMG

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

enum CopyMode
{
    REPLACE,
    SWAP
} typedef CopyMode;

/**
 * \brief describes possible image color-models.
 **/
enum ImageColor
{
    RGB = 0,        ///< Red, Green, Blue (24 bit per Pixel)
    RGBA = 1,       ///< Red, Green, Blue, Alpha (32 bit per Pixel)
    YCbCr = 2,      ///< not implemented at all
    GRAY = 3        ///< grayscale (8 bit per pixel)
} typedef ImageColor;

/**
 * \brief described possible modes for swapping
 **/
enum Swap_t
{
    SWAP_NO = 0,        ///< no swap at all
    SWAP_X = 1,         ///< swap in x-direction
    SWAP_Y = 2,         ///< swap in y-direction
    SWAP_XY = 3         ///< swap both in x and y-direction
} typedef Swap_t;

/**
 * \brief interpolation-mode for resizing (and maybe rotation later on TODO)
 **/
enum Interpolation_t
{
    NEAREST,     ///< nearest neighbor
    BILINEAR     ///< bilinear interpolation
} typedef Interpolation_t;

struct Image;

struct ImageVTable
{
    void (*brighten) (struct Image*, int16_t);
    void (*contrast) (struct Image*, int16_t);
    int (*resize) (struct Image*, struct Image*, enum Interpolation_t);
    void (*swap) (struct Image*, int type);
    int (*rotate) (struct Image*, struct Image*, float, enum Interpolation_t);
    int (*printChar) (struct Image*, uint32_t, uint32_t, void*, const char);
    int (*fill) (struct Image*, void*);
    int (*copy) (struct Image*, struct Image*, enum CopyMode);
    void (*interpolate) (void *, void*, float*, int );
    void* (*get) (struct Image*, int, int );
    void (*set) (struct Image*, int, int, void* );
    void (*blend) (struct Image*, struct Image*, float );
    int (*sprite) (struct Image*, struct Image*, int32_t, int32_t, uint32_t, uint32_t );

} typedef ImageVTable;

/**
 * \brief data structure for describing images
 **/
struct Image
{
    enum ImageColor bitdepth;   ///< color model used for this image
    int32_t width;              ///< width of the image in pixel
    int32_t height;             ///< height of the image in pixel
    void *payload;              ///< one-dimensional array of pixel-data (hopefully big enough)
    struct ImageVTable vtable;
    uint8_t colorsize;          ///< number of bytes for the description of a complete pixel
} typedef Image;

#pragma pack (1)
/**
 * \brief RGB-data
 **/
struct rgb
{
    uint8_t red, green, blue;
} typedef rgb;
#pragma pack (1)
struct rgba {
    uint8_t red, green, blue, alpha;
} typedef rgba;
#pragma pack (1)
struct grayscale {
    uint8_t gray;  
} typedef grayscale;

/**
 * \brief creates an image data structure. Will not allocate the memory for the image-data
 * \param bitdepth the bitdepth of the image
 * \param w the width in pixel
 * \param h height in pixel
 * \param payload the already allocated memory-region for the image-data
 * \return a struct containing data for working with the image-functions
 **/
struct Image createImage ( enum ImageColor bitdepth, int32_t w, int32_t h, void* payload );

/**
 * \brief draws a flat rectangle onto an image
 * \param[in|out] img the image to be drawn onto
 * \param[in] x the x-coordinate of the top left corner
 * \param[in] y the y-coordinate of the top left corner
 * \param[in] xend the x-coordinate of the bottom right corner
 * \param[in] yend the y-coordinate of the bottom right corner
 * \param[in] color the color-value; make sure it fits the image!
 **/
void yil_flatRectangle ( struct Image* img, int x, int y, int xend, int yend, void* color );



/**
 * \brief dummy-function for brightening an image
 * \param img the image to be processed
 * \param value the value to be added to the pixel-data
 **/
void brighten ( struct Image* img, int16_t value );
/**
 * \brief dummy-function for increasing the contrast of an image
 * \param img the image to be processed
 * \param value the value to be added to or subtracted from the pixel-data
 **/
void contrast ( struct Image* img, int16_t value );

/**
 * \brief dummy-function for swapping pixels in an image
 * \param img the image to be processed
 * \param type the direction of the swapping
 **/
void swap ( struct Image* img, int type );
/**
 * \brief dummy-function for resizing an image
 * \param dest the image for output (needs to be at least (nw, nh) pixels wide)
 * \param src the image used as input
 * \param interpolation the interpolation mode used for resizing the image
 **/
int resize ( struct Image* dest, struct Image* src, enum Interpolation_t interpolation );

void debugColor ( struct Image* img, void* color );
int rotate ( struct Image* dest, struct Image* src, float alpha, enum Interpolation_t interpolation );
int printText ( struct Image* img, int x, int y, void* color, const char* str, bool home );
int copy ( struct Image* dest, struct Image* src, enum CopyMode mode );
void* get ( struct Image* img, int x, int y );
void set ( struct Image* img, int x, int y, void* color );

int printIcon ( struct Image* img, uint32_t x, uint32_t y, void* color, uint8_t* icon, int xsize, int ysize );

int fill ( struct Image* img, void* color );
int fillRGBA ( struct Image* img, void* color );

int yil_sprite ( struct Image* backdrop, struct Image* sprite, int32_t x, int32_t y, uint32_t width, uint32_t height );
int yil_spriteRGB ( struct Image* backdrop, struct Image* sprite, int32_t x, int32_t y, uint32_t width, uint32_t height );

void* getRGB ( struct Image* img, int x, int y);
void* getGray ( struct Image* img, int x, int y );
void setRGB ( struct Image* img, int x, int y, void* color );
void setPtrAlpha ( struct Image* img, void* ptr, void* color, float alpha );
void setPtr ( enum ImageColor index, void* ptr, void* color );

/**
 * \brief blend two images together. src will be blended over the destination image
 * \param[in] dest the "lower" destination image
 * \param[in] src the "higher" source image
 * \param[in] alpha the alpha-value of the complete(!) source-image
 **/
void blend ( struct Image* dest, struct Image* src, float alpha );




/**
 * \brief blend two RGB images together. src will be blended over the destination image
 * \param[in] dest the "lower" destination image
 * \param[in] src the "higher" source image
 * \param[in] alpha the alpha-value of the complete(!) source-image
 **/
void blendRGB ( struct Image* dest, struct Image* src, float alpha );

/**
 * \brief function for brightening an RGB-image
 * \param img the image to be processed
 * \param value the value to be added to the pixel-data
 **/
void brightenRGB ( struct Image* img, int16_t value );
/**
 * \brief function for swapping pixels in an RGB-image
 * \param img the image to be processed
 * \param type the direction of the swapping
 **/
void swapRGB ( struct Image* img, int type );
/**
 * \brief function for increasing the contrast of an RGB-image
 * \param img the image to be processed
 * \param value the value to be added to or subtracted from the pixel-data
 **/
void contrastRGB ( struct Image* img, int16_t value );
/**
 * \brief function for resizing an RGB-image
 * \param dest the image for output (needs to be at least (nw, nh) pixels wide)
 * \param src the image used as input
 * \param interpolation the interpolation mode used for resizing the image
 **/
int resizeRGB ( struct Image* dest, struct Image* src, enum Interpolation_t interpolation );
int copyRGB2RGBA ( struct Image* dest, struct Image* src, enum CopyMode mode );
int resizeRGB2RGBA ( struct Image* dest, struct Image* src, enum Interpolation_t interpolation );
int rotateRGB ( struct Image* dest, struct Image* src, float alpha, enum Interpolation_t interpolation );
int printCharRGB ( struct Image* img, uint32_t x, uint32_t y, void* color, const char c );

int fillRGB ( struct Image* img, void* color );
int copyRGB ( struct Image* dest, struct Image* src, enum CopyMode mode );

int filter ( struct Image* img, void (*filterFunc) ( struct Image*, uint8_t *, void* ), void* data);

void yil_interpolateColors ( struct Image* img, void *colbuff, void* colors, float* coeff, int num );
void yil_interpolateRGB ( void *colbuff, void* colors, float* coeff, int num );

void yil_interpolateRGBA ( void *colbuff, void* colors, float* coeff, int num );

int yil_resizeRGBA ( struct Image* dest, struct Image* src, enum Interpolation_t interpolation );

int yil_rotateRGBA ( struct Image* dest, struct Image* src, float alpha, enum Interpolation_t interpolation );
void yil_contrastRGBA ( struct Image* img, int16_t value );

void yil_swapRGBA ( struct Image* img, int type );


int yil_printCharRGBA ( struct Image* img, uint32_t x, uint32_t y, void* color, const char c );

void yil_brightenRGBA ( struct Image* img, int16_t value );

int yil_fillRGBA ( struct Image* img, void* color );

void yil_blendRGBA ( struct Image* dest, struct Image* src, float alpha );
int yil_copyRGBA ( struct Image* dest, struct Image* src, enum CopyMode mode );
void* yil_getRGBA ( struct Image* img, int x, int y);


void yil_setRGBA ( struct Image* img, int x, int y, void* color );

#endif
