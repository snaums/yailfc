#include "texture.h"

int yil_texturizeAlpha ( uint8_t* ptr, uint8_t* color, int colorsize, float overallAlpha )
{
    float alpha = ((float) color[3] / 255.f) * overallAlpha;
    if (alpha >= 0.999)
    {
        for ( int i = 0 ; i<colorsize; i++ )
        {
            ptr[i] = color[i];
        }
    }
    else
    {
        for ( int i = 0 ; i<colorsize; i++ )
        {
            ptr[i] = ptr[i] * (1-alpha) + color[i] * alpha;
        }
        return 1;
    }
    return 0;
}

int yil_texturize ( struct Image* img, uint8_t* ptr, float alpha, float tx, float ty, void* icolor )
{
    struct Image* tex = yil_context.texture;
    if (tex == NULL || (tex->bitdepth != img->bitdepth && tex->bitdepth != RGBA) )
        return -1;
    
    uint8_t* col;
    uint8_t fincol[tex->colorsize];
    if ( yil_context.texture_interpol == YIL_BILINEAR )
    {
        float cx = tx * tex->width +.5;
        float cy = ty * tex->height+.5;
        int32_t mx = cx;
        int32_t my = cy;
        
        cx -= mx;
        cy -= my;
                
        int32_t x = (abs(mx)) % (tex->width);
        int32_t y = (abs(my)) % (tex->height);
          
        if ( x <= 0 )
            x = 1;
        if ( y <= 0 )
            y = 1;
            
        uint8_t* col1 = ((uint8_t*)tex->payload) + (x + y*tex->width) * tex->colorsize;
        uint8_t* col2 = col1-tex->colorsize;
        uint8_t* col3 = col2-(tex->width*tex->colorsize);
        uint8_t* col4 = col3+tex->colorsize;
                
        for ( int i = 0; i < tex->colorsize; i++ )
        {
            fincol[i] = col1[i] *    cx  *    cy  + 
                        col2[i] * (1-cx) *    cy  + 
                        col3[i] * (1-cx) * (1-cy) + 
                        col4[i] *    cx  * (1-cy);
        }
        col = fincol;
    }
    else
    {
        int32_t x = ( (int32_t) ((tx * tex->width) +.5)) % (int32_t) tex->width;
        int32_t y = ( (int32_t) ((ty * tex->height)+.5)) % (int32_t) tex->height;
            
        if ( x < 0 )
            x += tex->width;
        if ( y < 0 )
            y += tex->height;
            
        col = ((uint8_t*)tex->payload) + (x + y*tex->width) * tex->colorsize;
    }
    
    uint8_t bcol[4];
    if ( yil_context.lighting_enable )
    {
        uint8_t *input_color = icolor;
        for ( int i = 0; i<tex->colorsize; i++ )
        {
            bcol[i] = col[i] * (input_color[i] / 255.f);
        }
        col = bcol;
    }
    
    if (tex->bitdepth == RGBA)
    {
        return yil_texturizeAlpha ( ptr, col, img->colorsize, alpha );
    }
    
    if (alpha >= 0.999)
    {
        for ( int i = 0 ; i<img->colorsize; i++ )
        {
            ptr[i] = col[i];
        }
    }
    else
    {
        for ( int i = 0 ; i<img->colorsize; i++ )
        {
            ptr[i] = ptr[i] * (1-alpha) + col[i] * alpha;
        }
    }
    return 0;
}

/*
int yil_addTexture ( struct Image* img, int flags )
{
    if ( yil_context.texture_num < YIL_MAX_TEX )
    {
        yil_context.texture[yil_context.texture_num] = img;
        yil_context.texture_cap[yil_context.texture_num] = flags;
        
        yil_context.texture_num ++;
        return 0;
    }
    else
    {
        return -1;
    }
}

int yil_removeTexture ( int index )
{
    for (int i=index; (i+1)<YIL_MAX_TEX; i++)
    {
        yil_context.texture[i] = yil_context.texture[i+1];
        yil_context.texture_coord[i][0] = yil_context.texture_coord[i+1][0];
        yil_context.texture_coord[i][1] = yil_context.texture_coord[i+1][1];
        yil_context.texture_cap[i] = yil_context.texture_cap[i+1];
    }
    yil_context.texture[YIL_MAX_TEX-1]=NULL;
    yil_context.texture_num --;
    
    return 0;
}

int yil_replaceTexture ( int index, struct Image* newTex, int flags )
{
    if ( index >= yil_context.texture_num )
        return -1;
        
    yil_context.texture[index] = newTex;
    yil_context.texture_cap[index] = flags;
    
    return 0;
}

struct Image* yil_indexTexture ( int index )
{
    return yil_context.texture[index];
}

int yil_createMipMap ( struct Image* img, struct Image* target )
{
    uint32_t w = img->width;
    uint32_t h = img->height;
    
    
}*/
