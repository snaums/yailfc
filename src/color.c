#include "image.h"

struct rgb yil_fblack = {.red=0, .green=0, .blue=0};
void* yil_fixedcolor_black = &yil_fblack;
struct rgb yil_fwhite = {.red=255, .green=255, .blue=255};
void* yil_fixedcolor_white = &yil_fwhite;
