/**
 * \file camera.h
 * \author Stefan Naumann
 * \brief interface for manipulating the camera and some preset modes
 **/

#ifndef YIL_CAMERA
#define YIL_CAMERA

/**
 * \brief struct to describe the camera position and angles of the camera
 **/
struct _camera
{
    float tx,       ///< x-position
          ty,       ///< y-position
          tz;       ///< z-position
    float xphi,     ///< angle to turn around the x-axis    
          yphi,     ///< angle to turn around the y-axis
          zphi;     ///< angle to turn around the z-axis
};

/**
 * \brief initialise a camera-struct with default values
 * \return an "empty" camera-struct
 **/
struct _camera yil_cameraInit ();

/**
 * \brief set the camera to specific position and view a specific area
 * \param[inout] cam the camera-object to be modified
 * \param[in] x x-position
 * \param[in] y y-position
 * \param[in] x
 * \param[in] tx target x-position
 * \param[in] ty
 * \param[in] tz
 * \param[in] ux x-value of the up-vector
 * \param[in] uy 
 * \param[in] uz
 **/
void yil_cameraLookAt ( struct _camera* cam, float  x, float  y, float  z, 
                                             float tx, float ty, float tz, 
                                             float ux, float uy, float uz );
/**
 * \brief move the camera with a specific offset (does not return to 0 beforehand)
 * \param[inout] cam the camera-object to be modified
 * \param[in] x x-offset
 * \param[in] y
 * \param[in] z
 **/
void yil_cameraMove ( struct _camera* cam, float x, float y, float z );
/**
 * \brief rotate the camera by specific angles
 * \param[inout] cam camera object to be modified
 * \param[in] xphi angle around the x-angle
 * \param[in] yphi angle around the y-angle
 * \param[in] zphi angle around the z-angle
 **/
void yil_cameraRotate ( struct _camera* cam, float xphi, float yphi, float zphi );

/**
 * \brief transform vertices according to the camera coordinate-system
 * \param[in] cam camera object for the camera-coord
 * \param[inout] pts the vertex-data to be transformed
 * \param[in] num the number of vertices to be transformed
 **/
void yil_camera ( struct _camera* cam, float* pts, int num );
/**
 * \brief sets a transformation-function for the camera-coordinate-system
 * \param[in] fn the function to be used for transforming vertices by yil_camera()
 * \sa yil_camera, yil_ego
 **/
void yil_cameraSet ( void (*fn)(struct _camera*, float*, int) );

/**
 * \brief a function which implements a simple ego-perspective. 
 * \param[in] cam the camera object for coordinate-system fixation
 * \param[inout] pts the vertex-data
 * \param[in] num the number of vertices to be transformed
 **/
void yil_ego ( struct _camera* cam, float* pts, int num );


#endif
