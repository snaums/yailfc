#include "cmd.h"
#include <stdio.h>
#include <stdlib.h>

extern enum _state yil_state;

int echoCmd ( int argc, char** argv )
{   
    printf ("%d %s\n", argc, argv[0]);
    if (argc > 1)
    {
        printf ("%s\n", argv[1]);
        pios_fbconsole_write (argv[1]);
        pios_fbconsole_write ( "\n" );
    }
    else
    {
        pios_fbconsole_write ( "Usage: ");
        pios_fbconsole_write ( argv[0] );
        pios_fbconsole_write ( " <something to output>\n");
    }
    return 0;
}

int cake ( int argc, char** argv )
{
    pios_fbconsole_write ("Oh, you found it.\n");
    pios_fbconsole_write( "\
       ,\n\
    ___|___\n\
   {~*~*~*~}\n\
   `-------`\n");

    return 0;
}

int setStart ( int argc, char** argv )
{
    yil_state = SETTINGS;
    return 0;
}

char* helptexts[9]={ "Just enter 'run' to run the demo.\n",
                     "Why not try 'run'.\n",
                     "Is it too hard to type in 'run'?\n",
                     "Mate... 'run'. Not that hard, is it?\n",
                     "Now you are just waisting my time. Try 'run'\n",
                     "Do think there is something here? Try 'run'\n",
                     "There are no easter eggs here, no cake. Just 'run'\n",
                     "The cake is a lie. No cake! None! Enter 'run'!\n",
                     "Segmentation fault.\n" };
int help ( int argc, char** argv )
{
    static int r = 0;
    pios_fbconsole_write ( helptexts[r++] );
    if ( r > 8 ) r = 8;
    return 0;
}

int cmdCrash ( int argc, char** argv )
{
    void* lr;
    #ifdef PLATFORM
    asm volatile ( "mov %0, lr\n"
                   : "=r" (lr) );
    #else
    lr = (void*)0xdeadbeef;
    #endif

    int r = atoi(argv[1]);
    crash (r, lr );   
    
    return 0; 
}

int cmdClear ( int argc, char** argv )
{
    pios_fbconsole_clear();
    return 0;
}

int uname ( int argc, char** argv )
{
    if (argc==1)
        pios_fbconsole_write ( "piOS\n" );
    else if (argc==2)
    {
        if (strcmp ( argv[1], "-a" ) == 0)
            pios_fbconsole_write ( "PiOS v<M_PI>\n" );
        else if (strcmp ( argv[1], "--help" ) == 0 || strcmp ( argv[1], "-h" ) == 0)
        {
            pios_fbconsole_write ( "Usage: uname [-a]\n" );
        }
        else
        {
            pios_fbconsole_write ( "uname: Invalid Option" );
            pios_fbconsole_write ( argv[1] );
            pios_fbconsole_write ("\n'uname --help' gives more info\n");
            
        }
    }
    return 0;
}

int listCommands ( int argc, char ** argv )
{
    pios_fbconsole_write ("Available commands: \n");
    for ( int i = 0; i < pios_fbconsole_getCommandCount (); i+=2 )
    {
        pios_fbconsole_write (" -- ");
        pios_fbconsole_write ( pios_fbconsole_getCommand ( i ) );
        if ( i+1 < pios_fbconsole_getCommandCount() )
        {
            pios_fbconsole_write ("    -- ");
            pios_fbconsole_write ( pios_fbconsole_getCommand ( i+1 ) );
        }
        pios_fbconsole_write ( "\n");
    }
    return 0;
}
 
int yil_exit ( int argc, char** argv )
{
    int retval = 0;
    if ( argc > 1 )
    {
        retval = atoi ( argv[1] );
    }
    
    exit ( retval );
    return retval;
}
  
int run ( int argc, char** argv )
{
    yil_state = RENDERER;
    return 0;
} 
