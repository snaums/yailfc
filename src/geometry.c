#include "geometry.h"
#include "light.h"

#define X 0
#define Y 1
#define Z 2
#define W 3

int yil_geometry_num=0;

struct yil_geometry yil_geometry_array[YIL_GEOMETRY_MAX];

float yil_box_data[] = { 
    1, -1, 1, 1,
    1, 1, 1, 1,
    -1, 1, 1, 1,
    -1, -1, 1, 1,
    
    -1, -1, -1, 1, 
    -1, -1, 1, 1, 
    -1, 1, 1, 1,
    -1, 1, -1, 1,

    1, 1, -1, 1,
    -1, 1, -1, 1,
    -1, 1, 1, 1,
    1, 1, 1, 1,

    1, -1, -1, 1,
    -1, -1, -1, 1,
    -1, 1, -1, 1,
    1, 1, -1, 1,

    1, -1, 1, 1, 
    1, -1, -1, 1, 
    1, 1, -1, 1,
    1, 1, 1, 1,

    -1, -1, -1, 1,
    1, -1, -1, 1,
    1, -1, 1, 1,
    -1, -1, 1, 1
};

float yil_box_normal[] = {
    0,0,1,0,
    0,0,1,0,
    0,0,1,0,
    0,0,1,0,

    -1,0,0,0,
    -1,0,0,0,
    -1,0,0,0,
    -1,0,0,0,

    0,1,0,0,
    0,1,0,0,
    0,1,0,0,
    0,1,0,0,
    
    0,0,-1,0,
    0,0,-1,0,
    0,0,-1,0,
    0,0,-1,0,
    
    1,0,0,0,
    1,0,0,0,
    1,0,0,0,
    1,0,0,0,
    
    0,-1,0,0,
    0,-1,0,0,
    0,-1,0,0,
    0,-1,0,0,

};

float yil_plane_data[] = {
    1, 0, 1, 1,
    -1, 0, 1, 1,
    -1, 0, -1, 1,
    1, 0, -1, 1
};

float yil_plane_normal[] = {
    0,-1,0,0,
    0,-1,0,0,
    0,-1,0,0,
    0,-1,0,0
};

void yil_box ( float* buff, int num, float* normals )
{
    yil_geometry ( yil_box_data, buff, 6*16, num, normals, yil_box_normal );
}

void yil_plane ( float *buff, int num, float* normals )
{
    yil_geometry ( yil_plane_data, buff, 16, num, normals, yil_plane_normal );
}

void yil_planeEx ( float* buff, int x, int z, int num, float *normals )
{
    if ( num >= 16*x*z )
    {    
        int n = 0;
        for ( float xs = 0; xs < x; xs ++ )
        {
            for ( float zs = 0; zs < z; zs ++ )
            {
                buff[n+X] = xs;
                buff[n+Y] = 0.0;
                buff[n+Z] = zs;
                buff[n+W] = 1.0;
                
                normals[n+X] = 0;
                normals[n+Y] = -1;
                normals[n+Z] = 0;
                normals[n+W] = 0;
                n+=4;
                
                buff[n+X] = xs+1;
                buff[n+Y] = 0.0;
                buff[n+Z] = zs;
                buff[n+W] = 1.0;
                
                normals[n+X] = 0;
                normals[n+Y] = -1;
                normals[n+Z] = 0;
                normals[n+W] = 0;
                n+=4;
                
                buff[n+X] = xs+1;
                buff[n+Y] = 0.0;
                buff[n+Z] = zs+1;
                buff[n+W] = 1.0;
                
                normals[n+X] = 0;
                normals[n+Y] = -1;
                normals[n+Z] = 0;
                normals[n+W] = 0;
                n+=4;
                
                buff[n+X] = xs;
                buff[n+Y] = 0.0;
                buff[n+Z] = zs+1;
                buff[n+W] = 1.0;
                
                normals[n+X] = 0;
                normals[n+Y] = -1;
                normals[n+Z] = 0;
                normals[n+W] = 0;
                n+=4;
            }
        }
    }
    else
    {
        return ;
    }    
}

void yil_geometry ( float * in, float* buff, int minNum, int num, float* normalbuff, float* normalIn )
{
    if ( num < minNum ) 
        return;
        
    for ( ; minNum > 0; minNum-- )
    {
        *buff = *in;
        buff++;
        in++;
        
        *normalbuff = *normalIn;
        normalIn++;
        normalbuff++;
    }
}

// TODO: normals correct?
void yil_heightmap ( struct Image* height, float* pts, int* num, float offset, float scale, float* normal )
{
    // not enough space!?
    if (*num < (height->height-1) * (height->width-1) * 4)
        return; 
    if (height->bitdepth != GRAY)
        return;
    
    *num = 0;
    int n= 0;
    uint8_t value;
    int n1, n2;
    int k = 0;
    for (int j=1; j<height->height; j++)
    {
        for (int i=1; i<height->width; i++)
        {
            value = *(uint8_t*) get ( height, i-1, j-1 );
            pts[n+X]=i-1;
            pts[n+Y] = value * scale + offset;
            pts[n+Z]=j-1;
            pts[n+W]=1;
                        
            n1=n;            
            n+=4;  
            
            value = *(uint8_t*) get ( height, i, j-1 );
            pts[n+X]=i;
            pts[n+Y] = value * scale + offset;
            pts[n+Z]=j-1;
            pts[n+W]=1;
                        
            n2=n;
            n+=4;  
            
            value = *(uint8_t*) get ( height, i, j );
            pts[n+X]=i;
            pts[n+Y] = value * scale + offset;
            pts[n+Z]=j;
            pts[n+W]=1;
                        
            n+=4;
            
            value = *(uint8_t*) get ( height, i-1, j );
            pts[n+X]=i-1;
            pts[n+Y] = value * scale + offset;
            pts[n+Z]=j;
            pts[n+W]=1;    
                        
            float a[4] = { pts[n2+X] - pts[n1+X], 
                           pts[n2+Y] - pts[n1+Y], 
                           pts[n2+Z] - pts[n1+Z], 0};
            float b[4] = { pts[n+X] - pts[n1+X], 
                           pts[n+Y] - pts[n1+Y], 
                           pts[n+Z] - pts[n1+Z], 0};
            float norm[4] = { a[1] * b[2] - a[2] * b[1],
                              a[2] * b[0] - a[0] * b[2],
                              a[0] * b[1] - a[1] * b[0], 0};
            float length = sqrt ( norm[0] * norm[0] + norm[1] * norm[1] + norm[2] * norm[2] );
            norm[0] /= length;
            norm[1] /= length;
            norm[2] /= length;
            
            if ( norm[1] > 0 ) // normale zeigt nach unten
            {
                norm[0] = -norm[0];
                norm[1] = -norm[1];
                norm[2] = -norm[2];
            }
                        
            for ( ; k <= n; k+=4 )
            {
                normal[k+X] = norm[0];
                normal[k+Y] = norm[1];
                normal[k+Z] = norm[2];
                normal[k+W] = norm[3];
            }                   
            
            n+=4; 
            *num+=4;   
        }
    }
}

struct yil_geometry* yil_geometry_add ( enum yil_geometry_primitive type, float* pts, int numPts, struct Image* texture, float* texcoord, void* colors, float* alpha, bool loop, float* normals, float shinyness )
{
    if ( yil_geometry_num < YIL_GEOMETRY_MAX )
    {
        struct yil_geometry *geo = &yil_geometry_array[yil_geometry_num++];
        geo->type = type;
        geo->pts = pts;
        geo->numPts = numPts;
        geo->texture = texture;
        geo->texcoord = texcoord;
        geo->colors = colors;
        geo->alpha = alpha;
        geo->loop = loop;
        geo->normal = normals;
        geo->shinyness = shinyness;
        
        return geo;
    }
    return NULL;
}

int yil_geometry_drawAll ( struct Image* img, struct _camera* cam )
{
    for ( int i = 0; i < yil_geometry_num; i++ )
    {
        yil_geometry_draw ( img, &yil_geometry_array[i], cam );
    }
    return 0;
}

// TODO: normals
int yil_geometry_draw ( struct Image* img, struct yil_geometry* geo, struct _camera* cam )
{
    int add = geo->type;
    
    if ( add == 0 )
        return -1;
    
    yil_context.texture=geo->texture;
    
    float pts[geo->numPts*4];
    float normal[geo->numPts*4];
    for ( int i = 0; i < geo->numPts*4; i++ )
    {
        pts[i] = geo->pts[i];
        normal[i] = geo->normal[i];
    }
    
    // here would be a swell place for lighting 
    struct rgb* c = geo->colors;
    struct rgb colors[geo->numPts];
    if (yil_context.lighting_enable)
    {
        yil_light ( pts, geo, colors, cam, normal );
        c = colors;
    }
    
    /// camera transformation
    yil_camera ( cam, pts, geo->numPts );
    
    for ( int i=0; i < geo->numPts; i++ )
    {
        /// perspective transformation
        yil_matVecMul ( yil_context.perspective_matrix, (pts+(i<<2)), 4 );

        if ( !yil_context.ortho )
            yil_vecScale ( (pts+(i<<2)), fabs(1.f/pts[(i<<2)+3]), 3 );
    }
        
    /// NDC to Window space
    yil_NDC2Window ( pts, (uint32_t*) &yil_context.viewport, geo->numPts );
    int32_t buff[geo->numPts*4];
    yil_copyPoints ( buff, pts, geo->numPts );
                       
    /// draw the primitives                             
    for ( int i=0; i<geo->numPts; i+=add )
    {    
        if ( geo->loop && !yil_context.lighting_enable )
        {
            switch ( geo->type )
            {
                case YIL_RECT:
                    yil_rectangle ( img, (buff+(i<<2)), c, geo->texcoord, geo->alpha, normal );
                    break;
                case YIL_TRI:
                    yil_triangle ( img, (buff+(i<<2)), c, geo->texcoord, geo->alpha, normal );
                    break;
                case YIL_LINE:
                {
                    int indizes[2] = { 0, 1 };
                    yil_line ( img, (buff+(i<<2)), geo->colors, indizes );
                    break;
                }
                default:
                    break;
            }
        }
        else
        {
            uint8_t *col = (uint8_t*) c;
            col += img->colorsize * i;

            if ( geo->loop )
            {
                switch ( geo->type )
                {
                    case YIL_RECT:
                        yil_rectangle ( img, (buff+(i<<2)), col, geo->texcoord, geo->alpha, normal );
                        break;
                    case YIL_TRI:
                        yil_triangle ( img, (buff+(i<<2)), col, geo->texcoord, geo->alpha, normal );
                        break;
                    case YIL_LINE:
                    {
                        int indizes[2] = { 0, 1 };
                        yil_line ( img, (buff+(i<<2)), col, indizes );
                        break;
                    }
                    default:
                        break;
                }
            }
            else
            {
                switch ( geo->type )
                {
                    case YIL_RECT:
                        yil_rectangle ( img, (buff+(i<<2)), col, geo->texcoord+(i<<1), (geo->alpha+i), normal );
                        break;
                    case YIL_TRI:
                        yil_triangle ( img, (buff+(i<<2)), col, geo->texcoord+(i<<1), (geo->alpha+i), normal );
                        break;
                    case YIL_LINE:
                    {
                        int indizes[2] = { 0, 1 };
                        yil_line ( img, (buff+(i<<2)), col, indizes );
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
    return 0;
}

void yil_cone ( float* buff, int num, int angles, int height, float* normal )
{
    if ( angles < 4 )
        return;
    int minNum = angles * 4 * height;
    
    if ( num < minNum )
        return;
    
    float r = 1;
    float rstep = 1.f / height;
    int n1, n2;
    for ( int y = 0; y < height; y++ )
    {
        float lastX =1, lastZ = 0;
        float step = (2 * M_PI) / angles;
        float phi = step;
        int k = 0;
        for ( int n = 0; n < angles*16; )
        {
            int off = y*16*angles;
            buff[off+n+X] = r*lastX;
            buff[off+n+Y] = y;
            buff[off+n+Z] = r*lastZ;
            buff[off+n+W] = 1;
            n1=n;
            n+=4;
            
            r -= rstep;
            
            buff[off+n+X] = r*lastX;
            buff[off+n+Y] = y+1;
            buff[off+n+Z] = r*lastZ;
            buff[off+n+W] = 1;
            n2=n;
            n+=4;
            
            lastZ = sin ( phi );
            lastX = cos ( phi );
            
            buff[off+n+X] = r*lastX;
            buff[off+n+Y] = y+1;
            buff[off+n+Z] = r*lastZ;
            buff[off+n+W] = 1;
            n+=4;
            
            r += rstep;
            
            buff[off+n+X] = r*lastX;
            buff[off+n+Y] = y;
            buff[off+n+Z] = r*lastZ;
            buff[off+n+W] = 1;
                                    
            float a[4] = { buff[n2+X] - buff[n1+X], 
                           buff[n2+Y] - buff[n1+Y], 
                           buff[n2+Z] - buff[n1+Z], 0};
            float b[4] = { buff[n+X] - buff[n1+X], 
                           buff[n+Y] - buff[n1+Y], 
                           buff[n+Z] - buff[n1+Z], 0};
            float norm[4] = { a[1] * b[2] - a[2] * b[1],
                              a[2] * b[0] - a[0] * b[2],
                              a[0] * b[1] - a[1] * b[0], 0};
            float length = sqrt ( norm[0] * norm[0] + norm[1] * norm[1] + norm[2] * norm[2] );
            norm[0] /= length;
            norm[1] /= length;
            norm[2] /= length;
            
            if ( norm[1] > 0 ) // normale zeigt nach unten
            {
                norm[0] = -norm[0];
                norm[1] = -norm[1];
                norm[2] = -norm[2];
            }
                        
            for ( ; k <= n; k+=4 )
            {
                normal[off+k+X] = norm[0];
                normal[off+k+Y] = norm[1];
                normal[off+k+Z] = norm[2];
                normal[off+k+W] = norm[3];
            }  
            
            n+=4;
            phi += step;
        }
        r -= rstep;
    }
}

// TODO: normals
void yil_disk ( float* buff, int num, int angles, float* normals )
{
    if ( angles < 3 )
        return;
    int minNum = angles * 3;
    
    if ( num < minNum )
        return;
        
    
        
    float lastX =1, lastZ = 0;
    float step = (2 * M_PI) / angles;
    float phi = step;
    for ( int i = 0; i < angles*4; )
    {       
        buff[i*4] = 0;
        buff[i*4+1] = 0;
        buff[i*4+2] = 0;
        buff[i*4+3] = 1;
        i++;
                
        buff[i*4] = lastX;
        buff[i*4+1] = 0;
        buff[i*4+2] = lastZ;
        buff[i*4+3] = 1;
        i++;
        
        lastZ = sin ( phi );
        lastX = cos ( phi );
        
        buff[i*4] = lastX;
        buff[i*4+1] = 0;
        buff[i*4+2] = lastZ;
        buff[i*4+3] = 1;
        i++;
        
        phi += step;
    }
}

// TODO
void yil_sphere ( float* buff, int num, int angles, float* normals );

// TODO: normals
void yil_cylinder ( float* buff, int num, int angles, int height, float* normals )
{
    /*if ( angles < 4 )
        return;*/
    int minNum = angles * 4 * height;
    
    if ( num < minNum )
        return;   
    for ( int y = 0; y < height; y++ )
    {
        float lastX =1, lastZ = 0;
        float step = (2 * M_PI) / angles;
        float phi = step;
        int n1, n2;
        int k = 0;
        for ( int n = 0; n < angles*16; )
        {
            int off = y*16*angles;
            buff[off+n+X] = lastX;
            buff[off+n+Y] = y;
            buff[off+n+Z] = lastZ;
            buff[off+n+W] = 1;
            n1=n;
            n+=4;
            
            buff[off+n+X] = lastX;
            buff[off+n+Y] = y+1;
            buff[off+n+Z] = lastZ;
            buff[off+n+W] = 1;
            n2=n;
            n+=4;
            
            lastZ = sin ( phi );
            lastX = cos ( phi );
            
            buff[off+n+X] = lastX;
            buff[off+n+Y] = y+1;
            buff[off+n+Z] = lastZ;
            buff[off+n+W] = 1;
            n+=4;
            
            buff[off+n+X] = lastX;
            buff[off+n+Y] = y;
            buff[off+n+Z] = lastZ;
            buff[off+n+W] = 1;
                                           
            float a[4] = { buff[n2+X] - buff[n1+X], 
                           buff[n2+Y] - buff[n1+Y], 
                           buff[n2+Z] - buff[n1+Z], 0};
            float b[4] = { buff[n+X] - buff[n1+X], 
                           buff[n+Y] - buff[n1+Y], 
                           buff[n+Z] - buff[n1+Z], 0};
            float norm[4] = { a[1] * b[2] - a[2] * b[1],
                              a[2] * b[0] - a[0] * b[2],
                              a[0] * b[1] - a[1] * b[0], 0};
            float length = sqrt ( norm[0] * norm[0] + norm[1] * norm[1] + norm[2] * norm[2] );
            norm[0] /= length;
            norm[1] /= length;
            norm[2] /= length;
            
            if ( norm[1] > 0 ) // normale zeigt nach unten
            {
                norm[0] = -norm[0];
                norm[1] = -norm[1];
                norm[2] = -norm[2];
            }
                        
            for ( ; k <= n; k+=4 )
            {
                normals[off+k+X] = norm[0];
                normals[off+k+Y] = norm[1];
                normals[off+k+Z] = norm[2];
                normals[off+k+W] = norm[3];
            }  
            
            n+=4;
            phi += step;
        }
    }
}

