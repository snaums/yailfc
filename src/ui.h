/**
 * \file ui.h
 * \author Stefan Naumann
 * \brief UI-concerns like windows and stuff like that
 *
 * \page windows Windows and UI in PiOS
 * 
 * Creating a Window can be done by using the pios_window()-function which also 
 * draws the new window on screen, but you may also use pios_window_create(), 
 * which will not draw the new window. Make sure you save the handle to the 
 * window as you will need that to create controls on the window or control the 
 * window. 
 *
 * A handle to a window is always above or equal 0 and lower than PIOS_MAX_WIN
 * (hwnd >= 0 && hwnd < PIOS_MAX_WIN). The functions should check whether you 
 * gave them a valid hwnd. The hwnd will be used as array-offset. 
 * 
 * \section control Controls
 *
 * There are several types of controls as of writing: Label, Button, Checkbox, 
 * Radiobutton, Textbox (Textinput). As of writing they do not serve a practical
 * purpose (no input it possible at the moment), but that will come later. 
 *
 * Also a handle to a control element is always >= 0 and lower than 
 * PIOS_MAX_WIN_CTRL. The hctrl will also be used as array-offset and is only 
 * valid combined with the hwnd of the parent window. Make sure to not confuse 
 * hctrl and hwnd of several windows as this may cause 'really strange' 
 * behaviours. 
 *
 * \section drawing Drawing and Redrawing
 * 
 * The window environment (i.e. "Desktop") will be redrawn on invalidation. An 
 * invalidation occurs when windows are resized or moved (TODO: invalidate only
 * the affected windows) or called by the user pios_window_update(). The window
 * on position 0 is always the one on the front.
 **/

#ifndef PIOS_UI
#define PIOS_UI

#ifndef PIOS_UI_VER
/// constant defining the version of PIOS_UI-API
#define PIOS_UI_VER 1
#endif

#ifndef PIOS_MAX_WIN_CTRL
/// maximal number of controls on a single window
#define PIOS_MAX_WIN_CTRL 32
#endif

#ifndef PIOS_MAX_WIN 
/// maximal number of windows in the system
#define PIOS_MAX_WIN 32
#endif

/// default window border (top)
#define PIOS_WIN_BORDER_TOP 20
/// default window border (bottom)
#define PIOS_WIN_BORDER_BOTTOM 1
/// default window border (left)
#define PIOS_WIN_BORDER_LEFT 1
/// default window border (right)
#define PIOS_WIN_BORDER_RIGHT 1

/// window border color (uses the same value for RGB(A)-components)
#define PIOS_WINDOW_BORDERCOLOUR 0x44
/// window background (fill)-color (uses the same value for (RGB(A)-components)
#define PIOS_WINDOW_FILLCOLOUR 0xdd

/// default title forthe window
#define PIOS_WINDOW_DEFAULT_TITLE "(Unnamed)\0"

#include <stdio.h>
#include <string.h>

#include "image.h"

/// bitmapped close-button icon for the window
extern uint8_t pios_closeBtn[32];
/// bitmapped minimize-button icon for the window
extern uint8_t pios_minBtn[32];
/// bitmapped maximize button icon for the window
extern uint8_t pios_maxBtn[32];
/// bitmapped checkbox (not ticked)
extern uint8_t pios_checkBtn_false[32];
/// bitmapped checkbox (ticked)
extern uint8_t pios_checkBtn_true[32];
/// bitmapped radiobutton (not ticked)
extern uint8_t pios_radioBtn_false[32];
/// bitmapped radiobutton (ticked)
extern uint8_t pios_radioBtn_true[32];
/// bitmapped icon for the currently focused control element
extern uint8_t pios_selected[32];

/// the framebuffer to be used for the window-environment
extern struct Image* fb;

enum EventType 
{
    KEYBOARD=0x01,
    MISC=0x02
};

struct EventDescr
{
    enum EventType type;
    int value;
};

/**
 * \brief enumerates the (currently supported) list of control elements
 **/
enum CtrlType
{
    LABEL = 0x00,                       ///< a label type
    CHECKBOX = 0x01,                    ///< a checkbox (bool)
    RADIOBOX = 0x02,                    ///< a radiobutton (bool in a group)
    TEXTBOX = 0x03,                     ///< a text box (string)
    BUTTON = 0x04,                      ///< a button (press <=> "timing")
    IMAGE = 0x05,                       ///< an image for copying directly into the window
    PIOS_CTRL_ENDTYPELIST = 0x06        ///< a dummy element marking the end of the enumerate-list
};

/**
 * \brief a focus-state of a control-element
 * \note used as bitmap
 **/
enum CtrlState
{
    //DISABLED = 0bxxxxxxx0,
    ENABLED = 0x01,                     ///< the control is enabled
    FOCUS = 0x02                        ///< the control has focus
};

/**
 * \brief structure describing a control-element
 **/
struct CtrlDescr
{
    enum CtrlType type;                 ///< the type of the control element
    char name[20];                      ///< the name of the control element
    void* state;                        ///< the value / state of the element (ptr to user-data)
    enum CtrlState focus;               ///< the current focus-state
    int32_t x;                          ///< the x-position of the topleft corner
    int32_t y;                          ///< the y-position of the topleft corner  
    uint32_t width;                     ///< the width of the control element (currently only used with textbox)
    uint32_t height;                    ///< the height of the control element (currently only used with textbox)
    int gid;                            ///< the group of the control element (only used by radiobutton)
    int hctrl;                          ///< the handle of the control element (>= 0)
};

/**
 * \brief describes border of the windows
 **/
struct Border
{
    uint8_t top,        ///< top-border
            right,      ///< right-border
            bottom,     ///< bottom-border
            left;       ///< left-border
};

/**
 * \brief usable with OR on WindowDescr.state
 **/
enum WindowState 
{
    VISIBLE = 0x01      ///< is the window visible or hidden?
};

/**
 * \brief structure describing a window
 **/
struct WindowDescr
{
    int hwnd;                           ///< the handle of the window (>= 0)
    char title[30];                     ///< the title of the window
    int32_t x;                          ///< the x-position of the topleft corner
    int32_t y;                          ///< the y-position of the topleft corner
    uint32_t width;                     ///< the width of the window
    uint32_t height;                    ///< the height of the window
    uint32_t state;                     ///< state (min, max, etc)
    struct Border border;               ///< describes the borders of the window
    struct CtrlDescr controls[ PIOS_MAX_WIN_CTRL ]; ///< a list of control element of the window
};

/**
 * \brief a global structure describing all windows with their respective control elements
 **/
extern struct WindowDescr pios_window_descr[ PIOS_MAX_WIN ];

/**
 * \brief initialize the window-environment, i.e. set the data structure to init
 * \param[in] fb the framebuffer to draw into
 * \return always 0
 **/
int pios_window_init ( struct Image* fb );
/**
 * \brief create a window if possible
 * \param[in] x the x-position of the topleft corner
 * \param[in] y the y-position of the topleft corner
 * \param[in] width the width of the window
 * \param[in] height the height of the window
 * \return the hwnd on success ( >= 0 ), negative number on error
 **/
int pios_window_create ( int32_t x, int32_t y, uint32_t width, uint32_t height );
/**
 * \brief create a window and draw update the window-environment
 * \param[in] x the x-position of the topleft corner
 * \param[in] y the y-position of the topleft corner
 * \param[in] width the width of the window
 * \param[in] height the height of the window
 * \return the hwnd on success ( >= 0 ), negative number on error
 **/
int pios_window ( int32_t x, int32_t y, uint32_t width, uint32_t height );
/**
 * \brief change the sizes of the borders of a window
 * \param[in] hwnd the window to be modified
 * \param[in] top the top-border (or <0 if not to change)
 * \param[in] right the right-border (or <0 if not to change)
 * \param[in] bottom the bottom-border (or <0 if not to change)
 * \param[in] left the left-border (or <0 if not to change)
 * \return -1 on error, 0 on success
 **/
int pios_window_border ( int hwnd, int16_t top, int16_t right, int16_t bottom, int16_t left );
/**
 * \brief draw the window buttons ( min, max, close )
 * \param[in] hwnd the handle to the window to be docorated
 * \return 0 on success, something else otherwise
 **/
int pios_windowButtons ( int hwnd );
/**
 * \brief set the window title of a window
 * \param[in] hwnd the handle to the window to be docorated
 * \param[in] title a null-terminated string of the window title
 * \return 0 on success, something negative on error
 **/
int pios_windowTitle ( int hwnd, const char* title );
/**
 * \brief draw the title on the titlebar of a window
 * \param[in] hwnd the handle to the window to be docorated
 * \return 0 on success, something negative on error
 **/
int pios_windowDrawTitle ( int hwnd );
/**
 * \brief invalidates all windows and redraws them
 **/
int pios_window_update ();

/**
 * \brief empty a window (i.e. delete all control elements from a window)
 * \param[in] hwnd the handle to the window
 **/
int pios_window_empty ( int hwnd );
/**
 * \brief invalidate a window and redraw it
 * \param[in] hwnd handle to the window to be redrawn
 **/
int pios_window_invalidate ( int hwnd );
/**
 * \brief resize the window and invalidate it
 * \param[in] hwnd handle to the window
 * \param[in] w new width of the window
 * \param[in] h new height of the window
 * \return 0
 **/
int pios_windowResize ( int hwnd, int w, int h );
/**
 * \brief move the window and invalidate everything
 * \param[in] hwnd handle to the window
 * \param[in] x new x-position of the topleft corner
 * \param[in] y new y-position of the topleft corner
 * \return 0
 **/
int pios_windowMove ( int hwnd, int x, int y );



/**
 * \brief draws a symbol next to the control-element which has focus at the moment
 * \param[in] ctrl the control descriptor element which holds focus
 * \param[in] win the window descriptor of the element
 **/
int pios_window_control_showFocus (struct CtrlDescr* ctrl, struct WindowDescr* win);


/// TODO
struct EventDescr pios_wait ( int event );

enum Signal
{
    AA
};
/**
 * \brief register a callback function for when something happens with a control-element
 **/
int pios_window_control_cb ( int hwnd, int hctrl, int (*cb) ( int, int, enum Signal, void*, void*), void* data );

/**
 * \brief draw every control element of the selected window
 * \param[in] hwnd handle to the window for redecorating
 * \return 0
 **/
int pios_window_control_draw ( int hwnd );
/**
 * \brief create a control element
 * \param[in] hwnd the handle of the window to be filled with a new control-element
 * \param[in] x the x-value of the topleft corner of the element in the window
 * \param[in] y the y-value of the topleft corner of the element in the window
 * \param[in] type the type of the control-element (label, button, etc)
 * \param[in] state the value / state of the element (can be string, bool, int, whatever fits)
 * \return the handle to the control element created
 **/
int pios_window_createControl ( int hwnd, int32_t x, int32_t y, enum CtrlType type, void* state );
/**
 * \brief create an image control element
 * \param[in] hwnd the handle of the window to be filled with a new control-element
 * \param[in] x the x-value of the topleft corner of the label in the window
 * \param[in] y the y-value of the topleft corner of the label in the window
 * \param[in] width the width of the textbox
 * \param[in] height the height of the textbox (will not change the size of the text)
 * \param[in] buffer the struct Image to be shown inside the image-box
 * \return the handle to the control element created
 **/
int pios_window_control_image ( int hwnd, int32_t x, int32_t y, uint32_t width, uint32_t height, struct Image* buffer );
/**
 * \brief create a label control element
 * \param[in] hwnd the handle of the window to be filled with a new control-element
 * \param[in] x the x-value of the topleft corner of the label in the window
 * \param[in] y the y-value of the topleft corner of the label in the window
 * \param[in] str the string to be displayed by the label
 * \return the handle to the control element created
 **/
int pios_window_control_label ( int hwnd, int32_t x, int32_t y, const char* str );
/**
 * \brief create a checkbox control element
 * \param[in] hwnd the handle of the window to be filled with a new control-element
 * \param[in] x the x-value of the topleft corner of the element in the window
 * \param[in] y the y-value of the topleft corner of the element in the window
 * \param[in] str the string to be displayed by the label
 * \param[in] value the state of the checkbox (true = checked, false = unchecked)
 * \note will create a label right next to it
 * \return the handle to the control element created
 **/
int pios_window_control_checkbox ( int hwnd, int32_t x, int32_t y, const char* str, int* value );
/**
 * \brief create a radiobutton control element
 * \param[in] hwnd the handle of the window to be filled with a new control-element
 * \param[in] gid the group of radioboxes this one is part of; only one can be selected at once
 * \param[in] x the x-value of the topleft corner of the element in the window
 * \param[in] y the y-value of the topleft corner of the element in the window
 * \param[in] str the string to be displayed by the label
 * \param[in] value the state of the radiobox (true = checked, false = unchecked)
 * \note will create a label right next to it
 * \return the handle to the control element created
 **/
int pios_window_control_radiobox ( int hwnd, int gid, int32_t x, int32_t y, const char* str, int* value );
/**
 * \brief create a radiobutton control element
 * \param[in] hwnd the handle of the window to be filled with a new control-element
 * \param[in] x the x-value of the topleft corner of the element in the window
 * \param[in] y the y-value of the topleft corner of the element in the window
 * \param[in] width the width of the textbox
 * \param[in] height the height of the textbox (will not change the size of the text)
 * \param[in] value char* to the text to be visible 
 * \return the handle to the control element created
 **/
int pios_window_control_textbox ( int hwnd, int32_t x, int32_t y, uint32_t width, uint32_t height, char* value );
/**
 * \brief create a button control element
 * \param[in] hwnd the handle of the window to be filled with a new control-element
 * \param[in] x the x-value of the topleft corner of the element in the window
 * \param[in] y the y-value of the topleft corner of the element in the window
 * \param[in] value char* to the text to be visible 
 * \return the handle to the control element created
 **/
int pios_window_control_button ( int hwnd, int32_t x, int32_t y, char* value );


/** 
 * \brief draw an image-control into the window
 * \param[in] ctrl the control-element to be drawn
 * \param[in] win the parent window of the control element
 * \sa pios_window_control_draw
 * \return 0 on success
 **/
int pios_window_control_drawImage ( struct CtrlDescr* ctrl, struct WindowDescr* win );
/** 
 * \brief draw a label into the window
 * \param[in] ctrl the control-element to be drawn
 * \param[in] win the parent window of the control element
 * \sa pios_window_control_draw
 * \return 0 on success
 **/
int pios_window_control_drawLabel (struct CtrlDescr* ctrl, struct WindowDescr* win);
/** 
 * \brief draw a checkbox into the window
 * \param[in] ctrl the control-element to be drawn
 * \param[in] win the parent window of the control element
 * \sa pios_window_control_draw
 * \return 0 on success
 **/
int pios_window_control_drawCheckBox (struct CtrlDescr* ctrl, struct WindowDescr* win);
/** 
 * \brief draw a radiobutton into the window
 * \param[in] ctrl the control-element to be drawn
 * \param[in] win the parent window of the control element
 * \sa pios_window_control_draw
 * \return 0 on success
 **/
int pios_window_control_drawRadio (struct CtrlDescr* ctrl, struct WindowDescr* win);
/** 
 * \brief draw a textbox into the window
 * \param[in] ctrl the control-element to be drawn
 * \param[in] win the parent window of the control element
 * \sa pios_window_control_draw
 * \return 0 on success
 **/
int pios_window_control_drawTextbox (struct CtrlDescr* ctrl, struct WindowDescr* win);
/** 
 * \brief draw a button into the window
 * \param[in] ctrl the control-element to be drawn
 * \param[in] win the parent window of the control element
 * \sa pios_window_control_draw
 * \return 0 on success
 **/
int pios_window_control_drawBtn (struct CtrlDescr* ctrl, struct WindowDescr* win);

/** 
 * \brief name a control element
 * \param[in] hwnd the handle to the parent window of the control
 * \param[in] hctrl the handle to the control
 * \param[in] name the name (null-terminated)
 * \return 0 on success
 **/
int pios_window_nameControl ( int hwnd, int hctrl, const char* name );

/** 
 * TODO
 **/
int pios_window_control_activate ( int hwnd, int controlId );
/**
 * \brief focus a specific control element
 * \param[in] hwnd handle to the window containing the element
 * \param[in] controlId handle to the control element
 * \return 0 on success
 **/
int pios_window_control_focus ( int hwnd, int controlId );
/**
 * \brief enable a specific control element
 * \param[in] hwnd handle to the window containing the element
 * \param[in] controlId handle to the control element
 * \sa pios_window_control_disable
 * \return 0 on success
 **/
int pios_window_control_enable ( int hwnd, int controlId );
/**
 * \brief disable a specific control element
 * \param[in] hwnd handle to the window containing the element
 * \param[in] controlId handle to the control element
 * \sa pios_window_control_enable
 * \return 0 on success
 **/
int pios_window_control_disable ( int hwnd, int controlId );
/**
 * \brief set the group id of a control element
 * \param[in] hwnd handle to the window containing the element
 * \param[in] controlId handle to the control element
 * \param[in] gid the new group ID of the control element
 * \return 0 on success
 **/
int pios_window_control_setGid ( int hwnd, int controlId, int gid );

/**
 * \brief show a window (i.e. set visible)
 * \param[in] hwnd the window to set visible
 **/
int pios_window_show ( int hwnd );
/**
 * \brief hide a window (i.e. set invisible)
 * \param[in] hwnd the window to set invisible
 **/
int pios_window_hide ( int hwnd );

#endif

