# folder tree:
#   ./source   -> sourcefiles
#   ./build    -> built files
#   ./Makefile -> this Makefile

BOARD=x86
ifeq ($(BOARD),x86)
	DEBUGFLAGS=-fopenmp
	DEBUG=-g -fopenmp
	O=-O2
	LIBS=-lGL -lglut -lm
	LDFLAGS=$(LIBS) -fopenmp
	ASFILES=
	CPUINFO=-I.
else
	include lib/PiOS/boards/$(BOARD)/board.mk
	O=-Os
	#PF=arm-none-eabi-
	SHELL := /bin/bash
	## find the version number of gcc (I think this is a bit too complicated :/)
	CCVERSION:=$(shell $(PLATFORM)-gcc --version | sed 's/[ ]/\n/g' | grep "\." | xargs | awk '{ print $$1 }')
	ARMGCCLIBPATH=/usr/lib/gcc/$(PLATFORM)/$(CCVERSION)/#hard
	CPUINFO=-mcpu=$(CPU) -marm -mfloat-abi=hard -mfpu=vfp -Ilib/PiOS/include -Ilib/PiOS/boards/$(BOARD)
	LIBS=-lpios -lm -lgcc -lc
	LDFLAGS=$(LIBS) -Llib -L$(ARMGCCLIBPATH)
	DEPS=lib/libpios.a lib/libm.a #lib/PiOS/lib/libc.a
	outname=kernel.img
	ASOPTS=-mcpu=$(CPU) -mfloat-abi=hard -mfpu=vfp
	ASFILES=build/startup.o
	
	NEWLIB_CFLAGS?=-mcpu=$(CPU) -marm -mfloat-abi=hard -mfpu=vfp
    NEWLIB_OPTS?=--target=arm-none-eabi --enable-newlib-hw-fp --with-float=hard --with-cpu=arm1176jzf-s --with-fpu=vfp --disable-multilib --disable-shared --enable-target-optspace  --disable-newlib-supplied-syscalls
endif 

DEBUGFLAGS?=
DEBUG?=
DEPS?=

# folders
SOURCE=src/
BUILD=build/
DATA=data/

# tools
PF=$(PLATFORM)-
CC      = $(PF)gcc
AR      = $(PF)ar
LD      = $(PF)ld
AS      = $(PF)as
OBJCOPY = $(PF)objcopy
OBJDUMP = $(PF)objdump

#output filename
outname?=yailfc

# CPU and CFLAGS
O?=
CPUINFO?=
CFLAGSI=$(O) -Wall $(DEBUG) $(CPUINFO)
CFLAGS=-std=c99 -pedantic $(CFLAGSI)

OBJ := $(patsubst $(SOURCE)%.c, $(BUILD)%.o, $(wildcard $(SOURCE)*.c)) $(ASFILES)
DATAOBJ = $(DATA)boxtex.o $(DATA)stone.o $(DATA)wall.o $(DATA)water.o $(DATA)fire.o #$(DATA)cake.o
DATAFIN = $(DATA)ui_data.o $(DATA)fontd.o

# exclude main from libbuilding as it is evil
LIBOBJ = $(patsubst $(BUILD)main.o, , $(OBJ) $(DATAOBJ))

all: $(outname) $(DEPS)

lib/libpios.a:
	BOARD="$(BOARD)" \
		cd lib/PiOS/ && \
		make lib/libpios.a 
	mv lib/PiOS/lib/libpios.a lib/libpios.a

newlib:
	-mkdir lib/build
	cd lib/build && \
		CFLAGS_FOR_TARGET="$(NEWLIB_CFLAGS)"\
		../newlib-cygwin/configure $(NEWLIB_OPTS) && \
		$(MAKE)
	mv lib/build/arm-none-eabi/newlib/libc.a lib/libc.a
	mv lib/build/arm-none-eabi/newlib/libm.a lib/libm.a
    
newlib_clean:
	rm -r lib/build
	mkdir lib/build
	rm lib/libc.a lib/libm.a

lib/libm.a:
	make newlib

lib: $(LIBOBJ) 
	$(AR) rcv lib$(outname).a $(LIBOBJ)

# link the object-files together
yailfc: $(OBJ) $(DATAOBJ) $(DATAFIN)
	$(CC) -I $(SOURCE) $(OBJ) $(DATAOBJ) $(DATAFIN) $(LDFLAGS) -o yailfc

dump: kernel.elf kernel.list
kernel.list : kernel.elf
	$(OBJDUMP) -d kernel.elf > kernel.list

# make the kernel-image
kernel.img: kernel.elf
	$(OBJCOPY) kernel.elf -O binary kernel.img

$(BUILD)%.o: $(SOURCE)%.s
	$(AS) -I $(SOURCE) $(ASOPTS) $< -o $@

kernel.elf: $(OBJ) $(DATAOBJ) $(DATAFIN) $(DEPS)
	$(LD) --no-undefined $(OBJ) $(DATAOBJ) $(DATAFIN) $(LDFLAGS) -Map kernel.map -o kernel.elf -T lib/PiOS/boards/$(BOARD)/kernel.ld

# build the data-c-files (like textures, image-data, etc)
$(DATA)%.o: $(DATA)%.c
	$(CC) -I $(SOURCE) -std=c99 $(CFLAGSI) $< -c -o $@

# build code-files
$(BUILD)%.o: $(SOURCE)%.c
	$(CC) -I $(SOURCE) $(CFLAGS) $< -c -o $@

$(DATA)%.c: $(DATA)%.data
	cd $(DATA) && \
	    xxd -i $(subst $(DATA),,$<) $(subst $(DATA),,$@)

doc:
	doxygen doxygen_template

# mkdir build-folder
$(BUILD):
	mkdir $@

# remove everything
clean:
	rm $(BUILD)* $(DATAOBJ) $(DATAFIN)
	rm $(outname)

